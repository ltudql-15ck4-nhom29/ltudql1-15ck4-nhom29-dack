# NHÓM 33 - 15CK4 - LTDUQL1 - DACK #

## THÀNH VIÊN ##
* 1460044 Phạm Văn Tín Thành

## PHÂN CẤP THÀNH VIÊN CỦA TEAM ##
* ADMIN (Admin): Phạm Văn Tín Thành
* TEACHERS (Read): Thầy Ngô Chánh Đức

## THÔNG TIN REPOSITORY ##
* Owner: ltudql1-15ck4-nhom29
* Project name: ltudql1-15ck4-nhom29-dack
* Repository name: ltudql1-15ck4-nhom29-dack
* Access level: This is a private repository
* Repository type: Git
* Description: (none)
* Forking: Allow only private forks
* Project management: Issue tracking (no), Wiki (yes)
* Language: C#
* Integrations: Enable Hipchat notifications (no)