﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAL;
namespace BLL
{
    public class PhieuCanhCaoBLL
    {
        public List<PhieuCanhCaoDTO> layDSPhieuCanhCao()
        {
            PhieuCanhCaoDAL dal = new PhieuCanhCaoDAL();
            List<PhieuCanhCaoDTO> ds = dal.layDSPhieuCanhCao();
            return ds;
        }

        public int themPhieuCanhCao(PhieuCanhCaoDTO pcc)
        {
            PhieuCanhCaoDAL dal = new PhieuCanhCaoDAL();
            return dal.themPhieuCanhCao(pcc);
        }

        public int capNhatDanhSachPhieuCanhCao(PhieuCanhCaoDTO pcc)
        {
            xoaPhieuCanhCaoNeuMaHinhThucPhatLonHon(pcc);
            themPhieuCanhCaoNeuMaHinhThucPhatNhoHon(pcc);
            return 1;
        }

        public int xoaPhieuCanhCaoNeuMaHinhThucPhatLonHon(PhieuCanhCaoDTO pcc)
        {
            PhieuCanhCaoDAL dal = new PhieuCanhCaoDAL();
            return dal.xoaPhieuCanhCaoNeuMaHinhThucPhatLonHon(pcc);
        }

        public int themPhieuCanhCaoNeuMaHinhThucPhatNhoHon(PhieuCanhCaoDTO pcc)
        {
            PhieuCanhCaoDAL dal = new PhieuCanhCaoDAL();
            return dal.themPhieuCanhCaoNeuMaHinhThucPhatNhoHon(pcc);
        }
    }
}
