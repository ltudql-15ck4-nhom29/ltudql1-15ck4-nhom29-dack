﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAL;
namespace BLL
{
    public class HinhThucPhatBLL
    {
        public List<HinhThucPhatDTO> layDSHinhThucPhat()
        {
            HinhThucPhatDAL dal = new HinhThucPhatDAL();
            List<HinhThucPhatDTO> ds = dal.layDSHinhThucPhat();
            return ds;
        }
    }
}
