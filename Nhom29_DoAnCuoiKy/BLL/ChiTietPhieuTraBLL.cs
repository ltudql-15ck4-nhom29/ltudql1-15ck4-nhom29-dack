﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAL;
namespace BLL
{
    public class ChiTietPhieuTraBLL
    {
        public List<ChiTietPhieuTraDTO> layDSChiTietPhieuTra()
        {
            ChiTietPhieuTraDAL dal = new ChiTietPhieuTraDAL();
            List<ChiTietPhieuTraDTO> ds = dal.layDSChiTietPhieuTra();
            return ds;
        }
    }
}
