﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAL;
namespace BLL
{
    public class PhieuMuonBLL
    {
        public List<PhieuMuonDTO> layDSPhieuMuonDayDu()
        {
            PhieuMuonDAL dal1 = new PhieuMuonDAL();
            List<PhieuMuonDTO> ds = dal1.layDSPhieuMuon();
            ChiTietPhieuMuonDAL dal2 = new ChiTietPhieuMuonDAL();
            List<ChiTietPhieuMuonDTO> lst = dal2.layDSChiTietPhieuMuon();
            ds = PhieuMuonDTO.layDSPhieuMuonDayDu(ds, lst);
            return ds;
        }

        public List<PhieuMuonDTO> layDSPhieuMuonDayDuCoQuaHanChuaTraNgayTra()
        {
            List<PhieuMuonDTO> ds = layDSPhieuMuonDayDu();
            ChiTietPhieuTraDAL dal = new ChiTietPhieuTraDAL();
            List<ChiTietPhieuTraDTO> lst = dal.layDSChiTietPhieuTra();
            for (int i = 0; i < ds.Count; i++)
            {
                ds[i].tinhQuaHanChuaTraNgayTra(lst);
            }
            return ds;
        }

        public void themPhieuMuon(PhieuMuonDTO pm)
        {
            PhieuMuonDAL dal1 = new PhieuMuonDAL();
            ChiTietPhieuMuonDAL dal2 = new ChiTietPhieuMuonDAL();
            BanSaoDAL dal3 = new BanSaoDAL();
            pm.MaPhieu = dal1.themPhieuMuon(pm);
            List<ChiTietPhieuMuonDTO> lst = pm.LstCTPM;
            for(int i = 0; i < lst.Count; i++)
            {
                lst[i].MaPhieuMuon = pm.MaPhieu;
                dal2.themChiTietPhieuMuon(lst[i]);
                BanSaoDTO bs = new BanSaoDTO();
                bs.MaBanSao = lst[i].MaBanSao;
                DocGiaDTO dg = new DocGiaDTO();
                dg.MaDocGia = pm.MaDocGia;
                dal3.capNhatBanSao(bs, dg);
            }
        }

        public int xoaPhieuMuon(PhieuMuonDTO pm)
        {
            PhieuMuonDAL dal1 = new PhieuMuonDAL();
            ChiTietPhieuMuonDAL dal2 = new ChiTietPhieuMuonDAL();
            ChiTietPhieuTraDAL dal3 = new ChiTietPhieuTraDAL();
            PhieuTraDAL dal4 = new PhieuTraDAL();
            List<ChiTietPhieuMuonDTO> lstCTPM = pm.LstCTPM;
            for (int i = 0; i < lstCTPM.Count; i++)
            {
                ChiTietPhieuMuonDTO ctpm = lstCTPM[i];
                dal2.xoaChiTietPhieuMuon(ctpm);
            }
            dal3.xoaChiTietPhieuTraTheoPhieuMuon(pm);
            dal4.xoaPhieuTraTrong();
            return dal1.xoaPhieuMuon(pm);
        }

        public int capNhatPhieuMuonDayDu(PhieuMuonDTO pm)
        {
            PhieuMuonDAL dal = new PhieuMuonDAL();
            return dal.capNhatPhieuMuonDayDu(pm);
        }
    }
}
