﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAL;

namespace BLL
{
    public class TaiLieuBLL
    {
        public List<TaiLieuDTO> layDSTaiLieu()
        {
            TaiLieuDAL dal = new TaiLieuDAL();
            List<TaiLieuDTO> ds = dal.layDSTaiLieu();
            return ds;
        }

        public List<TaiLieuDTO> layDSTaiLieuDayDu()
        {
            TaiLieuDAL dal1 = new TaiLieuDAL();
            List<TaiLieuDTO> ds = dal1.layDSTaiLieu();
            BanSaoDAL dal2 = new BanSaoDAL();
            List<BanSaoDTO> lst = dal2.layDSBanSao();
            TaiLieuDTO.layDSTaiLieuCoDSBanSao(ds, lst);
            return ds;
        }

        public int xoaTaiLieu(TaiLieuDTO tl)
        {
            TaiLieuDAL dal1 = new TaiLieuDAL();
            BanSaoDAL dal2 = new BanSaoDAL();
            ChiTietPhieuMuonDAL dal3 = new ChiTietPhieuMuonDAL();
            ChiTietPhieuTraDAL dal4 = new ChiTietPhieuTraDAL();
            PhieuMuonDAL dal5 = new PhieuMuonDAL();
            PhieuTraDAL dal6 = new PhieuTraDAL();
            List<BanSaoDTO> lstBanSao = tl.LstBanSao;
            for (int i = 0; i < lstBanSao.Count; i++)
            {
                BanSaoDTO bs = lstBanSao[i];

                ChiTietPhieuTraDTO ctpt = new ChiTietPhieuTraDTO();
                ctpt.MaBanSao = bs.MaBanSao;
                dal4.xoaChiTietPhieuTraTheoMaBanSao(ctpt);
                dal6.xoaPhieuTraTrong();

                ChiTietPhieuMuonDTO ctpm = new ChiTietPhieuMuonDTO();
                ctpm.MaBanSao = bs.MaBanSao;
                dal3.xoaChiTietPhieuMuonTheoMaBanSao(ctpm);
                dal5.xoaPhieuMuonTrong();

                dal2.xoaBanSao(bs);
            }
            return dal1.xoaTaiLieu(tl);
        }

        public int capNhatTaiLieu(TaiLieuDTO tl)
        {
            TaiLieuDAL dal = new TaiLieuDAL();
            return dal.capNhatTaiLieu(tl);
        }

        public int themTaiLieuCoSoBanSao(TaiLieuDTO tl, int soBanSao)
        {
            TaiLieuDAL dal1 = new TaiLieuDAL();
            tl.MaTaiLieu = dal1.themTaiLieu(tl);
            BanSaoDAL dal2 = new BanSaoDAL();
            for(int i = 0; i < soBanSao; i++)
            {
                BanSaoDTO bs = new BanSaoDTO();
                bs.MaTaiLieu = tl.MaTaiLieu;
                dal2.themBanSao(bs);
            }
            return soBanSao + 1;
        }

        public int capNhatTaiLieuCoSoBanSao(TaiLieuDTO tl, int soBanSao)
        {
            BanSaoDAL dal = new BanSaoDAL();
            for(int i = 0; i < soBanSao; i++)
            {
                BanSaoDTO bs = new BanSaoDTO();
                bs.MaTaiLieu = tl.MaTaiLieu;
                dal.themBanSao(bs);
            }
            return soBanSao + 1;
        }

        public bool kiemTraTenTaiLieu(TaiLieuDTO tl)
        {
            TaiLieuDAL dal = new TaiLieuDAL();
            return dal.kiemTraTenTaiLieu(tl);
        }
    }
}
