﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using DTO;
namespace BLL
{
    public class LoaiDocGiaBLL
    {
        public List<LoaiDocGiaDTO> layDSLoaiDocGia()
        {
            LoaiDocGiaDAL dal = new LoaiDocGiaDAL();
            List<LoaiDocGiaDTO> ds = dal.layDSLoaiDocGia();
            return ds;
        }
    }
}
