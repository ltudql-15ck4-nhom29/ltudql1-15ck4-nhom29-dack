﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAL;
namespace BLL
{
    public class PhieuTraBLL
    {
        public List<PhieuTraDTO> layDSPhieuTra()
        {
            PhieuTraDAL dal = new PhieuTraDAL();
            List<PhieuTraDTO> ds = dal.layDSPhieuTra();
            return ds;
        }

        public List<PhieuTraDTO> layDSPhieuTraDayDuCoDSMaPhieuMuon()
        {
            PhieuTraDAL dal1 = new PhieuTraDAL();
            List<PhieuTraDTO> ds = dal1.layDSPhieuTra();
            ChiTietPhieuTraDAL dal2 = new ChiTietPhieuTraDAL();
            List<ChiTietPhieuTraDTO> lst = dal2.layDSChiTietPhieuTra();
            ds = PhieuTraDTO.layDSPhieuTraDayDu(ds, lst);
            for (int i = 0; i < ds.Count; i++)
            {
                ds[i].tinhDSMaPhieuMuon();
            }
           return ds;
        }

        public int xoaPhieuTra(PhieuTraDTO pt)
        {
            PhieuTraDAL dal1 = new PhieuTraDAL();
            ChiTietPhieuTraDAL dal2 = new ChiTietPhieuTraDAL();
            ChiTietPhieuMuonDAL dal3 = new ChiTietPhieuMuonDAL();
            PhieuMuonDAL dal4 = new PhieuMuonDAL();
            List<ChiTietPhieuTraDTO> lstCTPT = pt.LstCTPT;
            for (int i = 0; i < lstCTPT.Count; i++)
            {
                ChiTietPhieuTraDTO ctpt = lstCTPT[i];

                ChiTietPhieuMuonDTO ctpm = new ChiTietPhieuMuonDTO();
                ctpm.SoThuTuCTPT = ctpt.SoThuTu;
                dal3.xoaChiTietPhieuMuonTheoSTT_CTPT(ctpm);

                dal2.xoaChiTietPhieuTra(ctpt);                
            }
            dal4.xoaPhieuMuonTrong();
            return dal1.xoaPhieuTra(pt);
        }

        public int capNhatPhieuTra(PhieuTraDTO pt)
        {
            PhieuTraDAL dal1 = new PhieuTraDAL();
            ChiTietPhieuTraDAL dal2 = new ChiTietPhieuTraDAL();
            List<ChiTietPhieuTraDTO> lstCTPT = pt.LstCTPT;
            for (int i = 0; i < lstCTPT.Count; i++)
            {
                ChiTietPhieuTraDTO ctpt = lstCTPT[i];
                ctpt.NgayTra = pt.NgayLapPhieu;
                dal2.capNhatChiTietPhieuTraTheoPhieuTra(ctpt);
            }
            return dal1.capNhatPhieuTra(pt);
        }

        public void themPhieuTra(PhieuTraDTO pt)
        {
            PhieuTraDAL dal1 = new PhieuTraDAL();
            ChiTietPhieuTraDAL dal2 = new ChiTietPhieuTraDAL();
            BanSaoDAL dal3 = new BanSaoDAL();
            ChiTietPhieuMuonDAL dal4 = new ChiTietPhieuMuonDAL();
            pt.MaPhieu = dal1.themPhieuTra(pt);
            List<ChiTietPhieuTraDTO> lst = pt.LstCTPT;
            for (int i = 0; i < lst.Count; i++)
            {
                lst[i].MaPhieuTra = pt.MaPhieu;
                lst[i].SoThuTu = dal2.themChiTietPhieuTra(lst[i]);
                
                BanSaoDTO bs = new BanSaoDTO();
                bs.MaBanSao = lst[i].MaBanSao;
                dal3.capNhatBanSao(bs, null);

                ChiTietPhieuMuonDTO ctpm = new ChiTietPhieuMuonDTO();
                ctpm.MaBanSao = lst[i].MaBanSao;
                ctpm.SoThuTuCTPT = lst[i].SoThuTu;
                dal4.capNhatChiTietPhieuMuon(ctpm);
            }
        }
    }
}
