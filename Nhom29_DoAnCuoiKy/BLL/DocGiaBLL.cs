﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAL;

namespace BLL
{
    public class DocGiaBLL
    {
        public List<DocGiaDTO> layDSDocGia()
        {
            DocGiaDAL dal = new DocGiaDAL();
            List<DocGiaDTO> ds = dal.layDSDocGia();
            return ds;
        }

        public List<DocGiaDTO> layDSDocGiaDayDuCoQuaHan()
        {
            DocGiaDAL dal = new DocGiaDAL();
            List<DocGiaDTO> ds = dal.layDSDocGia();
            PhieuMuonBLL bll = new PhieuMuonBLL();
            List<PhieuMuonDTO> lst = bll.layDSPhieuMuonDayDuCoQuaHanChuaTraNgayTra();
            for(int i = 0; i < ds.Count; i++)
            {
                ds[i].LstPM = PhieuMuonDTO.layDSPhieuMuonTheoDocGia(ds[i], lst);
                ds[i].tinhSoTLQuaHan();
            }
            return ds;
        }

        public int themDocGia(DocGiaDTO dg)
        {
            DocGiaDAL dal = new DocGiaDAL();
            return dal.themDocGia(dg);
        }

        public int capNhatSoTLDangMuon(DocGiaDTO dg)
        {
            DocGiaDAL dal = new DocGiaDAL();
            return dal.capNhatSoTLDangMuon(dg);
        }

        public int xoaDocGia(DocGiaDTO dg)
        {
            DocGiaDAL dal1 = new DocGiaDAL();
            PhieuTraDAL dal2 = new PhieuTraDAL();
            PhieuMuonBLL bll = new PhieuMuonBLL();
            List<PhieuMuonDTO> lstPM = dg.LstPM;
            for (int i = 0; i < lstPM.Count; i++)
            {
                PhieuMuonDTO pm = lstPM[i];
                bll.xoaPhieuMuon(pm);
            }
            return dal1.xoaDocGia(dg);
        }
        
        public int capNhatDocGia(DocGiaDTO dg)
        {
            DocGiaDAL dal = new DocGiaDAL();
            return dal.capNhatDocGia(dg);
        }

        public int capNhatHinhThucPhat(DocGiaDTO dg)
        {
            DocGiaDAL dal = new DocGiaDAL();
            return dal.capNhatHinhThucPhat(dg);
        }
    }
}
