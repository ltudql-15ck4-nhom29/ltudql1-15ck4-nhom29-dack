﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAL;

namespace BLL
{
    public class BanSaoBLL
    {
        public List<BanSaoDTO> layDSBanSao()
        {
            BanSaoDAL dal = new BanSaoDAL();
            List<BanSaoDTO> ds = dal.layDSBanSao();
            return ds;
        }

        public int capNhatBanSao(BanSaoDTO banSao)
        {
            BanSaoDAL dal = new BanSaoDAL();
            return dal.capNhatBanSao(banSao);
        }
    }
}
