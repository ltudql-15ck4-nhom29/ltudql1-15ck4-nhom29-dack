﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAL;
namespace BLL
{
    public class ChiTietPhieuMuonBLL
    {
        public List<ChiTietPhieuMuonDTO> layDSChiTietPhieuMuon()
        {
            ChiTietPhieuMuonDAL dal = new ChiTietPhieuMuonDAL();
            List<ChiTietPhieuMuonDTO> ds = dal.layDSChiTietPhieuMuon();
            return ds;
        }
        
        public int themChiTietPhieuMuon(ChiTietPhieuMuonDTO ctpm)
        {
            ChiTietPhieuMuonDAL dal = new ChiTietPhieuMuonDAL();
            return dal.themChiTietPhieuMuon(ctpm);
        }
    }
}
