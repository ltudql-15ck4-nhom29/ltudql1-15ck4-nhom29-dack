﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAL;

namespace BLL
{
    public class LoaiCapBacBLL
    {
        public List<LoaiCapBacDTO> layDSLoaiCapBac()
        {
            LoaiCapBacDAL dal = new LoaiCapBacDAL();
            List<LoaiCapBacDTO> ds = dal.layDSLoaiCapBac();
            return ds;
        }
    }
}
