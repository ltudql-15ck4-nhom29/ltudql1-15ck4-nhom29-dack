﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAL;
namespace BLL
{
    public class LoaiTaiLieuBLL
    {
        public List<LoaiTaiLieuDTO> layDSLoaiTaiLieu()
        {
            LoaiTaiLieuDAL dal = new LoaiTaiLieuDAL();
            List<LoaiTaiLieuDTO> ds = dal.layDSLoaiTaiLieu();
            return ds;
        }
    }
}
