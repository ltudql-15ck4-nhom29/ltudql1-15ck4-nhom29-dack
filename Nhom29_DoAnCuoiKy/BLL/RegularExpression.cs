﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace BLL
{
    //Biểu thức chính quy (Regular Expression):
    //nếu chuỗi truyền vào hợp lệ thì trả về true, ngược lại trả về false
    public class RegularExpression
    {
        public static bool laTenTaiKhoan(string s)
        {
            string sPattern = @"^[a-zA-Z][a-zA-Z0-9]*$";
            return Regex.IsMatch(s, sPattern) && (s != "");
        }

        public static bool laMatKhau(string s)
        {
            string sPattern = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{0,}$";
            return Regex.IsMatch(s, sPattern) && (s != "");
        }
        
        public static bool laHoTen(string s)
        {
            string sPattern = @"^[a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ ]+$";
            return Regex.IsMatch(s, sPattern) && (s != "");
        }

        public static bool laEmail(string s)
        {
            string sPattern = @"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)";
            return Regex.IsMatch(s, sPattern, RegexOptions.IgnoreCase) && (s != "");
        }

        public static bool laSo(string s)
        {
            string sPattern = @"^[0-9]*$";
            return Regex.IsMatch(s, sPattern, RegexOptions.IgnoreCase) && (s != "");
        }
    }
}
