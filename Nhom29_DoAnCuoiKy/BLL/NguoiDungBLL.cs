﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAL;

namespace BLL
{
    public class NguoiDungBLL
    {
        public NguoiDungDTO layNguoiDung(string tenDangNhap, string matKhau)
        {
            NguoiDungDAL dal = new NguoiDungDAL();
            NguoiDungDTO nd = dal.layNguoiDung(tenDangNhap, matKhau);
            return nd;
        }

        public NguoiDungDTO layNguoiDung(string tenNguoiDung)
        {
            NguoiDungDAL dal = new NguoiDungDAL();
            NguoiDungDTO nd = dal.layNguoiDung(tenNguoiDung);
            return nd;
        }

        public List<NguoiDungDTO> layDSNguoiDung()
        {
            NguoiDungDAL dal = new NguoiDungDAL();
            List<NguoiDungDTO> ds = dal.layDSNguoiDung();
            return ds;
        }

        public int themNguoiDung(NguoiDungDTO nd)
        {
            NguoiDungDAL dal = new NguoiDungDAL();
            return dal.themNguoiDung(nd);
        }

        public int capNhatNguoiDung(NguoiDungDTO nd)
        {
            NguoiDungDAL dal = new NguoiDungDAL();
            return dal.capNhatNguoiDung(nd);
        }

        public int xoaNguoiDung(NguoiDungDTO nd)
        {
            NguoiDungDAL dal = new NguoiDungDAL();
            return dal.xoaNguoiDung(nd);
        }
    }
}
