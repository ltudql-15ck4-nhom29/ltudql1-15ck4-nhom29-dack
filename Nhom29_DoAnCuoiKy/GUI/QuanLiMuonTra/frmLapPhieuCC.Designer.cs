﻿namespace GUI
{
    partial class frmLapPhieuCC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cboMaDG = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cboHinhThucPhat = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lblTenDG = new System.Windows.Forms.Label();
            this.btnLapPhieuCC = new System.Windows.Forms.Button();
            this.btnQuayVe = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.lblNgungDichVu = new System.Windows.Forms.Label();
            this.lblNgayLapPhieu = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblHinhThucPhatHienTai = new System.Windows.Forms.Label();
            this.lblSoTienPhat = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cboMaDG
            // 
            this.cboMaDG.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboMaDG.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboMaDG.FormattingEnabled = true;
            this.cboMaDG.Location = new System.Drawing.Point(193, 27);
            this.cboMaDG.Name = "cboMaDG";
            this.cboMaDG.Size = new System.Drawing.Size(208, 21);
            this.cboMaDG.TabIndex = 3;
            this.cboMaDG.SelectedIndexChanged += new System.EventHandler(this.cboMaDG_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(49, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Mã độc giả:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(49, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Tên độc giả:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(49, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Ngày lập phiếu:";
            // 
            // cboHinhThucPhat
            // 
            this.cboHinhThucPhat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboHinhThucPhat.FormattingEnabled = true;
            this.cboHinhThucPhat.Location = new System.Drawing.Point(193, 135);
            this.cboHinhThucPhat.Name = "cboHinhThucPhat";
            this.cboHinhThucPhat.Size = new System.Drawing.Size(208, 21);
            this.cboHinhThucPhat.TabIndex = 9;
            this.cboHinhThucPhat.SelectedIndexChanged += new System.EventHandler(this.cboHinhThucPhat_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(49, 138);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Hình thức phạt:";
            // 
            // lblTenDG
            // 
            this.lblTenDG.AutoSize = true;
            this.lblTenDG.Location = new System.Drawing.Point(190, 57);
            this.lblTenDG.Name = "lblTenDG";
            this.lblTenDG.Size = new System.Drawing.Size(38, 13);
            this.lblTenDG.TabIndex = 11;
            this.lblTenDG.Text = "tenDG";
            // 
            // btnLapPhieuCC
            // 
            this.btnLapPhieuCC.Location = new System.Drawing.Point(262, 231);
            this.btnLapPhieuCC.Name = "btnLapPhieuCC";
            this.btnLapPhieuCC.Size = new System.Drawing.Size(122, 38);
            this.btnLapPhieuCC.TabIndex = 13;
            this.btnLapPhieuCC.Text = "Lập phiếu cảnh cáo";
            this.btnLapPhieuCC.UseVisualStyleBackColor = true;
            this.btnLapPhieuCC.Click += new System.EventHandler(this.btnLapPhieuCC_Click);
            // 
            // btnQuayVe
            // 
            this.btnQuayVe.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnQuayVe.Location = new System.Drawing.Point(52, 231);
            this.btnQuayVe.Name = "btnQuayVe";
            this.btnQuayVe.Size = new System.Drawing.Size(122, 38);
            this.btnQuayVe.TabIndex = 14;
            this.btnQuayVe.Text = "Quay về";
            this.btnQuayVe.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(49, 192);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(128, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Ngưng cung cấp dịch vụ:";
            // 
            // lblNgungDichVu
            // 
            this.lblNgungDichVu.AutoSize = true;
            this.lblNgungDichVu.Location = new System.Drawing.Point(190, 192);
            this.lblNgungDichVu.Name = "lblNgungDichVu";
            this.lblNgungDichVu.Size = new System.Drawing.Size(116, 13);
            this.lblNgungDichVu.TabIndex = 16;
            this.lblNgungDichVu.Text = "ngungCungCapDichVu";
            // 
            // lblNgayLapPhieu
            // 
            this.lblNgayLapPhieu.AutoSize = true;
            this.lblNgayLapPhieu.Location = new System.Drawing.Point(190, 84);
            this.lblNgayLapPhieu.Name = "lblNgayLapPhieu";
            this.lblNgayLapPhieu.Size = new System.Drawing.Size(75, 13);
            this.lblNgayLapPhieu.TabIndex = 17;
            this.lblNgayLapPhieu.Text = "ngayLapPhieu";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(49, 109);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Đang chịu hình thức phạt:";
            // 
            // lblHinhThucPhatHienTai
            // 
            this.lblHinhThucPhatHienTai.AutoSize = true;
            this.lblHinhThucPhatHienTai.Location = new System.Drawing.Point(190, 109);
            this.lblHinhThucPhatHienTai.Name = "lblHinhThucPhatHienTai";
            this.lblHinhThucPhatHienTai.Size = new System.Drawing.Size(111, 13);
            this.lblHinhThucPhatHienTai.TabIndex = 19;
            this.lblHinhThucPhatHienTai.Text = "hinhThucPhatHienTai";
            // 
            // lblSoTienPhat
            // 
            this.lblSoTienPhat.AutoSize = true;
            this.lblSoTienPhat.Location = new System.Drawing.Point(190, 168);
            this.lblSoTienPhat.Name = "lblSoTienPhat";
            this.lblSoTienPhat.Size = new System.Drawing.Size(61, 13);
            this.lblSoTienPhat.TabIndex = 21;
            this.lblSoTienPhat.Text = "soTienPhat";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(49, 168);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 13);
            this.label8.TabIndex = 20;
            this.label8.Text = "Số tiền phạt:";
            // 
            // frmLapPhieuCC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(430, 291);
            this.Controls.Add(this.lblSoTienPhat);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lblHinhThucPhatHienTai);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblNgayLapPhieu);
            this.Controls.Add(this.lblNgungDichVu);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnQuayVe);
            this.Controls.Add(this.btnLapPhieuCC);
            this.Controls.Add(this.lblTenDG);
            this.Controls.Add(this.cboHinhThucPhat);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cboMaDG);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmLapPhieuCC";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Lập phiếu cảnh cáo";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cboMaDG;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cboHinhThucPhat;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblTenDG;
        private System.Windows.Forms.Button btnLapPhieuCC;
        private System.Windows.Forms.Button btnQuayVe;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblNgungDichVu;
        private System.Windows.Forms.Label lblNgayLapPhieu;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblHinhThucPhatHienTai;
        private System.Windows.Forms.Label lblSoTienPhat;
        private System.Windows.Forms.Label label8;
    }
}