﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BLL;

namespace GUI
{
    public partial class frmLapPhieuCC : Form
    {
        List<DocGiaDTO> lstDocGia = null;
        List<HinhThucPhatDTO> lstHinhThucPhat = null;
        DocGiaDTO docGia = null;
        DateTime ngayLapPhieu = DateTime.Now.Date;

        public frmLapPhieuCC(DocGiaDTO dg)
        {
            InitializeComponent();
            docGia = dg;

            napDSDocGia();
            napDSHinhThucPhat();

            hienThiCboDocGia();
            hienThiCboHinhThucPhat();
        }

        void napDSDocGia()
        {
            DocGiaBLL bll = new DocGiaBLL();
            lstDocGia = bll.layDSDocGia();
        }

        void napDSHinhThucPhat()
        {
            HinhThucPhatBLL bll = new HinhThucPhatBLL();
            lstHinhThucPhat = bll.layDSHinhThucPhat();
        }

        void hienThiCboDocGia()
        {
            cboMaDG.Items.Clear();
            for(int i = 0; i < lstDocGia.Count; i++)
            {
                cboMaDG.Items.Add(lstDocGia[i]);
                if(lstDocGia[i].MaDocGia == docGia.MaDocGia)
                {
                    cboMaDG.SelectedIndex = i;
                }
            }
        }

        void hienThiCboHinhThucPhat()
        {
            cboHinhThucPhat.Items.Clear();
            for(int i = 0; i < lstHinhThucPhat.Count; i++)
            {
                if(lstHinhThucPhat[i].MaHinhThucPhat > docGia.MaHinhThucPhat)
                {
                    cboHinhThucPhat.Items.Add(lstHinhThucPhat[i]);
                }
            }
        }

        void hienThiThongTinDocGia()
        {
            lblTenDG.Text = docGia.HoTen;
            lblNgayLapPhieu.Text = ngayLapPhieu.ToString("dd/MM/yyyy");
            HinhThucPhatDTO htp = HinhThucPhatDTO.layHinhThucPhatTheoDocGia(docGia, lstHinhThucPhat);
            lblHinhThucPhatHienTai.Text = htp.ToString();
            lblSoTienPhat.Text = "0 VNĐ";
            lblNgungDichVu.Text = "";
            cboHinhThucPhat_SelectedIndexChanged(this, new EventArgs());
        }

        PhieuCanhCaoDTO taoPhieuCanhCao()
        {
            PhieuCanhCaoDTO pcc = new PhieuCanhCaoDTO();
            DocGiaDTO dg = (DocGiaDTO)cboMaDG.SelectedItem;
            HinhThucPhatDTO htp = (HinhThucPhatDTO)cboHinhThucPhat.SelectedItem;
            pcc.MaDocGia = dg.MaDocGia;
            pcc.MaHinhThucPhat = htp.MaHinhThucPhat;
            pcc.NgayLapPhieu = DateTime.Now.Date;
            return pcc;
        }

        private void cboHinhThucPhat_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cboHinhThucPhat.SelectedIndex > -1)
            {
                HinhThucPhatDTO htp = (HinhThucPhatDTO)cboHinhThucPhat.SelectedItem;
                lblSoTienPhat.Text = String.Format("{0:n0}", htp.SoTienPhat) + " VNĐ";
                if(htp.NgungCungCapDichVu)
                {
                    lblNgungDichVu.Text = "Có";
                }
                else
                {
                    lblNgungDichVu.Text = "Không";
                }
            }
        }

        private void cboMaDG_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cboMaDG.SelectedIndex > -1)
            {
                docGia = (DocGiaDTO)cboMaDG.SelectedItem;
                hienThiCboHinhThucPhat();
                hienThiThongTinDocGia();
            }
        }

        private void btnLapPhieuCC_Click(object sender, EventArgs e)
        {
            if(cboMaDG.SelectedIndex == -1)
            {
                MessageBox.Show("Chưa có độc giả nào được chọn!", "Lập phiếu cảnh cáo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if(cboHinhThucPhat.SelectedIndex == -1)
            {
                MessageBox.Show("Chưa có hình thức phạt nào được chọn!", "Lập phiếu cảnh cáo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (DialogResult.Yes == MessageBox.Show("Bạn có chắc muốn lập phiếu cảnh cáo cho độc giả này?", "Lập phiếu cảnh cáo", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                docGia.MaHinhThucPhat = ((HinhThucPhatDTO)cboHinhThucPhat.SelectedItem).MaHinhThucPhat;
                DocGiaBLL bll = new DocGiaBLL();
                if(bll.capNhatHinhThucPhat(docGia) == 1)
                {
                    PhieuCanhCaoBLL bll2 = new PhieuCanhCaoBLL();
                    PhieuCanhCaoDTO pcc = taoPhieuCanhCao();
                    bll2.themPhieuCanhCao(pcc);
                    MessageBox.Show("Lập phiếu cảnh cáo thành công!", "Lập phiếu cảnh cáo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.Yes;
                    Close();
                }
                else
                {
                    MessageBox.Show("Lập phiếu cảnh cáo thất bại!", "Lập phiếu cảnh cáo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
