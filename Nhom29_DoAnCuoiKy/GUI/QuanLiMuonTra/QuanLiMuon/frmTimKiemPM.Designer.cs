﻿namespace GUI
{
    partial class frmTimKiemPM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnXemHet = new System.Windows.Forms.Button();
            this.lblTimKiemChuaTra = new System.Windows.Forms.Label();
            this.lblTimKiemQuaHan = new System.Windows.Forms.Label();
            this.btnTimKiem = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNoiDungLoc = new System.Windows.Forms.TextBox();
            this.cboLoaiBoLoc = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lvDSPM = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnXemChiTiet = new System.Windows.Forms.Button();
            this.btnXoa = new System.Windows.Forms.Button();
            this.btnLapPhieuTra = new System.Windows.Forms.Button();
            this.btnQuayVe = new System.Windows.Forms.Button();
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnXemHet);
            this.groupBox1.Controls.Add(this.lblTimKiemChuaTra);
            this.groupBox1.Controls.Add(this.lblTimKiemQuaHan);
            this.groupBox1.Controls.Add(this.btnTimKiem);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtNoiDungLoc);
            this.groupBox1.Controls.Add(this.cboLoaiBoLoc);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(631, 172);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Bộ lọc tìm kiếm";
            // 
            // btnXemHet
            // 
            this.btnXemHet.Location = new System.Drawing.Point(229, 72);
            this.btnXemHet.Name = "btnXemHet";
            this.btnXemHet.Size = new System.Drawing.Size(100, 31);
            this.btnXemHet.TabIndex = 7;
            this.btnXemHet.Text = "Xem hết";
            this.btnXemHet.UseVisualStyleBackColor = true;
            this.btnXemHet.Click += new System.EventHandler(this.btnXemHet_Click);
            // 
            // lblTimKiemChuaTra
            // 
            this.lblTimKiemChuaTra.AutoSize = true;
            this.lblTimKiemChuaTra.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTimKiemChuaTra.ForeColor = System.Drawing.Color.Blue;
            this.lblTimKiemChuaTra.Location = new System.Drawing.Point(237, 137);
            this.lblTimKiemChuaTra.Name = "lblTimKiemChuaTra";
            this.lblTimKiemChuaTra.Size = new System.Drawing.Size(202, 13);
            this.lblTimKiemChuaTra.TabIndex = 6;
            this.lblTimKiemChuaTra.Text = "Xem danh sách các phiếu mượn chưa trả";
            this.lblTimKiemChuaTra.Click += new System.EventHandler(this.lblTimKiemChuaTra_Click);
            // 
            // lblTimKiemQuaHan
            // 
            this.lblTimKiemQuaHan.AutoSize = true;
            this.lblTimKiemQuaHan.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTimKiemQuaHan.ForeColor = System.Drawing.Color.Blue;
            this.lblTimKiemQuaHan.Location = new System.Drawing.Point(237, 115);
            this.lblTimKiemQuaHan.Name = "lblTimKiemQuaHan";
            this.lblTimKiemQuaHan.Size = new System.Drawing.Size(202, 13);
            this.lblTimKiemQuaHan.TabIndex = 5;
            this.lblTimKiemQuaHan.Text = "Xem danh sách các phiếu mượn quá hạn";
            this.lblTimKiemQuaHan.Click += new System.EventHandler(this.lblTimKiemQuaHan_Click);
            // 
            // btnTimKiem
            // 
            this.btnTimKiem.Location = new System.Drawing.Point(339, 72);
            this.btnTimKiem.Name = "btnTimKiem";
            this.btnTimKiem.Size = new System.Drawing.Size(100, 31);
            this.btnTimKiem.TabIndex = 4;
            this.btnTimKiem.Text = "Tìm kiếm";
            this.btnTimKiem.UseVisualStyleBackColor = true;
            this.btnTimKiem.Click += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(170, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Nội dung:";
            // 
            // txtNoiDungLoc
            // 
            this.txtNoiDungLoc.Location = new System.Drawing.Point(229, 46);
            this.txtNoiDungLoc.Name = "txtNoiDungLoc";
            this.txtNoiDungLoc.Size = new System.Drawing.Size(210, 20);
            this.txtNoiDungLoc.TabIndex = 2;
            // 
            // cboLoaiBoLoc
            // 
            this.cboLoaiBoLoc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboLoaiBoLoc.FormattingEnabled = true;
            this.cboLoaiBoLoc.Items.AddRange(new object[] {
            "Mã phiếu mượn",
            "Mã độc giả"});
            this.cboLoaiBoLoc.Location = new System.Drawing.Point(229, 19);
            this.cboLoaiBoLoc.Name = "cboLoaiBoLoc";
            this.cboLoaiBoLoc.Size = new System.Drawing.Size(210, 21);
            this.cboLoaiBoLoc.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(147, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tìm kiếm theo:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lvDSPM);
            this.groupBox2.Location = new System.Drawing.Point(12, 190);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(631, 256);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Danh sách phiếu mượn";
            // 
            // lvDSPM
            // 
            this.lvDSPM.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader6,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.lvDSPM.FullRowSelect = true;
            this.lvDSPM.Location = new System.Drawing.Point(6, 19);
            this.lvDSPM.MultiSelect = false;
            this.lvDSPM.Name = "lvDSPM";
            this.lvDSPM.Size = new System.Drawing.Size(619, 231);
            this.lvDSPM.TabIndex = 0;
            this.lvDSPM.UseCompatibleStateImageBehavior = false;
            this.lvDSPM.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Mã phiếu mượn";
            this.columnHeader1.Width = 90;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Tên độc giả";
            this.columnHeader2.Width = 160;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Ngày lập phiếu";
            this.columnHeader3.Width = 110;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Quá hạn";
            this.columnHeader4.Width = 80;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Chưa trả";
            this.columnHeader5.Width = 80;
            // 
            // btnXemChiTiet
            // 
            this.btnXemChiTiet.Location = new System.Drawing.Point(504, 452);
            this.btnXemChiTiet.Name = "btnXemChiTiet";
            this.btnXemChiTiet.Size = new System.Drawing.Size(139, 32);
            this.btnXemChiTiet.TabIndex = 3;
            this.btnXemChiTiet.Text = "Xem chi tiết";
            this.btnXemChiTiet.UseVisualStyleBackColor = true;
            this.btnXemChiTiet.Click += new System.EventHandler(this.btnXemChiTiet_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Location = new System.Drawing.Point(359, 452);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(139, 32);
            this.btnXoa.TabIndex = 4;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.UseVisualStyleBackColor = true;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnLapPhieuTra
            // 
            this.btnLapPhieuTra.Location = new System.Drawing.Point(214, 452);
            this.btnLapPhieuTra.Name = "btnLapPhieuTra";
            this.btnLapPhieuTra.Size = new System.Drawing.Size(139, 32);
            this.btnLapPhieuTra.TabIndex = 5;
            this.btnLapPhieuTra.Text = "Lập phiếu trả";
            this.btnLapPhieuTra.UseVisualStyleBackColor = true;
            this.btnLapPhieuTra.Click += new System.EventHandler(this.btnLapPhieuTra_Click);
            // 
            // btnQuayVe
            // 
            this.btnQuayVe.Location = new System.Drawing.Point(12, 452);
            this.btnQuayVe.Name = "btnQuayVe";
            this.btnQuayVe.Size = new System.Drawing.Size(139, 32);
            this.btnQuayVe.TabIndex = 6;
            this.btnQuayVe.Text = "Quay về";
            this.btnQuayVe.UseVisualStyleBackColor = true;
            this.btnQuayVe.Click += new System.EventHandler(this.btnQuayVe_Click);
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Mã độc giả";
            this.columnHeader6.Width = 70;
            // 
            // frmTimKiemPM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(660, 501);
            this.Controls.Add(this.btnQuayVe);
            this.Controls.Add(this.btnLapPhieuTra);
            this.Controls.Add(this.btnXoa);
            this.Controls.Add(this.btnXemChiTiet);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmTimKiemPM";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tìm kiếm phiếu mượn";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmTimKiemPM_FormClosed);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblTimKiemChuaTra;
        private System.Windows.Forms.Label lblTimKiemQuaHan;
        private System.Windows.Forms.Button btnTimKiem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNoiDungLoc;
        private System.Windows.Forms.ComboBox cboLoaiBoLoc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListView lvDSPM;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.Button btnXemChiTiet;
        private System.Windows.Forms.Button btnXoa;
        private System.Windows.Forms.Button btnLapPhieuTra;
        private System.Windows.Forms.Button btnQuayVe;
        private System.Windows.Forms.Button btnXemHet;
        private System.Windows.Forms.ColumnHeader columnHeader6;
    }
}