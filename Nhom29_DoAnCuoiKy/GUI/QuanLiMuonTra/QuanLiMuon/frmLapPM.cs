﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BLL;


namespace GUI
{
    public partial class frmLapPM : Form
    {
        List<DocGiaDTO> lstDocGia = null;
        List<LoaiDocGiaDTO> lstLoaiDocGia = null;
        List<TaiLieuDTO> lstTaiLieu = null;
        List<HinhThucPhatDTO> lstHinhThucPhat = null;

        int soTaiLieuChuaMuon = 0;

        List<BanSaoDTO> lstBanSaoMuon = new List<BanSaoDTO>();

        DateTime ngayLap = DateTime.Now.Date;

        public frmLapPM()
        {
            InitializeComponent();
            lblNgayLapPhieu.Text = ngayLap.ToString("dd/MM/yyyy");
            lblSoLuongChuaMuon.Text = "";
            lblChoGVCB.Text = "";
            lblLaSachHiem.Text = "";
            lblLaGVCB.Text = "";
            
            napDSDocGia();
            napDSTaiLieu();
            napDSLoaiDocGia();
            napDSHinhThucPhat();

            hienThiCboDSDocgia();
            hienthiCboDSTaiLieu();
        }

        private void btnQuayVe_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmLapPM_FormClosed(object sender, FormClosedEventArgs e)
        {
            Owner.Show();
        }

        void napDSDocGia()
        {
            DocGiaBLL bll = new DocGiaBLL();
            lstDocGia = bll.layDSDocGiaDayDuCoQuaHan();
        }

        void napDSLoaiDocGia()
        {
            LoaiDocGiaBLL bll = new LoaiDocGiaBLL();
            lstLoaiDocGia = bll.layDSLoaiDocGia();
        }

        void napDSTaiLieu()
        {
            TaiLieuBLL bll = new TaiLieuBLL();
            lstTaiLieu = bll.layDSTaiLieuDayDu();
        }

        void napDSHinhThucPhat()
        {
            HinhThucPhatBLL bll = new HinhThucPhatBLL();
            lstHinhThucPhat = bll.layDSHinhThucPhat();
        }

        void hienThiCboDSDocgia()
        {
            cboMaDG.Items.Clear();
            for (int i = 0; i < lstDocGia.Count; i++)
            {
                cboMaDG.Items.Add(lstDocGia[i]);                   
            }
        }

        void hienthiCboDSTaiLieu()
        {
            cboMaTL.Items.Clear();
            for(int i=0;i<lstTaiLieu.Count;i++)
            {
                cboMaTL.Items.Add(lstTaiLieu[i]);
                cboTenTL.Items.Add(lstTaiLieu[i]);
            }
        }
        
        void hienThiLvDSTL()
        {
            lvDSTL.Items.Clear();
            for (int i = 0; i < lstBanSaoMuon.Count; i++)
            {
                BanSaoDTO bs = lstBanSaoMuon[i];
                TaiLieuDTO tl = TaiLieuDTO.layTaiLieuTheoBanSao(bs, lstTaiLieu);
                ListViewItem item = new ListViewItem(tl.MaTaiLieu.ToString());
                item.SubItems.Add(bs.MaBanSao.ToString());
                item.SubItems.Add(tl.TenTaiLieu);
                if(tl.ChoGVCB == true)
                {
                    item.SubItems.Add("Có");
                }
                else
                {
                    item.SubItems.Add("Không");
                }
                lvDSTL.Items.Add(item);
            }
        }
        
        PhieuMuonDTO taoPhieuMuon()
        {
            PhieuMuonDTO pm = new PhieuMuonDTO();
            pm.NgayLapPhieu = ngayLap;
            pm.MaDocGia = ((DocGiaDTO)cboMaDG.SelectedItem).MaDocGia;
            pm.LstCTPM = taoDSChiTietPhieuMuonTuDSBanSao();
            return pm;
        }

        List<ChiTietPhieuMuonDTO> taoDSChiTietPhieuMuonTuDSBanSao()
        {
            DocGiaDTO dg = (DocGiaDTO)cboMaDG.SelectedItem;
            LoaiDocGiaDTO ldg = LoaiDocGiaDTO.layLoaiDocGiaTheoDocGia(dg, lstLoaiDocGia);
            List<ChiTietPhieuMuonDTO> ds = new List<ChiTietPhieuMuonDTO>();
            for(int i = 0; i < lstBanSaoMuon.Count; i++)
            {
                ChiTietPhieuMuonDTO ctpm = new ChiTietPhieuMuonDTO();
                ctpm.MaBanSao = lstBanSaoMuon[i].MaBanSao;
                ctpm.HanChotTra = DateTime.Now.Date.AddDays(Convert.ToDouble(ldg.SoNgayMuonMax));
                ds.Add(ctpm);
            }
            return ds;
        }

        bool kiemTraTaiLieuDaMuon()
        {
            List<PhieuMuonDTO> lst1 = ((DocGiaDTO)cboMaDG.SelectedItem).LstPM;
            for(int i = 0; i < lst1.Count; i++)
            {
                List<ChiTietPhieuMuonDTO> lst2 = lst1[i].LstCTPM;
                for (int j = 0; j < lst2.Count; j++)
                {
                    ChiTietPhieuMuonDTO ctpm = lst2[j];
                    if(ctpm.SoThuTuCTPT == -1)
                    {
                        TaiLieuDTO tl = TaiLieuDTO.layTaiLieuTheoChiTietPhieuMuon(ctpm, lstTaiLieu);
                        if(tl == lstTaiLieu[cboMaTL.SelectedIndex])
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private void cboMaTL_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboTenTL.SelectedIndex = cboMaTL.SelectedIndex;
            if(cboTenTL.SelectedIndex > -1)
            {
                TaiLieuDTO tl = (TaiLieuDTO)cboTenTL.SelectedItem;
                if(tl.ChoGVCB)
                {
                    lblChoGVCB.Text = "có";
                }
                else
                {
                    lblChoGVCB.Text = "không";
                }
                if(tl.LaSachHiem)
                {
                    lblLaSachHiem.Text = "Có";
                }
                else
                {
                    lblLaSachHiem.Text = "Không";
                }
            }
        }

        private void cboTenTL_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboMaTL.SelectedIndex = cboTenTL.SelectedIndex;
        }

        private void cboMaDG_SelectedIndexChanged(object sender, EventArgs e)
        {
            soTaiLieuChuaMuon = 0;
            if(cboMaDG.SelectedIndex > -1)
            {
                DocGiaDTO dg = (DocGiaDTO) cboMaDG.SelectedItem;
                HinhThucPhatDTO htp = HinhThucPhatDTO.layHinhThucPhatTheoDocGia(dg, lstHinhThucPhat);
                if (!htp.NgungCungCapDichVu)
                {
                    LoaiDocGiaDTO ldg = LoaiDocGiaDTO.layLoaiDocGiaTheoDocGia(dg, lstLoaiDocGia);
                    soTaiLieuChuaMuon = ldg.SoTaiLieuMuonMax - dg.SoTaiLieuDangMuon;
                    if(ldg.LaGVCB)
                    {
                        lblLaGVCB.Text = "có";
                    }
                    else
                    {
                        lblLaGVCB.Text = "không";
                    }
                }
                else
                {
                    MessageBox.Show("Đã ngưng cung cáp dịch vụ cho độc giả này!", "Chọn độc giả", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cboMaDG.SelectedIndex = -1;
                }
            }
            lblSoLuongChuaMuon.Text = soTaiLieuChuaMuon.ToString();
            lstBanSaoMuon.Clear();
            lvDSTL.Items.Clear();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            if (cboMaTL.SelectedIndex == -1)
            {
                MessageBox.Show("Không có tài liệu nào được chọn!", "Thêm tài liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if(soTaiLieuChuaMuon == 0)
            {
                MessageBox.Show("Số lượng tài liệu có thể mượn phải lớn hơn 0!", "Thêm tài liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (kiemTraTaiLieuDaMuon())
            {
                MessageBox.Show("Tài liệu này đang được mượn bởi độc giả này!", "Thêm tài liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            TaiLieuDTO tl = (TaiLieuDTO) cboMaTL.SelectedItem;
            for(int i = 0; i < lstBanSaoMuon.Count; i++)
            {
                if(lstBanSaoMuon[i].MaTaiLieu == tl.MaTaiLieu)
                {
                    MessageBox.Show("Tài liệu này đã được chọn!", "Thêm tài liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

            }       
            if(tl.LaSachHiem == true)
            {
                MessageBox.Show("Tài liệu là sách hiếm nên không thể mượn!", "Thêm tài liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DocGiaDTO dg = (DocGiaDTO) cboMaDG.SelectedItem;
            LoaiDocGiaDTO ldg = LoaiDocGiaDTO.layLoaiDocGiaTheoDocGia(dg, lstLoaiDocGia);
            if(tl.ChoGVCB == true && ldg.LaGVCB == false)
            {
                MessageBox.Show("Tài liệu chỉ dành cho Giảng viên/Cán bộ!", "Thêm tài liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            List<BanSaoDTO> lstBanSao = ((TaiLieuDTO) cboMaTL.SelectedItem).LstBanSao;
            BanSaoDTO banSaoMuon = null;
            for(int i = 0; i < lstBanSao.Count; i++)
            {
                if(lstBanSao[i].MaDocGiaDangMuon == -1)
                {
                    banSaoMuon = lstBanSao[i];
                    break;
                }
            }
            if(banSaoMuon == null)
            {
                MessageBox.Show("Tất cả các bản sao của tài liệu này đã được mượn!", "Thêm tài liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            soTaiLieuChuaMuon--;
            lblSoLuongChuaMuon.Text = soTaiLieuChuaMuon.ToString();
            lstBanSaoMuon.Add(banSaoMuon);
            hienThiLvDSTL();
        }

        private void btnLap_Click(object sender, EventArgs e)
        {
            if(cboMaDG.SelectedIndex == -1)
            {
                MessageBox.Show("Chưa có độc giả nào được chọn!", "Lập phiếu mượn", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if(lstBanSaoMuon.Count == 0)
            {
                MessageBox.Show("Danh sách tài liệu mượn trống!", "Lập phiếu mượn", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if(DialogResult.Yes == MessageBox.Show("Bạn có chắc muốn lập phiếu mượn hay không?", "Lập phiếu mượn", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                PhieuMuonDTO pm = taoPhieuMuon();
                PhieuMuonBLL bll1 = new PhieuMuonBLL();
                bll1.themPhieuMuon(pm);
                DocGiaDTO dg = (DocGiaDTO)cboMaDG.SelectedItem;
                dg.SoTaiLieuDangMuon += lstBanSaoMuon.Count;
                DocGiaBLL bll2 = new DocGiaBLL();
                bll2.capNhatSoTLDangMuon(dg);
                MessageBox.Show("Đã lập phiếu mượn thành công!", "Lập phiếu mượn", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (lvDSTL.SelectedItems.Count > 0)
            {
                if (DialogResult.Yes == MessageBox.Show("Bạn có chắc muốn xóa tài liệu này khỏi danh sách mượn không?", "xóa tài Liệu", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                {
                    soTaiLieuChuaMuon++;
                    lblSoLuongChuaMuon.Text = soTaiLieuChuaMuon.ToString();
                    lstBanSaoMuon.RemoveAt(lvDSTL.SelectedIndices[0]);
                    hienThiLvDSTL();
                    return;
                }
            }
            else
            {
                MessageBox.Show("Không có tài liệu nào được chọn!", "Xóa tài liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void btnChiTietLoaiDG_Click(object sender, EventArgs e)
        {
            if(cboMaDG.SelectedIndex == -1)
            {
                MessageBox.Show("Không có độc giả nào được chọn!", "Chi tiết loại độc giả", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DocGiaDTO dg = (DocGiaDTO) cboMaDG.SelectedItem;
            LoaiDocGiaDTO ldg = LoaiDocGiaDTO.layLoaiDocGiaTheoDocGia(dg, lstLoaiDocGia);
            frmChiTietLoaiDG f = new frmChiTietLoaiDG(ldg);
            f.ShowDialog();
        }
    }
}
