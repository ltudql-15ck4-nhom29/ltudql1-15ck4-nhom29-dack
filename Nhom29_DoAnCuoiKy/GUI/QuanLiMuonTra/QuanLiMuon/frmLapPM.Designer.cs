﻿namespace GUI
{
    partial class frmLapPM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblLaSachHiem = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblChoGVCB = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnXoa = new System.Windows.Forms.Button();
            this.btnThem = new System.Windows.Forms.Button();
            this.lvDSTL = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cboTenTL = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cboMaTL = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnLap = new System.Windows.Forms.Button();
            this.btnQuayVe = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblNgayLapPhieu = new System.Windows.Forms.Label();
            this.cboMaDG = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnChiTietLoaiDG = new System.Windows.Forms.Button();
            this.lblLaGVCB = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblSoLuongChuaMuon = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblLaSachHiem);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.lblChoGVCB);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.btnXoa);
            this.groupBox2.Controls.Add(this.btnThem);
            this.groupBox2.Controls.Add(this.lvDSTL);
            this.groupBox2.Controls.Add(this.cboTenTL);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.cboMaTL);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(12, 153);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(541, 408);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Danh sách tài liệu mượn";
            // 
            // lblLaSachHiem
            // 
            this.lblLaSachHiem.AutoSize = true;
            this.lblLaSachHiem.Location = new System.Drawing.Point(166, 98);
            this.lblLaSachHiem.Name = "lblLaSachHiem";
            this.lblLaSachHiem.Size = new System.Drawing.Size(64, 13);
            this.lblLaSachHiem.TabIndex = 12;
            this.lblLaSachHiem.Text = "laSachHiem";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(87, 98);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Là sách hiếm:";
            // 
            // lblChoGVCB
            // 
            this.lblChoGVCB.AutoSize = true;
            this.lblChoGVCB.Location = new System.Drawing.Point(166, 76);
            this.lblChoGVCB.Name = "lblChoGVCB";
            this.lblChoGVCB.Size = new System.Drawing.Size(54, 13);
            this.lblChoGVCB.TabIndex = 10;
            this.lblChoGVCB.Text = "choGVCB";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(66, 76);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Dành cho GV/CB:";
            // 
            // btnXoa
            // 
            this.btnXoa.Location = new System.Drawing.Point(430, 366);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(105, 31);
            this.btnXoa.TabIndex = 6;
            this.btnXoa.Text = "Xóa tài liệu";
            this.btnXoa.UseVisualStyleBackColor = true;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnThem
            // 
            this.btnThem.Location = new System.Drawing.Point(395, 21);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(98, 48);
            this.btnThem.TabIndex = 5;
            this.btnThem.Text = "Thêm tài liệu";
            this.btnThem.UseVisualStyleBackColor = true;
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // lvDSTL
            // 
            this.lvDSTL.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.lvDSTL.FullRowSelect = true;
            this.lvDSTL.Location = new System.Drawing.Point(6, 126);
            this.lvDSTL.MultiSelect = false;
            this.lvDSTL.Name = "lvDSTL";
            this.lvDSTL.Size = new System.Drawing.Size(529, 234);
            this.lvDSTL.TabIndex = 4;
            this.lvDSTL.UseCompatibleStateImageBehavior = false;
            this.lvDSTL.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Mã tài liệu";
            this.columnHeader1.Width = 80;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Mã bản sao";
            this.columnHeader2.Width = 80;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Tên tài liệu";
            this.columnHeader3.Width = 250;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Cho GV/CB";
            this.columnHeader4.Width = 90;
            // 
            // cboTenTL
            // 
            this.cboTenTL.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboTenTL.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboTenTL.DisplayMember = "tenTaiLieu";
            this.cboTenTL.FormattingEnabled = true;
            this.cboTenTL.Location = new System.Drawing.Point(169, 48);
            this.cboTenTL.Name = "cboTenTL";
            this.cboTenTL.Size = new System.Drawing.Size(210, 21);
            this.cboTenTL.TabIndex = 3;
            this.cboTenTL.SelectedIndexChanged += new System.EventHandler(this.cboTenTL_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(98, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Tên tài liệu:";
            // 
            // cboMaTL
            // 
            this.cboMaTL.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboMaTL.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboMaTL.DisplayMember = "maTaiLieu";
            this.cboMaTL.FormattingEnabled = true;
            this.cboMaTL.Location = new System.Drawing.Point(169, 21);
            this.cboMaTL.Name = "cboMaTL";
            this.cboMaTL.Size = new System.Drawing.Size(107, 21);
            this.cboMaTL.TabIndex = 1;
            this.cboMaTL.SelectedIndexChanged += new System.EventHandler(this.cboMaTL_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(102, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Mã tài liệu:";
            // 
            // btnLap
            // 
            this.btnLap.Location = new System.Drawing.Point(407, 567);
            this.btnLap.Name = "btnLap";
            this.btnLap.Size = new System.Drawing.Size(146, 41);
            this.btnLap.TabIndex = 2;
            this.btnLap.Text = "Lập phiếu mượn";
            this.btnLap.UseVisualStyleBackColor = true;
            this.btnLap.Click += new System.EventHandler(this.btnLap_Click);
            // 
            // btnQuayVe
            // 
            this.btnQuayVe.Location = new System.Drawing.Point(12, 567);
            this.btnQuayVe.Name = "btnQuayVe";
            this.btnQuayVe.Size = new System.Drawing.Size(146, 41);
            this.btnQuayVe.TabIndex = 3;
            this.btnQuayVe.Text = "Quay về";
            this.btnQuayVe.UseVisualStyleBackColor = true;
            this.btnQuayVe.Click += new System.EventHandler(this.btnQuayVe_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(79, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Ngày lập phiếu:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(113, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Độc giả:";
            // 
            // lblNgayLapPhieu
            // 
            this.lblNgayLapPhieu.AutoSize = true;
            this.lblNgayLapPhieu.Location = new System.Drawing.Point(166, 30);
            this.lblNgayLapPhieu.Name = "lblNgayLapPhieu";
            this.lblNgayLapPhieu.Size = new System.Drawing.Size(75, 13);
            this.lblNgayLapPhieu.TabIndex = 7;
            this.lblNgayLapPhieu.Text = "ngayLapPhieu";
            // 
            // cboMaDG
            // 
            this.cboMaDG.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboMaDG.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboMaDG.FormattingEnabled = true;
            this.cboMaDG.Location = new System.Drawing.Point(169, 53);
            this.cboMaDG.Name = "cboMaDG";
            this.cboMaDG.Size = new System.Drawing.Size(237, 21);
            this.cboMaDG.TabIndex = 8;
            this.cboMaDG.SelectedIndexChanged += new System.EventHandler(this.cboMaDG_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnChiTietLoaiDG);
            this.groupBox1.Controls.Add(this.lblLaGVCB);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.lblSoLuongChuaMuon);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.cboMaDG);
            this.groupBox1.Controls.Add(this.lblNgayLapPhieu);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(541, 135);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin phiếu mượn";
            // 
            // btnChiTietLoaiDG
            // 
            this.btnChiTietLoaiDG.Location = new System.Drawing.Point(412, 51);
            this.btnChiTietLoaiDG.Name = "btnChiTietLoaiDG";
            this.btnChiTietLoaiDG.Size = new System.Drawing.Size(123, 23);
            this.btnChiTietLoaiDG.TabIndex = 13;
            this.btnChiTietLoaiDG.Text = "Chi tiết loại độc giả";
            this.btnChiTietLoaiDG.UseVisualStyleBackColor = true;
            this.btnChiTietLoaiDG.Click += new System.EventHandler(this.btnChiTietLoaiDG_Click);
            // 
            // lblLaGVCB
            // 
            this.lblLaGVCB.AutoSize = true;
            this.lblLaGVCB.Location = new System.Drawing.Point(166, 109);
            this.lblLaGVCB.Name = "lblLaGVCB";
            this.lblLaGVCB.Size = new System.Drawing.Size(44, 13);
            this.lblLaGVCB.TabIndex = 12;
            this.lblLaGVCB.Text = "laGVCB";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(48, 109);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(112, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Là giảng viên/cán bộ:";
            // 
            // lblSoLuongChuaMuon
            // 
            this.lblSoLuongChuaMuon.AutoSize = true;
            this.lblSoLuongChuaMuon.Location = new System.Drawing.Point(166, 86);
            this.lblSoLuongChuaMuon.Name = "lblSoLuongChuaMuon";
            this.lblSoLuongChuaMuon.Size = new System.Drawing.Size(100, 13);
            this.lblSoLuongChuaMuon.TabIndex = 10;
            this.lblSoLuongChuaMuon.Text = "soLuongChuaMuon";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(42, 86);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(118, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Số tài liệu có thể mượn:";
            // 
            // frmLapPM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(570, 615);
            this.Controls.Add(this.btnQuayVe);
            this.Controls.Add(this.btnLap);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmLapPM";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Lập phiếu mượn";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmLapPM_FormClosed);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnXoa;
        private System.Windows.Forms.Button btnThem;
        private System.Windows.Forms.ListView lvDSTL;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ComboBox cboTenTL;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cboMaTL;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnLap;
        private System.Windows.Forms.Button btnQuayVe;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblNgayLapPhieu;
        private System.Windows.Forms.ComboBox cboMaDG;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblLaSachHiem;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblChoGVCB;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblSoLuongChuaMuon;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblLaGVCB;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnChiTietLoaiDG;
    }
}