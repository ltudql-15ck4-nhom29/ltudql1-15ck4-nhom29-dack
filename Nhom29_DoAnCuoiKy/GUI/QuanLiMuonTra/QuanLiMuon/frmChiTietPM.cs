﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BLL;

namespace GUI
{
    public partial class frmChiTietPM : Form
    {
        PhieuMuonDTO phieuMuon = null;

        List<DocGiaDTO> lstDocGia = null;
        List<LoaiDocGiaDTO> lstLoaiDocGia = null;
        List<TaiLieuDTO> lstTaiLieu = null;

        public frmChiTietPM(PhieuMuonDTO pm)
        {
            InitializeComponent();
            phieuMuon = pm;

            napDSDocGia();
            napDSTaiLieu();
            napDSLoaiDocGia();

            hienThiCboDocGia();
            hienThiThongTinPhieuMuon();
            hienThiLvChiTietPhieuMuon();

            khoaChinhSua();
            btnLuu.Enabled = false;
        }
        
        void khoaChinhSua()
        {
            cboMaDocGia.Enabled = false;
            txtNgayLapPhieu.ReadOnly = true;
        }

        void moChinhSua()
        {
            cboMaDocGia.Enabled = true;
            txtNgayLapPhieu.ReadOnly = false;
        }

        void napDSDocGia()
        {
            DocGiaBLL bll = new DocGiaBLL();
            lstDocGia = bll.layDSDocGia();
        }

        void napDSLoaiDocGia()
        {
            LoaiDocGiaBLL bll = new LoaiDocGiaBLL();
            lstLoaiDocGia = bll.layDSLoaiDocGia();
        }

        void napDSTaiLieu()
        {
            TaiLieuBLL bll = new TaiLieuBLL();
            lstTaiLieu = bll.layDSTaiLieuDayDu();
        }

        void hienThiCboDocGia()
        {
            cboMaDocGia.Items.Clear();
            for(int i = 0; i < lstDocGia.Count; i++)
            {
                cboMaDocGia.Items.Add(lstDocGia[i]);
            }
        }

        void hienThiThongTinPhieuMuon()
        {
            lblMaPhieuMuon.Text = phieuMuon.MaPhieu.ToString();
            DocGiaDTO dg = DocGiaDTO.layDocGiaTheoPhieuMuon(phieuMuon, lstDocGia);
            cboMaDocGia.SelectedItem = dg;
            txtNgayLapPhieu.Text = phieuMuon.NgayLapPhieu.ToString("dd/MM/yyyy");
        }

        void hienThiLvChiTietPhieuMuon()
        {
            lvDSTL.Items.Clear();
            List<ChiTietPhieuMuonDTO> lst = phieuMuon.LstCTPM;
            for(int i = 0; i < lst.Count; i++)
            {
                ChiTietPhieuMuonDTO ctpm = lst[i];
                TaiLieuDTO tl = TaiLieuDTO.layTaiLieuTheoChiTietPhieuMuon(ctpm, lstTaiLieu);
                ListViewItem item = new ListViewItem(tl.MaTaiLieu.ToString());
                item.SubItems.Add(ctpm.MaBanSao.ToString());
                item.SubItems.Add(tl.TenTaiLieu);
                item.SubItems.Add(ctpm.HanChotTra.ToString("dd/MM/yyyy"));
                if(ctpm.NgayTra == DateTime.MinValue)
                {
                    item.SubItems.Add("");
                    if(DateTime.Now.Date > ctpm.HanChotTra.Date)
                    {
                        item.SubItems.Add("Có");
                    }
                    else
                    {
                        item.SubItems.Add("Không");
                    }
                    item.SubItems.Add("Chưa");
                }
                else
                {
                    item.SubItems.Add(ctpm.NgayTra.ToString("dd/MM/yyyy"));
                    if(ctpm.NgayTra.Date > ctpm.HanChotTra.Date)
                    {
                        item.SubItems.Add("Có");
                    }
                    else
                    {
                        item.SubItems.Add("Không");
                    }
                    item.SubItems.Add("Rồi");
                }
                lvDSTL.Items.Add(item);
            }
        }

        PhieuMuonDTO taoPhieuMuon()
        {
            PhieuMuonDTO pm = new PhieuMuonDTO();
            pm.MaPhieu = phieuMuon.MaPhieu;
            DateTime value;
            DateTime.TryParseExact(txtNgayLapPhieu.Text, "d/M/yyyy", null, System.Globalization.DateTimeStyles.None, out value);
            pm.NgayLapPhieu = value;
            pm.MaDocGia = ((DocGiaDTO)cboMaDocGia.SelectedItem).MaDocGia;
            pm.LstCTPM = phieuMuon.LstCTPM;
            pm.QuaHan = phieuMuon.QuaHan;
            pm.ChuaTra = phieuMuon.ChuaTra;
            return pm;
        }

        void capNhatPhieuMuonHienTai()
        {
            List<ChiTietPhieuMuonDTO> lstCTPM = phieuMuon.LstCTPM;
            DocGiaDTO dg = (DocGiaDTO)cboMaDocGia.SelectedItem;
            LoaiDocGiaDTO ldg = LoaiDocGiaDTO.layLoaiDocGiaTheoDocGia(dg, lstLoaiDocGia);
            for(int i = 0; i < lstCTPM.Count; i++)
            {
                ChiTietPhieuMuonDTO ctpm = lstCTPM[i];
                if(ctpm.NgayTra != DateTime.MinValue)
                {
                    ctpm.NgayTra = phieuMuon.NgayLapPhieu.AddDays(Convert.ToDouble(ldg.SoNgayMuonMax));
                }
            }
        }

        private void cboMaDocGia_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cboMaDocGia.SelectedIndex > -1)
            {
                lblTenDocGia.Text = ((DocGiaDTO)cboMaDocGia.SelectedItem).HoTen;
            }
        }

        private void btnChinhSua_Click(object sender, EventArgs e)
        {
            if(btnChinhSua.Text == "Chỉnh sửa")
            {
                btnChinhSua.Text = "Hủy";
                btnLuu.Enabled = true;
                moChinhSua();
            }
            else if(btnChinhSua.Text == "Hủy")
            {
                if(DialogResult.Yes == MessageBox.Show("Bạn có chắc muốn hủy thay đổi?", "Hủy", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                {
                    hienThiThongTinPhieuMuon();
                    btnChinhSua.Text = "Chỉnh sửa";
                    btnLuu.Enabled = false;
                    khoaChinhSua();
                }
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if(cboMaDocGia.SelectedIndex == -1)
            {
                MessageBox.Show("Không có độc giả nào được chọn!", "Lưu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DateTime value;
            if(!DateTime.TryParseExact(txtNgayLapPhieu.Text, "d/M/yyyy", null, System.Globalization.DateTimeStyles.None, out value))
            {
                MessageBox.Show("Ngày lập phiếu không hợp lệ!", "Lưu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if(DialogResult.Yes == MessageBox.Show("Bạn có chắc muốn lưu thay đổi?", "Lưu", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                PhieuMuonDTO pm = taoPhieuMuon();
                PhieuMuonBLL bll = new PhieuMuonBLL();
                if(bll.capNhatPhieuMuonDayDu(pm) == 1)
                {
                    MessageBox.Show("Cập nhật phiếu mượn thành công!", "Lưu", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    btnLuu.Enabled = false;
                    btnChinhSua.Text = "Chỉnh sửa";
                    khoaChinhSua();
                    phieuMuon = pm;
                    capNhatPhieuMuonHienTai();
                    hienThiThongTinPhieuMuon();
                    hienThiLvChiTietPhieuMuon();
                }
                else
                {
                    MessageBox.Show("Cập nhật phiếu mượn thất bại!", "Lưu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
