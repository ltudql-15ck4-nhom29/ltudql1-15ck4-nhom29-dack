﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BLL;

namespace GUI
{
    public partial class frmTimKiemPM : Form
    {
        List<PhieuMuonDTO> lstPhieuMuon = null;
        List<PhieuMuonDTO> lstHienThi = null;

        List<DocGiaDTO> lstDocGia = null;

        public frmTimKiemPM()
        {
            InitializeComponent();

            napDSPhieuMuon();
            napDSDocGia();

            lstHienThi = lstPhieuMuon;

            hienThiLvPhieuMuon();
        }

        void napDSPhieuMuon()
        {
            PhieuMuonBLL bll = new PhieuMuonBLL();
            lstPhieuMuon = bll.layDSPhieuMuonDayDuCoQuaHanChuaTraNgayTra();
        }

        void napDSDocGia()
        {
            DocGiaBLL bll = new DocGiaBLL();
            lstDocGia = bll.layDSDocGia();
        }

        void hienThiLvPhieuMuon()
        {
            lvDSPM.Items.Clear();
            for(int i = 0; i < lstHienThi.Count; i++)
            {
                PhieuMuonDTO pm = lstHienThi[i];
                DocGiaDTO dg = DocGiaDTO.layDocGiaTheoPhieuMuon(pm, lstDocGia);
                ListViewItem item = new ListViewItem(pm.MaPhieu.ToString());
                item.SubItems.Add(dg.MaDocGia.ToString());
                item.SubItems.Add(dg.HoTen);
                item.SubItems.Add(pm.NgayLapPhieu.ToString("dd/MM/yyyy"));
                item.SubItems.Add(pm.QuaHan.ToString());
                item.SubItems.Add(pm.ChuaTra.ToString());
                lvDSPM.Items.Add(item);
            }
        }

        void timKiemMaDG()
        {
            lstHienThi = new List<PhieuMuonDTO>();
            int iSearch = int.Parse(txtNoiDungLoc.Text);
            for (int i = 0; i < lstPhieuMuon.Count; i++)
            {
                PhieuMuonDTO pm = lstPhieuMuon[i];
                if (pm.MaDocGia == iSearch)
                {
                    lstHienThi.Add(pm);
                }
            }
            hienThiLvPhieuMuon();
        }

        void timKiemMaPM()
        {
            lstHienThi = new List<PhieuMuonDTO>();
            int iSearch = int.Parse(txtNoiDungLoc.Text);
            for (int i = 0; i < lstPhieuMuon.Count; i++)
            {
                PhieuMuonDTO pm = lstPhieuMuon[i];
                if (pm.MaPhieu == iSearch)
                {
                    lstHienThi.Add(pm);
                }
            }
            hienThiLvPhieuMuon();
        }

        bool kiemTraPhieuMuonDaTraHet(PhieuMuonDTO pm)
        {
            List<ChiTietPhieuMuonDTO> lstCTPM = pm.LstCTPM;
            for(int i = 0; i < lstCTPM.Count; i++)
            {
                ChiTietPhieuMuonDTO ctpm = lstCTPM[i];
                if(ctpm.SoThuTuCTPT == -1)
                {
                    return false;
                }
            }
            return true;
        }

        private void capNhatForm(object sender, EventArgs e)
        {
            napDSPhieuMuon();

            lstHienThi = lstPhieuMuon;

            hienThiLvPhieuMuon();            
        }

        private void btnQuayVe_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmTimKiemPM_FormClosed(object sender, FormClosedEventArgs e)
        {
            Owner.Show();
        }

        private void btnLapPhieuTra_Click(object sender, EventArgs e)
        {
            frmLapPT f = new frmLapPT();
            f.FormClosed += capNhatForm;
            f.Show(this);
            Hide();
        }

        private void btnXemChiTiet_Click(object sender, EventArgs e)
        {
            if(lvDSPM.SelectedItems.Count == 0)
            {
                MessageBox.Show("Không có phiếu mượn nào được chọn!", "Xem chi tiết", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            PhieuMuonDTO pm = lstHienThi[lvDSPM.SelectedIndices[0]];
            frmChiTietPM f = new frmChiTietPM(pm);
            f.ShowDialog();
            capNhatForm(this, new EventArgs());
        }

        private void btnXemHet_Click(object sender, EventArgs e)
        {
            lstHienThi = lstPhieuMuon;
            hienThiLvPhieuMuon();
        }

        private void lblTimKiemQuaHan_Click(object sender, EventArgs e)
        {
            lstHienThi = new List<PhieuMuonDTO>();
            for(int i = 0; i < lstPhieuMuon.Count; i++)
            {
                if(lstPhieuMuon[i].QuaHan > 0)
                {
                    lstHienThi.Add(lstPhieuMuon[i]);
                }
            }
            hienThiLvPhieuMuon();
        }

        private void lblTimKiemChuaTra_Click(object sender, EventArgs e)
        {
            lstHienThi = new List<PhieuMuonDTO>();
            for (int i = 0; i < lstPhieuMuon.Count; i++)
            {
                if (lstPhieuMuon[i].ChuaTra > 0)
                {
                    lstHienThi.Add(lstPhieuMuon[i]);
                }
            }
            hienThiLvPhieuMuon();
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            if (cboLoaiBoLoc.SelectedIndex == -1)
            {
                MessageBox.Show("Loại bộ lọc chưa được chọn!", "Tìm kiếm", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            switch (cboLoaiBoLoc.SelectedIndex)
            {
                case 0:
                    if (!RegularExpression.laSo(txtNoiDungLoc.Text))
                    {
                        MessageBox.Show("Nội dung tìm kiếm phải là số!", "Tìm kiếm", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    timKiemMaPM();
                    break;
                case 1:
                    if (!RegularExpression.laSo(txtNoiDungLoc.Text))
                    {
                        MessageBox.Show("Nội dung tìm kiếm phải là số!", "Tìm kiếm", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    timKiemMaDG();
                    break;
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (lvDSPM.SelectedItems.Count == 0)
            {
                MessageBox.Show("Không có phiếu mượn nào được chọn!", "Xóa phiếu mượn", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            PhieuMuonDTO pm = lstHienThi[lvDSPM.SelectedIndices[0]];
            if (!kiemTraPhieuMuonDaTraHet(pm))
            {
                MessageBox.Show("Không thể xóa phiếu mượn này vì tài liệu của phiếu mượn này chưa trả hết!", "Xóa phiếu mượn", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (DialogResult.Yes == MessageBox.Show("Hành động này không thể hoàn tác! Bạn có chắc muốn xóa phiếu mượn này không?", "Xóa phiếu mượn", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                PhieuMuonBLL bll = new PhieuMuonBLL();
                if (bll.xoaPhieuMuon(pm) == 1)
                {
                    MessageBox.Show("Xóa phiếu mượn thành công!", "Xóa phiếu mượn", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lstHienThi.Remove(pm);
                    lstPhieuMuon.Remove(pm);
                    hienThiLvPhieuMuon();
                }
                else
                {
                    MessageBox.Show("Xóa phiếu mượn thất bại!", "Xóa phiếu mượn", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
