﻿namespace GUI
{
    partial class frmLapPT
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnQuayVe = new System.Windows.Forms.Button();
            this.btnLap = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblHanChotTra = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnXoa = new System.Windows.Forms.Button();
            this.btnThem = new System.Windows.Forms.Button();
            this.lvDSTL = new System.Windows.Forms.ListView();
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cboTenTL = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cboMaTL = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnChiTietLoaiDG = new System.Windows.Forms.Button();
            this.cboDG = new System.Windows.Forms.ComboBox();
            this.lblNgayLapPhieu = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnQuayVe
            // 
            this.btnQuayVe.Location = new System.Drawing.Point(12, 502);
            this.btnQuayVe.Name = "btnQuayVe";
            this.btnQuayVe.Size = new System.Drawing.Size(146, 41);
            this.btnQuayVe.TabIndex = 7;
            this.btnQuayVe.Text = "Quay về";
            this.btnQuayVe.UseVisualStyleBackColor = true;
            this.btnQuayVe.Click += new System.EventHandler(this.btnQuayVe_Click);
            // 
            // btnLap
            // 
            this.btnLap.Location = new System.Drawing.Point(479, 502);
            this.btnLap.Name = "btnLap";
            this.btnLap.Size = new System.Drawing.Size(146, 41);
            this.btnLap.TabIndex = 6;
            this.btnLap.Text = "Lập phiếu trả\r\n";
            this.btnLap.UseVisualStyleBackColor = true;
            this.btnLap.Click += new System.EventHandler(this.btnLap_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblHanChotTra);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.btnXoa);
            this.groupBox2.Controls.Add(this.btnThem);
            this.groupBox2.Controls.Add(this.lvDSTL);
            this.groupBox2.Controls.Add(this.cboTenTL);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.cboMaTL);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(12, 112);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(613, 384);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Danh sách tài liệu trả";
            // 
            // lblHanChotTra
            // 
            this.lblHanChotTra.AutoSize = true;
            this.lblHanChotTra.Location = new System.Drawing.Point(196, 85);
            this.lblHanChotTra.Name = "lblHanChotTra";
            this.lblHanChotTra.Size = new System.Drawing.Size(63, 13);
            this.lblHanChotTra.TabIndex = 8;
            this.lblHanChotTra.Text = "hanChotTra";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(121, 85);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Hạn chót trả:";
            // 
            // btnXoa
            // 
            this.btnXoa.Location = new System.Drawing.Point(502, 347);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(105, 31);
            this.btnXoa.TabIndex = 6;
            this.btnXoa.Text = "Xóa tài liệu";
            this.btnXoa.UseVisualStyleBackColor = true;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnThem
            // 
            this.btnThem.Location = new System.Drawing.Point(425, 30);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(98, 48);
            this.btnThem.TabIndex = 5;
            this.btnThem.Text = "Thêm tài liệu";
            this.btnThem.UseVisualStyleBackColor = true;
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // lvDSTL
            // 
            this.lvDSTL.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader6,
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.lvDSTL.FullRowSelect = true;
            this.lvDSTL.Location = new System.Drawing.Point(6, 123);
            this.lvDSTL.MultiSelect = false;
            this.lvDSTL.Name = "lvDSTL";
            this.lvDSTL.Size = new System.Drawing.Size(601, 218);
            this.lvDSTL.TabIndex = 4;
            this.lvDSTL.UseCompatibleStateImageBehavior = false;
            this.lvDSTL.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Mã phiếu mượn";
            this.columnHeader6.Width = 90;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Mã tài liệu";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Mã bản sao";
            this.columnHeader2.Width = 70;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Tên tài liệu";
            this.columnHeader3.Width = 200;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Hạn chót trả";
            this.columnHeader4.Width = 80;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Quá hạn";
            this.columnHeader5.Width = 70;
            // 
            // cboTenTL
            // 
            this.cboTenTL.FormattingEnabled = true;
            this.cboTenTL.Location = new System.Drawing.Point(199, 57);
            this.cboTenTL.Name = "cboTenTL";
            this.cboTenTL.Size = new System.Drawing.Size(210, 21);
            this.cboTenTL.TabIndex = 3;
            this.cboTenTL.SelectedIndexChanged += new System.EventHandler(this.cboTenTL_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(128, 60);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Tên tài liệu:";
            // 
            // cboMaTL
            // 
            this.cboMaTL.FormattingEnabled = true;
            this.cboMaTL.Location = new System.Drawing.Point(199, 30);
            this.cboMaTL.Name = "cboMaTL";
            this.cboMaTL.Size = new System.Drawing.Size(107, 21);
            this.cboMaTL.TabIndex = 1;
            this.cboMaTL.SelectedIndexChanged += new System.EventHandler(this.cboMaTL_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(132, 33);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Mã tài liệu:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnChiTietLoaiDG);
            this.groupBox1.Controls.Add(this.cboDG);
            this.groupBox1.Controls.Add(this.lblNgayLapPhieu);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(607, 94);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin phiếu trả";
            // 
            // btnChiTietLoaiDG
            // 
            this.btnChiTietLoaiDG.Location = new System.Drawing.Point(439, 49);
            this.btnChiTietLoaiDG.Name = "btnChiTietLoaiDG";
            this.btnChiTietLoaiDG.Size = new System.Drawing.Size(115, 23);
            this.btnChiTietLoaiDG.TabIndex = 9;
            this.btnChiTietLoaiDG.Text = "Chi tiết loại độc giả";
            this.btnChiTietLoaiDG.UseVisualStyleBackColor = true;
            this.btnChiTietLoaiDG.Click += new System.EventHandler(this.btnChiTietLoaiDG_Click);
            // 
            // cboDG
            // 
            this.cboDG.FormattingEnabled = true;
            this.cboDG.Location = new System.Drawing.Point(197, 51);
            this.cboDG.Name = "cboDG";
            this.cboDG.Size = new System.Drawing.Size(236, 21);
            this.cboDG.TabIndex = 8;
            this.cboDG.SelectedIndexChanged += new System.EventHandler(this.cboDG_SelectedIndexChanged);
            // 
            // lblNgayLapPhieu
            // 
            this.lblNgayLapPhieu.AutoSize = true;
            this.lblNgayLapPhieu.Location = new System.Drawing.Point(194, 28);
            this.lblNgayLapPhieu.Name = "lblNgayLapPhieu";
            this.lblNgayLapPhieu.Size = new System.Drawing.Size(75, 13);
            this.lblNgayLapPhieu.TabIndex = 7;
            this.lblNgayLapPhieu.Text = "ngayLapPhieu";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(95, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Độc giả:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(95, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Ngày lập phiếu:";
            // 
            // frmLapPT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(637, 560);
            this.Controls.Add(this.btnQuayVe);
            this.Controls.Add(this.btnLap);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmLapPT";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Lập phiếu trả";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmLapPT_FormClosed);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnQuayVe;
        private System.Windows.Forms.Button btnLap;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblHanChotTra;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnXoa;
        private System.Windows.Forms.Button btnThem;
        private System.Windows.Forms.ListView lvDSTL;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ComboBox cboTenTL;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cboMaTL;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblNgayLapPhieu;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ComboBox cboDG;
        private System.Windows.Forms.Button btnChiTietLoaiDG;
    }
}