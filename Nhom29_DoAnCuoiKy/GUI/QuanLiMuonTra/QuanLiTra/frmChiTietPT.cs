﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BLL;

namespace GUI
{
    public partial class frmChiTietPT : Form
    {
        PhieuTraDTO phieuTra = null;

        List<DocGiaDTO> lstDocGia = null;
        List<TaiLieuDTO> lstTaiLieu = null;

        public frmChiTietPT(PhieuTraDTO pt)
        {
            InitializeComponent();
            phieuTra = pt;

            napDSDocGia();
            napDSTaiLieu();

            hienThiCboDocGia();
            hienThiThongTinPhieuTra();
            hienThiLvChiTietPhieuTra();

            khoaChinhSua();
            btnLuu.Enabled = false;
        }

        void khoaChinhSua()
        {
            cboMaDocGia.Enabled = false;
            txtNgayLapPhieu.ReadOnly = true;
        }

        void moChinhSua()
        {
            cboMaDocGia.Enabled = true;
            txtNgayLapPhieu.ReadOnly = false;
        }

        void napDSDocGia()
        {
            DocGiaBLL bll = new DocGiaBLL();
            lstDocGia = bll.layDSDocGia();
        }

        void napDSTaiLieu()
        {
            TaiLieuBLL bll = new TaiLieuBLL();
            lstTaiLieu = bll.layDSTaiLieuDayDu();
        }

        void hienThiCboDocGia()
        {
            cboMaDocGia.Items.Clear();
            for (int i = 0; i < lstDocGia.Count; i++)
            {
                cboMaDocGia.Items.Add(lstDocGia[i]);
            }
        }

        void hienThiThongTinPhieuTra()
        {
            lblMaPhieuTra.Text = phieuTra.MaPhieu.ToString();
            DocGiaDTO dg = DocGiaDTO.layDocGiaTheoPhieuTra(phieuTra, lstDocGia);
            cboMaDocGia.SelectedItem = dg;
            txtNgayLapPhieu.Text = phieuTra.NgayLapPhieu.ToString("dd/MM/yyyy");
        }

        void hienThiLvChiTietPhieuTra()
        {
            //ADDITIONAL FEATURE: HanChotTra (denied)
            lvDSTL.Items.Clear();
            List<ChiTietPhieuTraDTO> lstCTPT = phieuTra.LstCTPT;
            for(int i = 0; i < lstCTPT.Count; i++)
            {
                ChiTietPhieuTraDTO ctpt = lstCTPT[i];
                TaiLieuDTO tl = TaiLieuDTO.layTaiLieuTheoChiTietPhieuTra(ctpt, lstTaiLieu);
                ListViewItem item = new ListViewItem(ctpt.SoThuTu.ToString());
                item.SubItems.Add(ctpt.MaPhieuMuon.ToString());
                item.SubItems.Add(ctpt.MaBanSao.ToString());
                item.SubItems.Add(tl.TenTaiLieu);
                item.SubItems.Add(ctpt.NgayTra.ToString("dd/MM/yyyy"));
                lvDSTL.Items.Add(item);
            }
        }

        PhieuTraDTO taoPhieuTra()
        {
            PhieuTraDTO pt = new PhieuTraDTO();
            pt.MaPhieu = phieuTra.MaPhieu;
            DateTime value;
            DateTime.TryParseExact(txtNgayLapPhieu.Text, "d/M/yyyy", null, System.Globalization.DateTimeStyles.None, out value);
            pt.NgayLapPhieu = value;
            pt.MaDocGia = ((DocGiaDTO)cboMaDocGia.SelectedItem).MaDocGia;
            pt.LstCTPT = phieuTra.LstCTPT;
            pt.LstMaPM = phieuTra.LstMaPM;
            return pt;
        }

        void capNhatPhieuTraHienTai()
        {
            List<ChiTietPhieuTraDTO> lstCTPT = phieuTra.LstCTPT;
            for(int i = 0; i < lstCTPT.Count; i++)
            {
                ChiTietPhieuTraDTO ctpt = lstCTPT[i];
                ctpt.NgayTra = phieuTra.NgayLapPhieu;
            }
        }

        private void btnQuayVe_Click(object sender, EventArgs e)
        {

        }

        private void cboMaDocGia_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboMaDocGia.SelectedIndex > -1)
            {
                lblTenDocGia.Text = ((DocGiaDTO)cboMaDocGia.SelectedItem).HoTen;
            }
        }

        private void btnChinhSua_Click(object sender, EventArgs e)
        {
            if (btnChinhSua.Text == "Chỉnh sửa")
            {
                btnChinhSua.Text = "Hủy";
                btnLuu.Enabled = true;
                moChinhSua();
            }
            else if (btnChinhSua.Text == "Hủy")
            {
                if (DialogResult.Yes == MessageBox.Show("Bạn có chắc muốn hủy thay đổi?", "Hủy", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                {
                    hienThiThongTinPhieuTra();
                    btnChinhSua.Text = "Chỉnh sửa";
                    btnLuu.Enabled = false;
                    khoaChinhSua();
                }
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (cboMaDocGia.SelectedIndex == -1)
            {
                MessageBox.Show("Không có độc giả nào được chọn!", "Lưu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DateTime value;
            if (!DateTime.TryParseExact(txtNgayLapPhieu.Text, "d/M/yyyy", null, System.Globalization.DateTimeStyles.None, out value))
            {
                MessageBox.Show("Ngày lập phiếu không hợp lệ!", "Lưu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (DialogResult.Yes == MessageBox.Show("Bạn có chắc muốn lưu thay đổi?", "Lưu", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                PhieuTraDTO pt = taoPhieuTra();
                PhieuTraBLL bll = new PhieuTraBLL();
                if (bll.capNhatPhieuTra(pt) == 1)
                {
                    MessageBox.Show("Cập nhật phiếu mượn thành công!", "Lưu", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    btnLuu.Enabled = false;
                    btnChinhSua.Text = "Chỉnh sửa";
                    khoaChinhSua();
                    phieuTra = pt;
                    capNhatPhieuTraHienTai();
                    hienThiThongTinPhieuTra();
                    hienThiLvChiTietPhieuTra();
                }
                else
                {
                    MessageBox.Show("Cập nhật phiếu mượn thất bại!", "Lưu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
