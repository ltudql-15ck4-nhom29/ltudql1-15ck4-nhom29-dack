﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BLL;

namespace GUI
{
    public partial class frmTimKiemPT : Form
    {
        List<PhieuTraDTO> lstPhieuTra = null;
        List<PhieuTraDTO> lstHienThi = null;

        List<DocGiaDTO> lstDocGia = null;

        public frmTimKiemPT()
        {
            InitializeComponent();

            napDSDocGia();
            napDSPhieuTra();

            lstHienThi = lstPhieuTra;
            hienThiLvPhieuTra();
        }

        void napDSPhieuTra()
        {
            PhieuTraBLL bll = new PhieuTraBLL();
            lstPhieuTra = bll.layDSPhieuTraDayDuCoDSMaPhieuMuon();
        }

        void napDSDocGia()
        {
            DocGiaBLL bll = new DocGiaBLL();
            lstDocGia = bll.layDSDocGia();
        }

        void hienThiLvPhieuTra()
        {
            lvDSPT.Items.Clear();
            for(int i = 0; i < lstHienThi.Count; i++)
            {
                PhieuTraDTO pt = lstHienThi[i];
                DocGiaDTO dg = DocGiaDTO.layDocGiaTheoPhieuTra(pt, lstDocGia);
                ListViewItem item = new ListViewItem(pt.MaPhieu.ToString());
                item.SubItems.Add(pt.taoChuoiDSMaPhieuMuon());
                item.SubItems.Add(pt.MaDocGia.ToString());
                item.SubItems.Add(dg.HoTen);
                item.SubItems.Add(pt.NgayLapPhieu.ToString("dd/MM/yyyy"));
                item.SubItems.Add(pt.LstCTPT.Count.ToString());
                lvDSPT.Items.Add(item);
            }
        }
        
        void timKiemMaPT()
        {
            lstHienThi = new List<PhieuTraDTO>();
            int iSearch = int.Parse(txtNoiDungLoc.Text);
            for (int i = 0; i < lstPhieuTra.Count; i++)
            {
                PhieuTraDTO pt = lstPhieuTra[i];
                if (pt.MaPhieu == iSearch)
                {
                    lstHienThi.Add(pt);
                }
            }
            hienThiLvPhieuTra();
        }

        void timKiemMaPM()
        {
            lstHienThi = new List<PhieuTraDTO>();
            int iSearch = int.Parse(txtNoiDungLoc.Text);
            for (int i = 0; i < lstPhieuTra.Count; i++)
            {
                PhieuTraDTO pt = lstPhieuTra[i];
                List<int> lstMaPM = pt.LstMaPM;
                for(int j = 0; j < lstMaPM.Count; j++)
                {
                    if(lstMaPM[j] == iSearch)
                    {
                        lstHienThi.Add(pt);
                        break;
                    }
                }
            }
            hienThiLvPhieuTra();
        }

        void timKiemMaDG()
        {
            lstHienThi = new List<PhieuTraDTO>();
            int iSearch = int.Parse(txtNoiDungLoc.Text);
            for (int i = 0; i < lstPhieuTra.Count; i++)
            {
                PhieuTraDTO pt = lstPhieuTra[i];
                if (pt.MaDocGia == iSearch)
                {
                    lstHienThi.Add(pt);
                }
            }
            hienThiLvPhieuTra();
        }

        private void capNhatForm(object sender, EventArgs e)
        {
            napDSPhieuTra();

            lstHienThi = lstPhieuTra;
            hienThiLvPhieuTra();
        }

        private void btnQuayVe_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmTimKiemPT_FormClosed(object sender, FormClosedEventArgs e)
        {
            Owner.Show();
        }

        private void btnXemChiTiet_Click(object sender, EventArgs e)
        {
            if (lvDSPT.Items.Count == 0)
            {
                MessageBox.Show("Chưa có phiếu trả nào được chọn!", "Xóa phiếu trả", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            PhieuTraDTO pt = lstHienThi[lvDSPT.SelectedIndices[0]];
            frmChiTietPT f = new frmChiTietPT(pt);
            f.ShowDialog();
            capNhatForm(this, new EventArgs());
        }

        private void btnXemHet_Click(object sender, EventArgs e)
        {
            lstHienThi = lstPhieuTra;
            hienThiLvPhieuTra();
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            if (cboLoaiBoLoc.SelectedIndex == -1)
            {
                MessageBox.Show("Loại bộ lọc chưa được chọn!", "Tìm kiếm", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            switch (cboLoaiBoLoc.SelectedIndex)
            {
                case 0:
                    if (!RegularExpression.laSo(txtNoiDungLoc.Text))
                    {
                        MessageBox.Show("Nội dung tìm kiếm phải là số!", "Tìm kiếm", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    timKiemMaPT();
                    break;
                case 1:
                    if (!RegularExpression.laSo(txtNoiDungLoc.Text))
                    {
                        MessageBox.Show("Nội dung tìm kiếm phải là số!", "Tìm kiếm", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    timKiemMaPM();
                    break;
                case 2:
                    if (!RegularExpression.laSo(txtNoiDungLoc.Text))
                    {
                        MessageBox.Show("Nội dung tìm kiếm phải là số!", "Tìm kiếm", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    timKiemMaDG();
                    break;
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if(lvDSPT.Items.Count == 0)
            {
                MessageBox.Show("Chưa có phiếu trả nào được chọn!", "Xóa phiếu trả", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if(DialogResult.Yes == MessageBox.Show("Hành động này không thể hoàn tác. Bạn có chắc muốn xóa phiếu trả này?", "Xóa phiếu trả", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                PhieuTraDTO pt = lstHienThi[lvDSPT.SelectedIndices[0]];
                PhieuTraBLL bll = new PhieuTraBLL();
                if(bll.xoaPhieuTra(pt) == 1)
                {
                    MessageBox.Show("Xóa phiếu trả thành công!", "Xóa phiếu trả", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lstHienThi.Remove(pt);
                    lstPhieuTra.Remove(pt);
                    hienThiLvPhieuTra();
                }
                else
                {
                    MessageBox.Show("Xóa phiếu trả thất bại!", "Xóa phiếu trả", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
