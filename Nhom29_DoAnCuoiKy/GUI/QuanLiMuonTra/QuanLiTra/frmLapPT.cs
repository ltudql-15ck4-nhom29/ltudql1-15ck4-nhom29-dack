﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BLL;

namespace GUI
{
    public partial class frmLapPT : Form
    {
        List<DocGiaDTO> lstDocGia = null;
        List<BanSaoDTO> lstBanSao = null;
        List<TaiLieuDTO> lstTaiLieu = null;
        List<LoaiDocGiaDTO> lstLoaiDG = null;

        List<BanSaoDTO> lstBanSaoMuon = null;
        List<TaiLieuDTO> lstTaiLieuMuon = null;
        List<PhieuMuonDTO> lstPhieuMuonMuon = null;
        List<ChiTietPhieuMuonDTO> lstCTPMMuon = null;

        List<int> lstIndexBanSaoTra = null;
        List<int> lstIndexQuaHan = null;



        public frmLapPT()
        {
            InitializeComponent();

            napDSDocGia();
            napDSLoaiDocGia();
            napDSBanSao();
            napDSTaiLieu();

            hienThiCboDocGia();
            lblNgayLapPhieu.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            lblHanChotTra.Text = "";
        }

        void napDSDocGia()
        {
            DocGiaBLL bll = new DocGiaBLL();
            lstDocGia = bll.layDSDocGiaDayDuCoQuaHan();
        }

        void napDSLoaiDocGia()
        {
            LoaiDocGiaBLL bll = new LoaiDocGiaBLL();
            lstLoaiDG = bll.layDSLoaiDocGia();
        }

        void napDSBanSao()
        {
            BanSaoBLL bll = new BanSaoBLL();
            lstBanSao = bll.layDSBanSao();
        }

        void napDSTaiLieu()
        {
            TaiLieuBLL bll = new TaiLieuBLL();
            lstTaiLieu = bll.layDSTaiLieu();
        }

        void hienThiCboDocGia()
        {
            cboDG.Items.Clear();
            for(int i = 0; i < lstDocGia.Count; i++)
            {
                cboDG.Items.Add(lstDocGia[i]);
            }
        }

        void hienThiCboTaiLieu()
        {
            cboMaTL.Items.Clear();
            cboMaTL.DisplayMember = "MaTaiLieu";
            cboTenTL.Items.Clear();
            cboTenTL.DisplayMember = "TenTaiLieu";
            for(int i = 0; i < lstTaiLieuMuon.Count; i++)
            {
                TaiLieuDTO tl = lstTaiLieuMuon[i];
                cboMaTL.Items.Add(tl);
                cboTenTL.Items.Add(tl);
            }
        }

        void hienThiLvTaiLieu()
        {
            lvDSTL.Items.Clear();
            lstIndexQuaHan = new List<int>();
            for(int i = 0; i < lstIndexBanSaoTra.Count; i++)
            {
                int index = lstIndexBanSaoTra[i];
                BanSaoDTO bs = lstBanSaoMuon[index];
                TaiLieuDTO tl = lstTaiLieuMuon[index];
                PhieuMuonDTO pm = lstPhieuMuonMuon[index];
                ChiTietPhieuMuonDTO ctpm = lstCTPMMuon[index];
                ListViewItem item = new ListViewItem(pm.MaPhieu.ToString());
                item.SubItems.Add(tl.MaTaiLieu.ToString());
                item.SubItems.Add(bs.MaBanSao.ToString());
                item.SubItems.Add(tl.TenTaiLieu);
                item.SubItems.Add(ctpm.HanChotTra.ToString("dd/MM/yyyy"));
                if(ctpm.HanChotTra < DateTime.Now.Date)
                {
                    lstIndexQuaHan.Add(i);
                    item.SubItems.Add("Có");
                }
                else
                {
                    item.SubItems.Add("Không");
                }
                lvDSTL.Items.Add(item);
            }
        }

        bool kiemTraDaCoBanSaoTra(int index)
        {
            for(int i = 0; i < lstIndexBanSaoTra.Count; i++)
            {
                if(index == lstIndexBanSaoTra[i])
                {
                    return true;
                }
            }
            return false;
        }

        PhieuTraDTO taoPhieuTra()
        {
            DocGiaDTO dg = (DocGiaDTO) cboDG.SelectedItem;
            PhieuTraDTO pt = new PhieuTraDTO();
            pt.NgayLapPhieu = DateTime.Now.Date;
            pt.MaDocGia = dg.MaDocGia;
            List<ChiTietPhieuTraDTO> lstCTPT = new List<ChiTietPhieuTraDTO>();
            for(int i = 0; i < lstIndexBanSaoTra.Count; i++)
            {
                int index = lstIndexBanSaoTra[i];
                BanSaoDTO bs = lstBanSaoMuon[index];
                TaiLieuDTO tl = lstTaiLieuMuon[index];
                PhieuMuonDTO pm = lstPhieuMuonMuon[index];
                ChiTietPhieuMuonDTO ctpm = lstCTPMMuon[index];
                ChiTietPhieuTraDTO ctpt = new ChiTietPhieuTraDTO();
                ctpt.MaBanSao = bs.MaBanSao;
                ctpt.MaPhieuMuon = pm.MaPhieu;
                ctpt.NgayTra = pt.NgayLapPhieu;
                lstCTPT.Add(ctpt);
            }
            pt.LstCTPT = lstCTPT;
            return pt;
        }

        private void btnQuayVe_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmLapPT_FormClosed(object sender, FormClosedEventArgs e)
        {
            Owner.Show();
        }

        private void cboDG_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cboDG.SelectedIndex > -1)
            {
                lstIndexBanSaoTra = new List<int>();
                lstBanSaoMuon = new List<BanSaoDTO>();
                lstTaiLieuMuon = new List<TaiLieuDTO>();
                lstPhieuMuonMuon = new List<PhieuMuonDTO>();
                lstCTPMMuon = new List<ChiTietPhieuMuonDTO>();
                List<PhieuMuonDTO> lstPhieuMuon = lstDocGia[cboDG.SelectedIndex].LstPM;
                for(int i = 0; i < lstPhieuMuon.Count; i++)
                {
                    PhieuMuonDTO pm = lstPhieuMuon[i];
                    List<ChiTietPhieuMuonDTO> lstCTPM = pm.LstCTPM;
                    for(int j = 0; j < lstCTPM.Count; j++)
                    {
                        ChiTietPhieuMuonDTO ctpm = lstCTPM[j];
                        if(ctpm.SoThuTuCTPT == -1)
                        {
                            BanSaoDTO bs = BanSaoDTO.layBanSaoTheoChiTietPhieuMuon(ctpm, lstBanSao);
                            lstBanSaoMuon.Add(bs);
                            TaiLieuDTO tl = TaiLieuDTO.layTaiLieuTheoBanSao(bs, lstTaiLieu);
                            lstTaiLieuMuon.Add(tl);
                            lstPhieuMuonMuon.Add(pm);
                            lstCTPMMuon.Add(ctpm);
                        }
                    }
                }
                hienThiCboTaiLieu();
            }
        }

        private void cboMaTL_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboTenTL.SelectedIndex = cboMaTL.SelectedIndex;
            if(cboTenTL.SelectedIndex > -1)
            {
                lblHanChotTra.Text = lstCTPMMuon[cboTenTL.SelectedIndex].HanChotTra.ToString("dd/MM/yyyy");
            }
        }

        private void cboTenTL_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboMaTL.SelectedIndex = cboTenTL.SelectedIndex;
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            if(cboDG.SelectedIndex == -1)
            {
                MessageBox.Show("Chưa có độc giả nào được chọn!", "Thêm tài liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (cboMaTL.SelectedIndex == -1)
            {
                MessageBox.Show("Chưa có tài liệu nào được chọn!", "Thêm tài liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if(kiemTraDaCoBanSaoTra(cboMaTL.SelectedIndex))
            {
                MessageBox.Show("Tài liệu này đã được chọn!", "Thêm tài liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            lstIndexBanSaoTra.Add(cboMaTL.SelectedIndex);
            hienThiLvTaiLieu();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if(lvDSTL.SelectedItems.Count > 0)
            {
                lstIndexBanSaoTra.RemoveAt(lvDSTL.SelectedIndices[0]);
                hienThiLvTaiLieu();
            }
            else
            {
                MessageBox.Show("Chưa có tài liệu nào được chọn!", "Xóa tài liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnLap_Click(object sender, EventArgs e)
        {
            if(lstIndexBanSaoTra.Count == 0)
            {
                MessageBox.Show("Chưa có tài liệu nào được chọn!", "Xóa tài liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if(DialogResult.Yes == MessageBox.Show("Bạn có chắc muốn lập phiếu trả?", "Lập phiếu trả", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                PhieuTraBLL bll1 = new PhieuTraBLL();
                PhieuTraDTO pt = taoPhieuTra();
                bll1.themPhieuTra(pt);
                DocGiaDTO dg = (DocGiaDTO)cboDG.SelectedItem;
                dg.SoTaiLieuDangMuon -= lstIndexBanSaoTra.Count;
                DocGiaBLL bll2 = new DocGiaBLL();
                bll2.capNhatSoTLDangMuon(dg);
                MessageBox.Show("Đã lập phiếu trả thành công!", "Lập phiếu trả", MessageBoxButtons.OK, MessageBoxIcon.Information);
                if(lstIndexQuaHan.Count > 0 && DialogResult.Yes == MessageBox.Show("Độc giả này đã trả quá hạn, bạn có muốn lập phiếu cảnh cáo?", "Lập phiếu cảnh cáo", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                {
                    frmLapPhieuCC f = new frmLapPhieuCC(dg);
                    f.ShowDialog();
                }
                Close();
            }
        }

        private void btnChiTietLoaiDG_Click(object sender, EventArgs e)
        {
            if(cboDG.SelectedIndex == -1)
            {
                MessageBox.Show("Không có độc giả nào được chọn!", "Chi tiết loại độc giả", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DocGiaDTO dg = (DocGiaDTO) cboDG.SelectedItem;
            LoaiDocGiaDTO ldg = LoaiDocGiaDTO.layLoaiDocGiaTheoDocGia(dg, lstLoaiDG);
            frmChiTietLoaiDG f = new frmChiTietLoaiDG(ldg);
            f.ShowDialog();
        }
    }
}
