﻿namespace GUI
{
    partial class frmBaoCaoLoaiTaiLieu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.reportViewerLoaiTaiLieu = new Microsoft.Reporting.WinForms.ReportViewer();
            this.SuspendLayout();
            // 
            // reportViewerLoaiTaiLieu
            // 
            this.reportViewerLoaiTaiLieu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reportViewerLoaiTaiLieu.LocalReport.ReportEmbeddedResource = "GUI.BaoCaoThongKe.Reports.LoaiTaiLieu.rdlc";
            this.reportViewerLoaiTaiLieu.Location = new System.Drawing.Point(0, 0);
            this.reportViewerLoaiTaiLieu.Name = "reportViewerLoaiTaiLieu";
            this.reportViewerLoaiTaiLieu.Size = new System.Drawing.Size(284, 262);
            this.reportViewerLoaiTaiLieu.TabIndex = 0;
            // 
            // frmBaoCaoLoaiTaiLieu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.reportViewerLoaiTaiLieu);
            this.MaximizeBox = false;
            this.Name = "frmBaoCaoLoaiTaiLieu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Báo cáo thống kê loại tài liệu";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmBaoCaoLoaiTaiLieu_FormClosed);
            this.Load += new System.EventHandler(this.frmBaoCaoLoaiTaiLieu_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewerLoaiTaiLieu;
    }
}