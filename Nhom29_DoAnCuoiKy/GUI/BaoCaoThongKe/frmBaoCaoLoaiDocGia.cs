﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace GUI
{
    public partial class frmBaoCaoLoaiDocGia : Form
    {
        public frmBaoCaoLoaiDocGia()
        {
            InitializeComponent();
            QLTVDataSet ds = new QLTVDataSet();
            QLTVDataSetTableAdapters.LoaiDocGiaTableAdapter ta = new QLTVDataSetTableAdapters.LoaiDocGiaTableAdapter();
            ta.Fill(ds.LoaiDocGia);
            ReportDataSource rds = new ReportDataSource();
            rds.Name = "LoaiDocGiaDataSet";
            rds.Value = ds.Tables["LoaiDocGia"];
            reportViewerLoaiDocGia.LocalReport.DataSources.Add(rds);
            this.Width = 960;
            this.Height = 460;
        }

        private void frmBaoCaoLoaiDocGia_Load(object sender, EventArgs e)
        {
            this.reportViewerLoaiDocGia.RefreshReport();
        }

        private void frmBaoCaoLoaiDocGia_FormClosed(object sender, FormClosedEventArgs e)
        {
            Owner.Show();
        }
    }
}
