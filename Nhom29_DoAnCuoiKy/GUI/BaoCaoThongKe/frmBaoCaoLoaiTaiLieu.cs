﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace GUI
{
    public partial class frmBaoCaoLoaiTaiLieu : Form
    {
        public frmBaoCaoLoaiTaiLieu()
        {
            InitializeComponent();
            QLTVDataSet ds = new QLTVDataSet();
            QLTVDataSetTableAdapters.LoaiTaiLieuTableAdapter ta = new QLTVDataSetTableAdapters.LoaiTaiLieuTableAdapter();
            ta.Fill(ds.LoaiTaiLieu);
            ReportDataSource rds = new ReportDataSource();
            rds.Name = "LoaiTaiLieuDataSet";
            rds.Value = ds.Tables["LoaiTaiLieu"];
            reportViewerLoaiTaiLieu.LocalReport.DataSources.Add(rds);
            this.Width = 680;
            this.Height = 510;
        }

        private void frmBaoCaLoaiTaiLieu_Load(object sender, EventArgs e)
        {
            this.reportViewerLoaiTaiLieu.RefreshReport();
        }

        private void frmBaoCaoLoaiTaiLieu_FormClosed(object sender, FormClosedEventArgs e)
        {
            Owner.Show();
        }

        private void frmBaoCaoLoaiTaiLieu_Load(object sender, EventArgs e)
        {

            this.reportViewerLoaiTaiLieu.RefreshReport();
        }
    }
}
