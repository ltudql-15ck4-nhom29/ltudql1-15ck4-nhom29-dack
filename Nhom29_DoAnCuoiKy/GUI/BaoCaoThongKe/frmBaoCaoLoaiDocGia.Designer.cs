﻿namespace GUI
{
    partial class frmBaoCaoLoaiDocGia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.reportViewerLoaiDocGia = new Microsoft.Reporting.WinForms.ReportViewer();
            this.SuspendLayout();
            // 
            // reportViewerLoaiDocGia
            // 
            this.reportViewerLoaiDocGia.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reportViewerLoaiDocGia.LocalReport.ReportEmbeddedResource = "GUI.BaoCaoThongKe.Reports.LoaiDocGia.rdlc";
            this.reportViewerLoaiDocGia.Location = new System.Drawing.Point(0, 0);
            this.reportViewerLoaiDocGia.Name = "reportViewerLoaiDocGia";
            this.reportViewerLoaiDocGia.Size = new System.Drawing.Size(672, 464);
            this.reportViewerLoaiDocGia.TabIndex = 0;
            // 
            // frmBaoCaoLoaiDocGia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(672, 464);
            this.Controls.Add(this.reportViewerLoaiDocGia);
            this.MaximizeBox = false;
            this.Name = "frmBaoCaoLoaiDocGia";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Báo cáo thống kê loại độc giả";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmBaoCaoLoaiDocGia_FormClosed);
            this.Load += new System.EventHandler(this.frmBaoCaoLoaiDocGia_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewerLoaiDocGia;
    }
}