﻿namespace GUI
{
    partial class frmBaoCaoDocGia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.reportViewerDocGia = new Microsoft.Reporting.WinForms.ReportViewer();
            this.SuspendLayout();
            // 
            // reportViewerDocGia
            // 
            this.reportViewerDocGia.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reportViewerDocGia.LocalReport.ReportEmbeddedResource = "GUI.BaoCaoThongKe.Reports.DocGia.rdlc";
            this.reportViewerDocGia.Location = new System.Drawing.Point(0, 0);
            this.reportViewerDocGia.Name = "reportViewerDocGia";
            this.reportViewerDocGia.Size = new System.Drawing.Size(284, 262);
            this.reportViewerDocGia.TabIndex = 0;
            // 
            // frmBaoCaoDocGia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.reportViewerDocGia);
            this.MaximizeBox = false;
            this.Name = "frmBaoCaoDocGia";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Báo cáo thống kê độc giả";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmBaoCaoDocGia_FormClosed);
            this.Load += new System.EventHandler(this.frmBaoCaoDocGia_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewerDocGia;
    }
}