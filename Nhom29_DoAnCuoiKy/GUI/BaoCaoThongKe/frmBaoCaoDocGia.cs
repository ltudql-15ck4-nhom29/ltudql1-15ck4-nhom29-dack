﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace GUI
{
    public partial class frmBaoCaoDocGia : Form
    {
        public frmBaoCaoDocGia()
        {
            InitializeComponent();
            QLTVDataSet ds = new QLTVDataSet();
            QLTVDataSetTableAdapters.DocGiaTableAdapter ta = new QLTVDataSetTableAdapters.DocGiaTableAdapter();
            ta.Fill(ds.DocGia);
            ReportDataSource rds = new ReportDataSource();
            rds.Name = "DocGiaDataSet";
            rds.Value = ds.Tables["DocGia"];
            reportViewerDocGia.LocalReport.DataSources.Add(rds);
            this.Width = 960;
            this.Height = 600;
        }

        private void frmBaoCaoDocGia_Load(object sender, EventArgs e)
        {
            this.reportViewerDocGia.RefreshReport();
        }

        private void frmBaoCaoDocGia_FormClosed(object sender, FormClosedEventArgs e)
        {
            Owner.Show();
        }
    }
}
