﻿namespace GUI
{
    partial class frmBaoCaoTaiLieu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.reportViewerTaiLieu = new Microsoft.Reporting.WinForms.ReportViewer();
            this.SuspendLayout();
            // 
            // reportViewerTaiLieu
            // 
            this.reportViewerTaiLieu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reportViewerTaiLieu.LocalReport.ReportEmbeddedResource = "GUI.BaoCaoThongKe.Reports.TaiLieu.rdlc";
            this.reportViewerTaiLieu.Location = new System.Drawing.Point(0, 0);
            this.reportViewerTaiLieu.Name = "reportViewerTaiLieu";
            this.reportViewerTaiLieu.Size = new System.Drawing.Size(473, 420);
            this.reportViewerTaiLieu.TabIndex = 0;
            // 
            // frmBaoCaoTaiLieu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(473, 420);
            this.Controls.Add(this.reportViewerTaiLieu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmBaoCaoTaiLieu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Báo cáo thống kê tài liệu";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmBaoCaoTaiLieu_FormClosed);
            this.Load += new System.EventHandler(this.frmBaoCaoTaiLieu_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewerTaiLieu;
    }
}