﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace GUI
{
    public partial class frmBaoCaoTaiLieu : Form
    {
        public frmBaoCaoTaiLieu()
        {
            InitializeComponent();
            QLTVDataSet ds = new QLTVDataSet();
            QLTVDataSetTableAdapters.TaiLieuTableAdapter ta = new QLTVDataSetTableAdapters.TaiLieuTableAdapter();
            ta.Fill(ds.TaiLieu);
            ReportDataSource rds = new ReportDataSource();
            rds.Name = "TaiLieuDataSet";
            rds.Value = ds.Tables["TaiLieu"];
            reportViewerTaiLieu.LocalReport.DataSources.Add(rds);
            this.Width = 925;
            this.Height = 990;
        }

        private void frmBaoCaoTaiLieu_Load(object sender, EventArgs e)
        {            
            this.reportViewerTaiLieu.RefreshReport();
        }

        private void frmBaoCaoTaiLieu_FormClosed(object sender, FormClosedEventArgs e)
        {
            Owner.Show();
        }
    }
}
