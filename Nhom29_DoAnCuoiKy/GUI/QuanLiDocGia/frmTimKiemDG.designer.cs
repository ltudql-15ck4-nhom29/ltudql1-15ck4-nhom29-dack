﻿namespace GUI
{
    partial class frmTimKiemDG
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnXemHet = new System.Windows.Forms.Button();
            this.btnTimKiem = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNoiDungLoc = new System.Windows.Forms.TextBox();
            this.cboLoaiBoLoc = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lvDSDG = new System.Windows.Forms.ListView();
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnXemChiTiet = new System.Windows.Forms.Button();
            this.btnLapPhieuMuon = new System.Windows.Forms.Button();
            this.btnLapPhieuTra = new System.Windows.Forms.Button();
            this.btnLapPhieuCanhCao = new System.Windows.Forms.Button();
            this.btnQuayVe = new System.Windows.Forms.Button();
            this.btnXoa = new System.Windows.Forms.Button();
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnXemHet);
            this.groupBox1.Controls.Add(this.btnTimKiem);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtNoiDungLoc);
            this.groupBox1.Controls.Add(this.cboLoaiBoLoc);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(828, 119);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Bộ lọc tìm kiếm";
            // 
            // btnXemHet
            // 
            this.btnXemHet.Location = new System.Drawing.Point(312, 75);
            this.btnXemHet.Name = "btnXemHet";
            this.btnXemHet.Size = new System.Drawing.Size(100, 31);
            this.btnXemHet.TabIndex = 5;
            this.btnXemHet.Text = "Xem hết";
            this.btnXemHet.UseVisualStyleBackColor = true;
            this.btnXemHet.Click += new System.EventHandler(this.btnXemHet_Click);
            // 
            // btnTimKiem
            // 
            this.btnTimKiem.Location = new System.Drawing.Point(422, 75);
            this.btnTimKiem.Name = "btnTimKiem";
            this.btnTimKiem.Size = new System.Drawing.Size(100, 31);
            this.btnTimKiem.TabIndex = 4;
            this.btnTimKiem.Text = "Tìm kiếm";
            this.btnTimKiem.UseVisualStyleBackColor = true;
            this.btnTimKiem.Click += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(253, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Nội dung:";
            // 
            // txtNoiDungLoc
            // 
            this.txtNoiDungLoc.Location = new System.Drawing.Point(312, 49);
            this.txtNoiDungLoc.Name = "txtNoiDungLoc";
            this.txtNoiDungLoc.Size = new System.Drawing.Size(210, 20);
            this.txtNoiDungLoc.TabIndex = 2;
            // 
            // cboLoaiBoLoc
            // 
            this.cboLoaiBoLoc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboLoaiBoLoc.FormattingEnabled = true;
            this.cboLoaiBoLoc.Items.AddRange(new object[] {
            "Mã độc giả",
            "CMND",
            "MSSV",
            "MSCB",
            "Họ và tên"});
            this.cboLoaiBoLoc.Location = new System.Drawing.Point(312, 22);
            this.cboLoaiBoLoc.Name = "cboLoaiBoLoc";
            this.cboLoaiBoLoc.Size = new System.Drawing.Size(210, 21);
            this.cboLoaiBoLoc.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(230, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tìm kiếm theo:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lvDSDG);
            this.groupBox2.Location = new System.Drawing.Point(12, 137);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(828, 203);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Danh sách độc giả";
            // 
            // lvDSDG
            // 
            this.lvDSDG.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader1,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8});
            this.lvDSDG.FullRowSelect = true;
            this.lvDSDG.Location = new System.Drawing.Point(6, 19);
            this.lvDSDG.MultiSelect = false;
            this.lvDSDG.Name = "lvDSDG";
            this.lvDSDG.Size = new System.Drawing.Size(816, 178);
            this.lvDSDG.TabIndex = 0;
            this.lvDSDG.UseCompatibleStateImageBehavior = false;
            this.lvDSDG.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Mã độc giả";
            this.columnHeader2.Width = 70;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Họ và tên";
            this.columnHeader3.Width = 150;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "CMND";
            this.columnHeader4.Width = 70;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "MSSV/MSCB";
            this.columnHeader1.Width = 80;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Loại độc giả";
            this.columnHeader5.Width = 110;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Số sách đang mượn";
            this.columnHeader6.Width = 110;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Số sách quá hạn";
            this.columnHeader7.Width = 100;
            // 
            // btnXemChiTiet
            // 
            this.btnXemChiTiet.Location = new System.Drawing.Point(628, 346);
            this.btnXemChiTiet.Name = "btnXemChiTiet";
            this.btnXemChiTiet.Size = new System.Drawing.Size(212, 40);
            this.btnXemChiTiet.TabIndex = 2;
            this.btnXemChiTiet.Text = "Xem chi tiết thông tin độc giả";
            this.btnXemChiTiet.UseVisualStyleBackColor = true;
            this.btnXemChiTiet.Click += new System.EventHandler(this.btnXemChiTiet_Click);
            // 
            // btnLapPhieuMuon
            // 
            this.btnLapPhieuMuon.Location = new System.Drawing.Point(209, 346);
            this.btnLapPhieuMuon.Name = "btnLapPhieuMuon";
            this.btnLapPhieuMuon.Size = new System.Drawing.Size(127, 40);
            this.btnLapPhieuMuon.TabIndex = 3;
            this.btnLapPhieuMuon.Text = "Lập phiếu mượn sách";
            this.btnLapPhieuMuon.UseVisualStyleBackColor = true;
            this.btnLapPhieuMuon.Click += new System.EventHandler(this.btnLapPhieuMuon_Click);
            // 
            // btnLapPhieuTra
            // 
            this.btnLapPhieuTra.Location = new System.Drawing.Point(342, 346);
            this.btnLapPhieuTra.Name = "btnLapPhieuTra";
            this.btnLapPhieuTra.Size = new System.Drawing.Size(116, 40);
            this.btnLapPhieuTra.TabIndex = 4;
            this.btnLapPhieuTra.Text = "Lập phiếu trả sách";
            this.btnLapPhieuTra.UseVisualStyleBackColor = true;
            this.btnLapPhieuTra.Click += new System.EventHandler(this.btnLapPhieuTra_Click);
            // 
            // btnLapPhieuCanhCao
            // 
            this.btnLapPhieuCanhCao.Location = new System.Drawing.Point(464, 346);
            this.btnLapPhieuCanhCao.Name = "btnLapPhieuCanhCao";
            this.btnLapPhieuCanhCao.Size = new System.Drawing.Size(158, 40);
            this.btnLapPhieuCanhCao.TabIndex = 5;
            this.btnLapPhieuCanhCao.Text = "Lập phiếu cảnh cáo";
            this.btnLapPhieuCanhCao.UseVisualStyleBackColor = true;
            this.btnLapPhieuCanhCao.Click += new System.EventHandler(this.btnLapPhieuCanhCao_Click);
            // 
            // btnQuayVe
            // 
            this.btnQuayVe.Location = new System.Drawing.Point(18, 392);
            this.btnQuayVe.Name = "btnQuayVe";
            this.btnQuayVe.Size = new System.Drawing.Size(185, 40);
            this.btnQuayVe.TabIndex = 6;
            this.btnQuayVe.Text = "Quay về";
            this.btnQuayVe.UseVisualStyleBackColor = true;
            this.btnQuayVe.Click += new System.EventHandler(this.btnQuayVe_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Location = new System.Drawing.Point(18, 346);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(185, 40);
            this.btnXoa.TabIndex = 7;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.UseVisualStyleBackColor = true;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Đóng phí";
            this.columnHeader8.Width = 90;
            // 
            // frmTimKiemDG
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 464);
            this.Controls.Add(this.btnXoa);
            this.Controls.Add(this.btnQuayVe);
            this.Controls.Add(this.btnLapPhieuCanhCao);
            this.Controls.Add(this.btnLapPhieuTra);
            this.Controls.Add(this.btnLapPhieuMuon);
            this.Controls.Add(this.btnXemChiTiet);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmTimKiemDG";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tìm kiếm độc giả";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmTimKiemDG_FormClosed);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnXemHet;
        private System.Windows.Forms.Button btnTimKiem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNoiDungLoc;
        private System.Windows.Forms.ComboBox cboLoaiBoLoc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListView lvDSDG;
        private System.Windows.Forms.Button btnXemChiTiet;
        private System.Windows.Forms.Button btnLapPhieuMuon;
        private System.Windows.Forms.Button btnLapPhieuTra;
        private System.Windows.Forms.Button btnLapPhieuCanhCao;
        private System.Windows.Forms.Button btnQuayVe;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.Button btnXoa;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader8;
    }
}