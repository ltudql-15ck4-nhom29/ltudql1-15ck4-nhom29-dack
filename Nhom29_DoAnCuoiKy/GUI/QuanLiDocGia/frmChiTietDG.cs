﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BLL;

namespace GUI
{
    public partial class frmChiTietDG : Form
    {
        DocGiaDTO docGia = null;
        List<LoaiDocGiaDTO> lstLoaiDocGia = null;
        List<HinhThucPhatDTO> lstHinhThucPhat = null;
        List<TaiLieuDTO> lstTaiLieu = null;
        List<LoaiTaiLieuDTO> lstLoaiTaiLieu = null;
        List<BanSaoDTO> lstBanSao = null;

        public frmChiTietDG(DocGiaDTO dg)
        {
            InitializeComponent();

            docGia = dg;

            btnLuu.Enabled = false;
            khoaChinhSua();

            napDSLoaiDocGia();
            napDSHinhThucPhat();
            napDSTaiLieu();
            napDSLoaiTaiLieu();
            napDSBanSao();

            hienThiCboLoaiDocGia();
            hienThiCboHinhThucPhat();
            hienThiThongTinDocGia();
            hienThiLvDanhSachTaiLieu();
        }

        void napDSLoaiDocGia()
        {
            LoaiDocGiaBLL bll = new LoaiDocGiaBLL();
            lstLoaiDocGia = bll.layDSLoaiDocGia();
        }

        void napDSHinhThucPhat()
        {
            HinhThucPhatBLL bll = new HinhThucPhatBLL();
            lstHinhThucPhat = bll.layDSHinhThucPhat();
        }

        void napDSTaiLieu()
        {
            TaiLieuBLL bll = new TaiLieuBLL();
            lstTaiLieu = bll.layDSTaiLieu();
        }

        void napDSLoaiTaiLieu()
        {
            LoaiTaiLieuBLL bll = new LoaiTaiLieuBLL();
            lstLoaiTaiLieu = bll.layDSLoaiTaiLieu();
        }

        void napDSBanSao()
        {
            BanSaoBLL bll = new BanSaoBLL();
            lstBanSao = bll.layDSBanSao();
        }

        void hienThiCboLoaiDocGia()
        {
            cboLoaiDG.Items.Clear();
            for(int i = 0; i < lstLoaiDocGia.Count; i++)
            {
                cboLoaiDG.Items.Add(lstLoaiDocGia[i]);
            }
        }

        void hienThiCboHinhThucPhat()
        {
            cboHinhThucPhat.Items.Clear();
            for (int i = 0; i < lstHinhThucPhat.Count; i++)
            {
                cboHinhThucPhat.Items.Add(lstHinhThucPhat[i]);
            }
        }

        void khoaChinhSua()
        {
            txtHoTen.ReadOnly = true;
            txtCMND.ReadOnly = true;
            txtNgaySinh.ReadOnly = true;
            txtSDT.ReadOnly = true;
            txtDiaChi.ReadOnly = true;
            txtEmail.ReadOnly = true;
            txtMaSo.ReadOnly = true;
            cboLoaiDG.Enabled = false;
            cboHinhThucPhat.Enabled = false;
        }

        void moChinhSua()
        {
            txtHoTen.ReadOnly = false;
            txtCMND.ReadOnly = false;
            txtNgaySinh.ReadOnly = false;
            txtSDT.ReadOnly = false;
            txtDiaChi.ReadOnly = false;
            txtEmail.ReadOnly = false;
            txtMaSo.ReadOnly = false;
            cboLoaiDG.Enabled = true;
            cboHinhThucPhat.Enabled = true;
            cboLoaiDG_SelectedIndexChanged(this, new EventArgs());
        }

        void hienThiThongTinDocGia()
        {
            lblMaDG.Text = docGia.MaDocGia.ToString();
            txtHoTen.Text = docGia.HoTen;
            txtCMND.Text = docGia.CMND;
            txtNgaySinh.Text = docGia.NgaySinh.ToString("dd/MM/yyyy");
            txtSDT.Text = docGia.SoDienThoai;
            txtDiaChi.Text = docGia.DiaChi;
            txtEmail.Text = docGia.Email;
            if(docGia.MSSV != null)
            {
                txtMaSo.Text = docGia.MSSV;
            }
            else if(docGia.MSCB != null)
            {
                txtMaSo.Text = docGia.MSCB;
            }
            else
            {
                txtMaSo.Text = "";
            }
            cboLoaiDG.SelectedIndex = docGia.LoaiDocGia;
            cboHinhThucPhat.SelectedIndex = docGia.MaHinhThucPhat;
            lblSoTLDangMuon.Text = docGia.SoTaiLieuDangMuon.ToString();  
        }

        void hienThiLvDanhSachTaiLieu()
        {
            lvDSTL.Items.Clear();
            List<PhieuMuonDTO> lst1 = docGia.LstPM;
            for(int i = 0; i < lst1.Count; i++)
            {
                List<ChiTietPhieuMuonDTO> lst2 = lst1[i].LstCTPM;
                for(int j = 0; j < lst2.Count; j++)
                {
                    ChiTietPhieuMuonDTO ctpm = lst2[j];
                    if (ctpm.SoThuTuCTPT == -1)
                    {
                        BanSaoDTO bs = BanSaoDTO.layBanSaoTheoChiTietPhieuMuon(ctpm, lstBanSao);
                        TaiLieuDTO tl = TaiLieuDTO.layTaiLieuTheoBanSao(bs, lstTaiLieu);
                        LoaiTaiLieuDTO ltl = LoaiTaiLieuDTO.layLoaiTaiLieuTheoTaiLieu(tl, lstLoaiTaiLieu);
                        ListViewItem item = new ListViewItem(tl.MaTaiLieu.ToString());
                        item.SubItems.Add(bs.MaBanSao.ToString());
                        item.SubItems.Add(tl.TenTaiLieu);
                        item.SubItems.Add(ltl.ToString());
                        lvDSTL.Items.Add(item);
                    }
                }
            }
        }

        DocGiaDTO taoDocGia()
        {
            DocGiaDTO dg = new DocGiaDTO();
            dg.MaDocGia = docGia.MaDocGia;
            dg.HoTen = txtHoTen.Text;
            dg.CMND = txtCMND.Text;
            DateTime value;
            DateTime.TryParseExact(txtNgaySinh.Text, "d/M/yyyy", null, System.Globalization.DateTimeStyles.None, out value);
            dg.NgaySinh = value;
            dg.SoDienThoai = txtSDT.Text;
            dg.DiaChi = txtDiaChi.Text;
            dg.Email = txtEmail.Text;
            LoaiDocGiaDTO ldg = (LoaiDocGiaDTO)cboLoaiDG.SelectedItem;
            dg.MSSV = null;
            dg.MSCB = null;
            if (ldg.LaSinhVien)
            {
                dg.MSSV = txtMaSo.Text;
            }
            else if(ldg.LaGVCB)
            {
                dg.MSCB = txtMaSo.Text;
            }
            dg.LoaiDocGia = cboLoaiDG.SelectedIndex;
            dg.MaHinhThucPhat = cboHinhThucPhat.SelectedIndex;
            dg.LstPM = docGia.LstPM;
            dg.SoTaiLieuDangMuon = docGia.SoTaiLieuDangMuon;
            dg.SoTaiLieuDangMuonQuaHan = docGia.SoTaiLieuDangMuonQuaHan;
            return dg;
        }

        private void btnChiTietLoaiDG_Click(object sender, EventArgs e)
        {
            if(cboLoaiDG.SelectedIndex > -1)
            {
                LoaiDocGiaDTO ldg = (LoaiDocGiaDTO)cboLoaiDG.SelectedItem;
                frmChiTietLoaiDG f = new frmChiTietLoaiDG(ldg);
                f.ShowDialog();
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (RegularExpression.laHoTen(txtHoTen.Text) == false)
            {
                MessageBox.Show("Họ tên không hợp lệ! Họ tên không được chứa số hay kí tự đặc biệt!", "Lỗi nhập liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (RegularExpression.laSo(txtCMND.Text) == false)
            {
                MessageBox.Show("Số CMND không hợp lệ!", "Lỗi nhập liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DateTime value;
            if (DateTime.TryParseExact(txtNgaySinh.Text, "d/M/yyyy", null, System.Globalization.DateTimeStyles.None, out value) == false)
            {
                MessageBox.Show("Ngày sinh không hợp lệ!", "Lỗi nhập liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (RegularExpression.laSo(txtSDT.Text) == false)
            {
                MessageBox.Show("Số điện thoại không hợp lệ!", "Lỗi nhập liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (RegularExpression.laEmail(txtEmail.Text) == false)
            {
                MessageBox.Show("Email không hợp lệ!", "Lỗi nhập liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (cboLoaiDG.SelectedIndex == -1)
            {
                MessageBox.Show("Chưa chọn loại độc giả!", "Lỗi nhập liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (txtMaSo.ReadOnly == false && !RegularExpression.laSo(txtMaSo.Text))
            {
                MessageBox.Show("Mã số sinh viên/Mã số cán bộ không hợp lệ!", "Lỗi nhập liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (cboHinhThucPhat.SelectedIndex == -1)
            {
                MessageBox.Show("Chưa chọn hình thức phạt!", "Lỗi nhập liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DocGiaDTO dg = taoDocGia();
            LoaiDocGiaDTO ldg = (LoaiDocGiaDTO)cboLoaiDG.SelectedItem;
            if (dg.SoTaiLieuDangMuon > ldg.SoTaiLieuMuonMax)
            {
                MessageBox.Show("Loại độc giả không hợp lệ vì số tài liệu mượn tối đa thấp hơn số tài liệu đang được mượn bởi độc giả này!", "Lỗi nhập liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if(DialogResult.Yes == MessageBox.Show("Bạn có chắc muốn cập nhật thông tin?", "Cập nhật", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                DocGiaBLL bll = new DocGiaBLL();
                if(bll.capNhatDocGia(dg) == 1)
                {
                    MessageBox.Show("Cập nhật thông tin độc giả thành công!", "Cập nhật", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    docGia = dg;
                    PhieuCanhCaoBLL bll1 = new PhieuCanhCaoBLL();
                    PhieuCanhCaoDTO pcc = new PhieuCanhCaoDTO();
                    pcc.MaDocGia = docGia.MaDocGia;
                    pcc.NgayLapPhieu = DateTime.Now.Date;
                    pcc.MaHinhThucPhat = docGia.MaHinhThucPhat;
                    bll1.capNhatDanhSachPhieuCanhCao(pcc);
                    btnChinhSua.Text = "Chỉnh sửa";
                    btnChinhSua.Enabled = true;
                    btnLuu.Enabled = false;
                    khoaChinhSua();
                }
                else
                {
                    MessageBox.Show("Cập nhật thông tin độc giả thất bại!", "Cập nhật", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnChinhSua_Click(object sender, EventArgs e)
        {
            if(btnChinhSua.Text == "Chỉnh sửa")
            {
                btnLuu.Enabled = true;
                btnChinhSua.Text = "Hủy";
                moChinhSua();
            }
            else if(btnChinhSua.Text == "Hủy")
            {
                if(DialogResult.Yes == MessageBox.Show("Bạn có muốn hủy thay đổi?", "Hủy", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                {
                    btnLuu.Enabled = false;
                    btnChinhSua.Text = "Chỉnh sửa";
                    hienThiThongTinDocGia();
                    khoaChinhSua();
                }
            }
        }

        private void cboLoaiDG_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboLoaiDG.SelectedIndex > -1)
            {
                LoaiDocGiaDTO ldg = (LoaiDocGiaDTO)cboLoaiDG.SelectedItem;
                if(btnLuu.Enabled)
                {
                    if (ldg.LaGVCB || ldg.LaSinhVien)
                    {
                        txtMaSo.ReadOnly = false;
                    }
                    else
                    {
                        txtMaSo.ReadOnly = true;
                        txtMaSo.Text = "";
                    }
                }
            }
        }
    }
}
