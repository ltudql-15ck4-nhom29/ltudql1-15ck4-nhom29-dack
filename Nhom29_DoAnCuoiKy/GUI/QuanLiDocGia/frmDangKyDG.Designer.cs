﻿namespace GUI
{
    partial class frmDangKyDG
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtHoTen = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCMND = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSDT = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDiaChi = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cboLoaiDG = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtMSSVCB = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtNgaySinh = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblLaSinhVien = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblLaGVCB = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblSoNgayMuonToiDa = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblSoSachMuonToiDa = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.lblPhiThuongNien = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblTenLoaiDG = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnDangKy = new System.Windows.Forms.Button();
            this.btnQuayVe = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(44, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Họ và tên:";
            // 
            // txtHoTen
            // 
            this.txtHoTen.Location = new System.Drawing.Point(152, 29);
            this.txtHoTen.MaxLength = 50;
            this.txtHoTen.Name = "txtHoTen";
            this.txtHoTen.Size = new System.Drawing.Size(260, 20);
            this.txtHoTen.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(44, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "CMND:";
            // 
            // txtCMND
            // 
            this.txtCMND.Location = new System.Drawing.Point(152, 55);
            this.txtCMND.MaxLength = 9;
            this.txtCMND.Name = "txtCMND";
            this.txtCMND.Size = new System.Drawing.Size(260, 20);
            this.txtCMND.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(44, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Ngày sinh:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(44, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Số điện thoại:";
            // 
            // txtSDT
            // 
            this.txtSDT.Location = new System.Drawing.Point(152, 107);
            this.txtSDT.MaxLength = 13;
            this.txtSDT.Name = "txtSDT";
            this.txtSDT.Size = new System.Drawing.Size(260, 20);
            this.txtSDT.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(44, 136);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Địa chỉ email:";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(152, 133);
            this.txtEmail.MaxLength = 50;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(260, 20);
            this.txtEmail.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(44, 162);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Địa chỉ:";
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Location = new System.Drawing.Point(152, 159);
            this.txtDiaChi.MaxLength = 100;
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(260, 20);
            this.txtDiaChi.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(44, 187);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Loại độc giả:";
            // 
            // cboLoaiDG
            // 
            this.cboLoaiDG.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboLoaiDG.FormattingEnabled = true;
            this.cboLoaiDG.Location = new System.Drawing.Point(152, 184);
            this.cboLoaiDG.Name = "cboLoaiDG";
            this.cboLoaiDG.Size = new System.Drawing.Size(260, 21);
            this.cboLoaiDG.TabIndex = 6;
            this.cboLoaiDG.SelectedIndexChanged += new System.EventHandler(this.cboLoaiDG_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtMSSVCB);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtNgaySinh);
            this.groupBox1.Controls.Add(this.cboLoaiDG);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtDiaChi);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtEmail);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtSDT);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtCMND);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtHoTen);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(457, 246);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin độc giả";
            // 
            // txtMSSVCB
            // 
            this.txtMSSVCB.Location = new System.Drawing.Point(152, 211);
            this.txtMSSVCB.MaxLength = 10;
            this.txtMSSVCB.Name = "txtMSSVCB";
            this.txtMSSVCB.ReadOnly = true;
            this.txtMSSVCB.Size = new System.Drawing.Size(260, 20);
            this.txtMSSVCB.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(44, 214);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "MSSV/MSCB:";
            // 
            // txtNgaySinh
            // 
            this.txtNgaySinh.Location = new System.Drawing.Point(152, 81);
            this.txtNgaySinh.MaxLength = 10;
            this.txtNgaySinh.Name = "txtNgaySinh";
            this.txtNgaySinh.Size = new System.Drawing.Size(260, 20);
            this.txtNgaySinh.TabIndex = 2;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblLaSinhVien);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.lblLaGVCB);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.lblSoNgayMuonToiDa);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.lblSoSachMuonToiDa);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.lblPhiThuongNien);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.lblTenLoaiDG);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Location = new System.Drawing.Point(12, 264);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(457, 176);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Thông tin loại độc giả";
            // 
            // lblLaSinhVien
            // 
            this.lblLaSinhVien.AutoSize = true;
            this.lblLaSinhVien.Location = new System.Drawing.Point(288, 49);
            this.lblLaSinhVien.Name = "lblLaSinhVien";
            this.lblLaSinhVien.Size = new System.Drawing.Size(57, 13);
            this.lblLaSinhVien.TabIndex = 15;
            this.lblLaSinhVien.Text = "laSinhVien";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(106, 49);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(67, 13);
            this.label14.TabIndex = 14;
            this.label14.Text = "Là sinh viên:";
            // 
            // lblLaGVCB
            // 
            this.lblLaGVCB.AutoSize = true;
            this.lblLaGVCB.Location = new System.Drawing.Point(287, 71);
            this.lblLaGVCB.Name = "lblLaGVCB";
            this.lblLaGVCB.Size = new System.Drawing.Size(44, 13);
            this.lblLaGVCB.TabIndex = 13;
            this.lblLaGVCB.Text = "laGVCB";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(106, 71);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(145, 13);
            this.label10.TabIndex = 12;
            this.label10.Text = "Là giảng viên/cán bộ trường:";
            // 
            // lblSoNgayMuonToiDa
            // 
            this.lblSoNgayMuonToiDa.AutoSize = true;
            this.lblSoNgayMuonToiDa.Location = new System.Drawing.Point(287, 139);
            this.lblSoNgayMuonToiDa.Name = "lblSoNgayMuonToiDa";
            this.lblSoNgayMuonToiDa.Size = new System.Drawing.Size(85, 13);
            this.lblSoNgayMuonToiDa.TabIndex = 9;
            this.lblSoNgayMuonToiDa.Text = "soNgayMuonTD";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(106, 139);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(108, 13);
            this.label13.TabIndex = 8;
            this.label13.Text = "Số ngày mượn tối đa:";
            // 
            // lblSoSachMuonToiDa
            // 
            this.lblSoSachMuonToiDa.AutoSize = true;
            this.lblSoSachMuonToiDa.Location = new System.Drawing.Point(287, 116);
            this.lblSoSachMuonToiDa.Name = "lblSoSachMuonToiDa";
            this.lblSoSachMuonToiDa.Size = new System.Drawing.Size(85, 13);
            this.lblSoSachMuonToiDa.TabIndex = 7;
            this.lblSoSachMuonToiDa.Text = "soSachMuonTD";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(106, 116);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(108, 13);
            this.label17.TabIndex = 6;
            this.label17.Text = "Số sách mượn tối đa:";
            // 
            // lblPhiThuongNien
            // 
            this.lblPhiThuongNien.AutoSize = true;
            this.lblPhiThuongNien.Location = new System.Drawing.Point(287, 94);
            this.lblPhiThuongNien.Name = "lblPhiThuongNien";
            this.lblPhiThuongNien.Size = new System.Drawing.Size(36, 13);
            this.lblPhiThuongNien.TabIndex = 3;
            this.lblPhiThuongNien.Text = "phiTN";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(106, 94);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(86, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Phí thường niên:";
            // 
            // lblTenLoaiDG
            // 
            this.lblTenLoaiDG.AutoSize = true;
            this.lblTenLoaiDG.Location = new System.Drawing.Point(288, 30);
            this.lblTenLoaiDG.Name = "lblTenLoaiDG";
            this.lblTenLoaiDG.Size = new System.Drawing.Size(38, 13);
            this.lblTenLoaiDG.TabIndex = 1;
            this.lblTenLoaiDG.Text = "tenDG";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(106, 30);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Tên loại độc giả:";
            // 
            // btnDangKy
            // 
            this.btnDangKy.Location = new System.Drawing.Point(327, 446);
            this.btnDangKy.Name = "btnDangKy";
            this.btnDangKy.Size = new System.Drawing.Size(142, 40);
            this.btnDangKy.TabIndex = 2;
            this.btnDangKy.Text = "Đăng ký";
            this.btnDangKy.UseVisualStyleBackColor = true;
            this.btnDangKy.Click += new System.EventHandler(this.btnDangKy_Click);
            // 
            // btnQuayVe
            // 
            this.btnQuayVe.Location = new System.Drawing.Point(12, 446);
            this.btnQuayVe.Name = "btnQuayVe";
            this.btnQuayVe.Size = new System.Drawing.Size(142, 40);
            this.btnQuayVe.TabIndex = 3;
            this.btnQuayVe.Text = "Quay về";
            this.btnQuayVe.UseVisualStyleBackColor = true;
            this.btnQuayVe.Click += new System.EventHandler(this.btnHuy_Click);
            // 
            // frmDangKyDG
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(481, 502);
            this.Controls.Add(this.btnQuayVe);
            this.Controls.Add(this.btnDangKy);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmDangKyDG";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Đăng ký độc giả";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmDangKyDG_FormClosed);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtHoTen;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCMND;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtSDT;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtDiaChi;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cboLoaiDG;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtNgaySinh;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblSoNgayMuonToiDa;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblSoSachMuonToiDa;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lblPhiThuongNien;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblTenLoaiDG;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnDangKy;
        private System.Windows.Forms.Button btnQuayVe;
        private System.Windows.Forms.Label lblLaGVCB;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtMSSVCB;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblLaSinhVien;
        private System.Windows.Forms.Label label14;
    }
}

