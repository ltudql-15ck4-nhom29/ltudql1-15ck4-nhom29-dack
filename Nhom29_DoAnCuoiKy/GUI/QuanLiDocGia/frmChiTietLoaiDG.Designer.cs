﻿namespace GUI
{
    partial class frmChiTietLoaiDG
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSoNgayMuonToiDa = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblSoSachMuonToiDa = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.lblPhiThuongNien = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblTenLoaiDG = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnQuayVe = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblMaLoaiDG = new System.Windows.Forms.Label();
            this.lblLaGVCB = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblLaSinhVien = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblSoNgayMuonToiDa
            // 
            this.lblSoNgayMuonToiDa.AutoSize = true;
            this.lblSoNgayMuonToiDa.Location = new System.Drawing.Point(227, 162);
            this.lblSoNgayMuonToiDa.Name = "lblSoNgayMuonToiDa";
            this.lblSoNgayMuonToiDa.Size = new System.Drawing.Size(85, 13);
            this.lblSoNgayMuonToiDa.TabIndex = 19;
            this.lblSoNgayMuonToiDa.Text = "soNgayMuonTD";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(46, 162);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(108, 13);
            this.label13.TabIndex = 18;
            this.label13.Text = "Số ngày mượn tối đa:";
            // 
            // lblSoSachMuonToiDa
            // 
            this.lblSoSachMuonToiDa.AutoSize = true;
            this.lblSoSachMuonToiDa.Location = new System.Drawing.Point(227, 139);
            this.lblSoSachMuonToiDa.Name = "lblSoSachMuonToiDa";
            this.lblSoSachMuonToiDa.Size = new System.Drawing.Size(85, 13);
            this.lblSoSachMuonToiDa.TabIndex = 17;
            this.lblSoSachMuonToiDa.Text = "soSachMuonTD";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(46, 139);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(108, 13);
            this.label17.TabIndex = 16;
            this.label17.Text = "Số sách mượn tối đa:";
            // 
            // lblPhiThuongNien
            // 
            this.lblPhiThuongNien.AutoSize = true;
            this.lblPhiThuongNien.Location = new System.Drawing.Point(227, 117);
            this.lblPhiThuongNien.Name = "lblPhiThuongNien";
            this.lblPhiThuongNien.Size = new System.Drawing.Size(36, 13);
            this.lblPhiThuongNien.TabIndex = 15;
            this.lblPhiThuongNien.Text = "phiTN";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(46, 117);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(86, 13);
            this.label11.TabIndex = 14;
            this.label11.Text = "Phí thường niên:";
            // 
            // lblTenLoaiDG
            // 
            this.lblTenLoaiDG.AutoSize = true;
            this.lblTenLoaiDG.Location = new System.Drawing.Point(227, 49);
            this.lblTenLoaiDG.Name = "lblTenLoaiDG";
            this.lblTenLoaiDG.Size = new System.Drawing.Size(38, 13);
            this.lblTenLoaiDG.TabIndex = 13;
            this.lblTenLoaiDG.Text = "tenDG";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(46, 49);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Tên loại độc giả:";
            // 
            // btnQuayVe
            // 
            this.btnQuayVe.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnQuayVe.Location = new System.Drawing.Point(208, 199);
            this.btnQuayVe.Name = "btnQuayVe";
            this.btnQuayVe.Size = new System.Drawing.Size(126, 43);
            this.btnQuayVe.TabIndex = 22;
            this.btnQuayVe.Text = "Quay về";
            this.btnQuayVe.UseVisualStyleBackColor = true;
            this.btnQuayVe.Click += new System.EventHandler(this.btnQuayVe_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(46, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 23;
            this.label1.Text = "Mã loại độc giả:";
            // 
            // lblMaLoaiDG
            // 
            this.lblMaLoaiDG.AutoSize = true;
            this.lblMaLoaiDG.Location = new System.Drawing.Point(227, 27);
            this.lblMaLoaiDG.Name = "lblMaLoaiDG";
            this.lblMaLoaiDG.Size = new System.Drawing.Size(57, 13);
            this.lblMaLoaiDG.TabIndex = 24;
            this.lblMaLoaiDG.Text = "maLoaiDG";
            // 
            // lblLaGVCB
            // 
            this.lblLaGVCB.AutoSize = true;
            this.lblLaGVCB.Location = new System.Drawing.Point(227, 94);
            this.lblLaGVCB.Name = "lblLaGVCB";
            this.lblLaGVCB.Size = new System.Drawing.Size(49, 13);
            this.lblLaGVCB.TabIndex = 28;
            this.lblLaGVCB.Text = "laGV/CB";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(46, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(145, 13);
            this.label3.TabIndex = 27;
            this.label3.Text = "Là giảng viên/cán bộ trường:";
            // 
            // lblLaSinhVien
            // 
            this.lblLaSinhVien.AutoSize = true;
            this.lblLaSinhVien.Location = new System.Drawing.Point(227, 72);
            this.lblLaSinhVien.Name = "lblLaSinhVien";
            this.lblLaSinhVien.Size = new System.Drawing.Size(29, 13);
            this.lblLaSinhVien.TabIndex = 26;
            this.lblLaSinhVien.Text = "laSV";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(46, 72);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 13);
            this.label5.TabIndex = 25;
            this.label5.Text = "Là sinh viên:";
            // 
            // frmChiTietLoaiDG
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(366, 262);
            this.Controls.Add(this.lblLaGVCB);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblLaSinhVien);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblMaLoaiDG);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnQuayVe);
            this.Controls.Add(this.lblSoNgayMuonToiDa);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.lblSoSachMuonToiDa);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.lblPhiThuongNien);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.lblTenLoaiDG);
            this.Controls.Add(this.label8);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmChiTietLoaiDG";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chi tiết loại độc giả";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSoNgayMuonToiDa;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblSoSachMuonToiDa;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lblPhiThuongNien;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblTenLoaiDG;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnQuayVe;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblMaLoaiDG;
        private System.Windows.Forms.Label lblLaGVCB;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblLaSinhVien;
        private System.Windows.Forms.Label label5;
    }
}