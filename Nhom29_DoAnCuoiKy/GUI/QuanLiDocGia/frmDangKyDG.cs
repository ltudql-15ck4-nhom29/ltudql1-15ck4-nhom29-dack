﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BLL;
using System.Text.RegularExpressions;

namespace GUI
{
    public partial class frmDangKyDG : Form
    {

        //List<DocGiaDTO> lstDocGia = null;
        List<LoaiDocGiaDTO> lstLoaiDocGia = null;
        public frmDangKyDG()
        {

            InitializeComponent();
            napListLoaiDocGia();
            hienThiCboLoaiDocGia();
            lblTenLoaiDG.Text="";
            lblLaSinhVien.Text = "";
            lblLaGVCB.Text="";
            lblPhiThuongNien.Text="";
            lblSoSachMuonToiDa.Text="";
            lblSoNgayMuonToiDa.Text="";
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmDangKyDG_FormClosed(object sender, FormClosedEventArgs e)
        {
            Owner.Show();
        }

        private void btnDangKy_Click(object sender, EventArgs e)
        {
            if (RegularExpression.laHoTen(txtHoTen.Text) == false)
            {
                MessageBox.Show("Họ tên không hợp lệ! Họ tên không được chứa số hay kí tự đặc biệt!", "Lỗi nhập liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (RegularExpression.laSo(txtCMND.Text) == false)
            {
                MessageBox.Show("Số CMND không hợp lệ!", "Lỗi nhập liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DateTime value;
            if (DateTime.TryParseExact(txtNgaySinh.Text, "d/M/yyyy", null, System.Globalization.DateTimeStyles.None, out value) == false)
            {
                MessageBox.Show("Ngày sinh không hợp lệ!", "Lỗi nhập liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (RegularExpression.laSo(txtSDT.Text) == false)
            {
                MessageBox.Show("Số điện thoại không hợp lệ!", "Lỗi nhập liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (RegularExpression.laEmail(txtEmail.Text) == false)
            {
                MessageBox.Show("Email không hợp lệ!", "Lỗi nhập liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if(cboLoaiDG.SelectedIndex == -1)
            {
                MessageBox.Show("Chưa chọn loại độc giả!", "Lỗi nhập liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (txtMSSVCB.ReadOnly == false && !RegularExpression.laSo(txtMSSVCB.Text))
            {
                MessageBox.Show("Mã số sinh viên/Mã số cán bộ không hợp lệ!", "Lỗi nhập liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DocGiaBLL bll = new DocGiaBLL();
            DocGiaDTO dg = taoDocGia();

            if (bll.themDocGia(dg) == 1)
            {
                MessageBox.Show("Đã thêm độc giả thành công!", "Thêm độc giả", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
            else
            {
                MessageBox.Show("Đã xảy ra lỗi khi thêm độc giả!", "Thêm độc giả", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        void napListLoaiDocGia()
        {
            LoaiDocGiaBLL bll = new LoaiDocGiaBLL();
            lstLoaiDocGia = bll.layDSLoaiDocGia();
        }


        void hienThiCboLoaiDocGia()
        {
            cboLoaiDG.Items.Clear();
            for (int i = 0; i < lstLoaiDocGia.Count; i++)
            {
                cboLoaiDG.Items.Add(lstLoaiDocGia[i]);
            }
        }

        DocGiaDTO taoDocGia()
        {
            DocGiaDTO dg = new DocGiaDTO();
            dg.HoTen = txtHoTen.Text;
            dg.CMND = txtCMND.Text;
            DateTime value;
            DateTime.TryParseExact(txtNgaySinh.Text, "d/M/yyyy", null, System.Globalization.DateTimeStyles.None, out value);
            dg.NgaySinh = value;
            dg.SoDienThoai = txtSDT.Text;
            dg.Email = txtEmail.Text;
            dg.DiaChi = txtDiaChi.Text;
            dg.LoaiDocGia = cboLoaiDG.SelectedIndex;
            dg.MSSV = null;
            dg.MSCB = null;
            LoaiDocGiaDTO ldg = (LoaiDocGiaDTO)cboLoaiDG.SelectedItem;
            if (ldg.LaGVCB)
            {
                dg.MSCB = txtMSSVCB.Text;
            }
            if (ldg.LaSinhVien)
            {
                dg.MSSV = txtMSSVCB.Text;
            }            
            return dg;
        }

        private void cboLoaiDG_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cboLoaiDG.SelectedIndex > -1)
            {
                //item lấy ra từ đây rồi nên không cần lấy nữa
                //khi lấy ra từ ComboBox item là 1 object nên phải ép kiểu
                //như phía dưới là ép (LoaiDocGiaDTO)
                LoaiDocGiaDTO ldg = (LoaiDocGiaDTO) cboLoaiDG.SelectedItem;
                if(ldg.LaGVCB || ldg.LaSinhVien)
                {
                    txtMSSVCB.ReadOnly = false;
                }
                else
                {
                    txtMSSVCB.ReadOnly = true;
                    txtMSSVCB.Text = "";
                }

                //Lúc add vào ComboBox là 1 class LoaiDocGiaDTO thì lúc chọn ComboBox
                //lấy item đó ra là ok r

                lblTenLoaiDG.Text = ldg.TenLoai;
                if (ldg.LaSinhVien)
                    lblLaSinhVien.Text = "Có";
                else
                    lblLaSinhVien.Text = "Không";
                if (ldg.LaGVCB)
                    lblLaGVCB.Text = "Có";
                else
                    lblLaGVCB.Text = "Không";
                lblPhiThuongNien.Text = String.Format("{0:n0}", ldg.PhiThuongNien);
                lblSoNgayMuonToiDa.Text = ldg.SoNgayMuonMax.ToString();
                lblSoSachMuonToiDa.Text = ldg.SoTaiLieuMuonMax.ToString();
            }
        }
    }
}
