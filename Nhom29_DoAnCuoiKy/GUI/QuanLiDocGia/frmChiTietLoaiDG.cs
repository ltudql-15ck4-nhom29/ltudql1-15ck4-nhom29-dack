﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;

namespace GUI
{
    public partial class frmChiTietLoaiDG : Form
    {
        public frmChiTietLoaiDG(LoaiDocGiaDTO ldg)
        {
            InitializeComponent();

            lblMaLoaiDG.Text = ldg.MaLoai.ToString();
            lblTenLoaiDG.Text = ldg.TenLoai;
            lblLaSinhVien.Text = "Không";
            if(ldg.LaSinhVien)
            {
                lblLaSinhVien.Text = "Có";
            }
            lblLaGVCB.Text = "Không";
            if (ldg.LaGVCB)
            {
                lblLaGVCB.Text = "Có";
            }
            lblPhiThuongNien.Text = String.Format("{0:n0}", ldg.PhiThuongNien);
            lblSoNgayMuonToiDa.Text = ldg.SoNgayMuonMax.ToString();
            lblSoSachMuonToiDa.Text = ldg.SoTaiLieuMuonMax.ToString();
        }

        private void btnQuayVe_Click(object sender, EventArgs e)
        {

        }
    }
}
