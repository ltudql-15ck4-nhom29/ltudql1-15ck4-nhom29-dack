﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BLL;

namespace GUI
{
    public partial class frmTimKiemDG : Form
    {
        List<DocGiaDTO> lstDocGia = null;
        List<LoaiDocGiaDTO> lstLoaiDocGia = null;
        List<DocGiaDTO> lstHienThi = null;

        public frmTimKiemDG()
        {
            InitializeComponent();

            napDSDocGia();
            napDSLoaiDocGia();

            lstHienThi = lstDocGia;

            hienThiLvDocGia();
        }

        void napDSDocGia()
        {
            DocGiaBLL bll = new DocGiaBLL();
            lstDocGia = bll.layDSDocGiaDayDuCoQuaHan();
        }

        void napDSLoaiDocGia()
        {
            LoaiDocGiaBLL bll = new LoaiDocGiaBLL();
            lstLoaiDocGia = bll.layDSLoaiDocGia();
        }

        void hienThiLvDocGia()
        {
            lvDSDG.Items.Clear();
            for(int i = 0; i < lstHienThi.Count; i++)
            {
                DocGiaDTO dg = lstHienThi[i];
                LoaiDocGiaDTO ldg = LoaiDocGiaDTO.layLoaiDocGiaTheoDocGia(dg, lstLoaiDocGia);
                ListViewItem item = new ListViewItem(dg.MaDocGia.ToString());
                item.SubItems.Add(dg.HoTen);
                item.SubItems.Add(dg.CMND.ToString());
                if(ldg.LaSinhVien)
                {
                    item.SubItems.Add(dg.MSSV.ToString());
                }
                else if(ldg.LaGVCB)
                {
                    item.SubItems.Add(dg.MSCB.ToString());
                }
                else
                {
                    item.SubItems.Add("");
                }
                item.SubItems.Add(ldg.ToString());
                item.SubItems.Add(dg.SoTaiLieuDangMuon.ToString());
                item.SubItems.Add(dg.SoTaiLieuDangMuonQuaHan.ToString());
                item.SubItems.Add(String.Format("{0:n0}",ldg.PhiThuongNien));
                lvDSDG.Items.Add(item);
            }
        }

        void timKiemMaDG()
        {
            lstHienThi = new List<DocGiaDTO>();
            int iSearch = int.Parse(txtNoiDungLoc.Text);
            for(int i = 0; i < lstDocGia.Count; i++)
            {
                DocGiaDTO dg = lstDocGia[i];
                if(dg.MaDocGia == iSearch)
                {
                    lstHienThi.Add(dg);
                }
            }
            hienThiLvDocGia();
        }

        void timKiemCMND()
        {
            lstHienThi = new List<DocGiaDTO>();
            string sSearch = txtNoiDungLoc.Text;
            for (int i = 0; i < lstDocGia.Count; i++)
            {
                DocGiaDTO dg = lstDocGia[i];
                string sText = dg.CMND;
                if (sText != null && sText.Contains(sSearch))
                {
                    lstHienThi.Add(dg);
                }
            }
            hienThiLvDocGia();
        }

        void timKiemMSSV()
        {
            lstHienThi = new List<DocGiaDTO>();
            string sSearch = txtNoiDungLoc.Text;
            for (int i = 0; i < lstDocGia.Count; i++)
            {
                DocGiaDTO dg = lstDocGia[i];
                string sText = dg.MSSV;
                if (sText != null && sText.Contains(sSearch))
                {
                    lstHienThi.Add(dg);
                }
            }
            hienThiLvDocGia();
        }

        void timKiemMSCB()
        {
            lstHienThi = new List<DocGiaDTO>();
            string sSearch = txtNoiDungLoc.Text;
            for (int i = 0; i < lstDocGia.Count; i++)
            {
                DocGiaDTO dg = lstDocGia[i];
                string sText = dg.MSCB;
                if (sText != null && sText.Contains(sSearch))
                {
                    lstHienThi.Add(dg);
                }
            }
            hienThiLvDocGia();
        }

        void timKiemHoTen()
        {
            lstHienThi = new List<DocGiaDTO>();
            string sSearch = txtNoiDungLoc.Text;
            for (int i = 0; i < lstDocGia.Count; i++)
            {
                DocGiaDTO dg = lstDocGia[i];
                string sText = dg.HoTen;
                if (sText.Contains(sSearch))
                {
                    lstHienThi.Add(dg);
                }
            }
            hienThiLvDocGia();
        }

        private void capNhatForm(object sender, EventArgs e)
        {
            napDSDocGia();

            lstHienThi = lstDocGia;

            hienThiLvDocGia();
        }

        private void btnLapPhieuMuon_Click(object sender, EventArgs e)
        {
            frmLapPM f = new frmLapPM();
            f.FormClosed += capNhatForm;
            f.Show(this);
            Hide();
        }

        private void frmTimKiemDG_FormClosed(object sender, FormClosedEventArgs e)
        {
            Owner.Show();
        }

        private void btnQuayVe_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnXemChiTiet_Click(object sender, EventArgs e)
        {
            if (lvDSDG.SelectedItems.Count == 0)
            {
                MessageBox.Show("Không có độc giả nào được chọn!", "Xem chi tiết", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DocGiaDTO dg = lstHienThi[lvDSDG.SelectedIndices[0]];
            frmChiTietDG f = new frmChiTietDG(dg);
            f.ShowDialog();
            capNhatForm(this, new EventArgs());
        }

        private void btnLapPhieuCanhCao_Click(object sender, EventArgs e)
        {
            if (lvDSDG.SelectedItems.Count == 0)
            {
                MessageBox.Show("Không có độc giả nào được chọn!", "Lập phiếu cảnh cáo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DocGiaDTO dg = lstHienThi[lvDSDG.SelectedIndices[0]];
            frmLapPhieuCC f = new frmLapPhieuCC(dg);
            f.ShowDialog();
            capNhatForm(this, new EventArgs());
        }

        private void btnLapPhieuTra_Click(object sender, EventArgs e)
        {
            frmLapPT f = new frmLapPT();
            f.FormClosed += capNhatForm;
            f.Show(this);
            Hide();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (lvDSDG.SelectedItems.Count == 0)
            {
                MessageBox.Show("Không có độc giả nào được chọn!", "Xóa độc giả", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DocGiaDTO dg = lstHienThi[lvDSDG.SelectedIndices[0]];
            if (dg.SoTaiLieuDangMuon > 0)
            {
                MessageBox.Show("Không thể xóa độc giả này vì có ít nhất một tài liệu được mượn bởi độc giả này!", "Xóa độc giả", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if(DialogResult.Yes == MessageBox.Show("Hành động này không thể hoàn tác! Bạn có chắc muốn xóa độc giả này không?", "Xóa độc giả", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                DocGiaBLL bll = new DocGiaBLL();
                if(bll.xoaDocGia(dg) == 1)
                {
                    MessageBox.Show("Xóa độc giả thành công!", "Xóa độc giả", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lstHienThi.Remove(dg);
                    lstDocGia.Remove(dg);
                    hienThiLvDocGia();
                }
                else
                {
                    MessageBox.Show("Xóa độc giả thất bại!", "Xóa độc giả", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnXemHet_Click(object sender, EventArgs e)
        {
            lstHienThi = lstDocGia;
            hienThiLvDocGia();
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            if(cboLoaiBoLoc.SelectedIndex == -1)
            {
                MessageBox.Show("Loại bộ lọc chưa được chọn!", "Tìm kiếm", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            switch(cboLoaiBoLoc.SelectedIndex)
            {
                case 0:
                    if(!RegularExpression.laSo(txtNoiDungLoc.Text))
                    {
                        MessageBox.Show("Nội dung tìm kiếm phải là số!", "Tìm kiếm", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    timKiemMaDG();
                    break;
                case 1:
                    if (!RegularExpression.laSo(txtNoiDungLoc.Text))
                    {
                        MessageBox.Show("Nội dung tìm kiếm phải là số!", "Tìm kiếm", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    timKiemCMND();
                    break;
                case 2:
                    if (!RegularExpression.laSo(txtNoiDungLoc.Text))
                    {
                        MessageBox.Show("Nội dung tìm kiếm phải là số!", "Tìm kiếm", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    timKiemMSSV();
                    break;
                case 3:
                    if (!RegularExpression.laSo(txtNoiDungLoc.Text))
                    {
                        MessageBox.Show("Nội dung tìm kiếm phải là số!", "Tìm kiếm", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    timKiemMSCB();
                    break;
                case 4:
                    if (!RegularExpression.laHoTen(txtNoiDungLoc.Text))
                    {
                        MessageBox.Show("Nội dung tìm kiếm phải là họ tên! Họ tên không được chứa số hay kí tự đặc biệt!", "Tìm kiếm", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    timKiemHoTen();
                    break;
            }
        }
    }
}
