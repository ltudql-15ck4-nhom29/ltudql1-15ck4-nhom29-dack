﻿namespace GUI
{
    partial class frmManHinhChucNang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grbQuanLiDG = new System.Windows.Forms.GroupBox();
            this.btnBaoCaoLoaiDocGia = new System.Windows.Forms.Button();
            this.btnBaoCaoDocGia = new System.Windows.Forms.Button();
            this.btnTimKiemDG = new System.Windows.Forms.Button();
            this.btnDangKyDG = new System.Windows.Forms.Button();
            this.grbQuanLiTL = new System.Windows.Forms.GroupBox();
            this.btnBaoCaoLoaiTaiLieu = new System.Windows.Forms.Button();
            this.btnBaoCaoTaiLieu = new System.Windows.Forms.Button();
            this.btnTimKiemTL = new System.Windows.Forms.Button();
            this.btnTiepNhanTL = new System.Windows.Forms.Button();
            this.grbQuanLiMT = new System.Windows.Forms.GroupBox();
            this.btnTimKiemPT = new System.Windows.Forms.Button();
            this.btnLapPT = new System.Windows.Forms.Button();
            this.btnTimKiemPM = new System.Windows.Forms.Button();
            this.btnLapPM = new System.Windows.Forms.Button();
            this.grbQuanLiND = new System.Windows.Forms.GroupBox();
            this.btnQuanLiND = new System.Windows.Forms.Button();
            this.btnDangXuat = new System.Windows.Forms.Button();
            this.grbQuanLiDG.SuspendLayout();
            this.grbQuanLiTL.SuspendLayout();
            this.grbQuanLiMT.SuspendLayout();
            this.grbQuanLiND.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbQuanLiDG
            // 
            this.grbQuanLiDG.Controls.Add(this.btnBaoCaoLoaiDocGia);
            this.grbQuanLiDG.Controls.Add(this.btnBaoCaoDocGia);
            this.grbQuanLiDG.Controls.Add(this.btnTimKiemDG);
            this.grbQuanLiDG.Controls.Add(this.btnDangKyDG);
            this.grbQuanLiDG.Enabled = false;
            this.grbQuanLiDG.Location = new System.Drawing.Point(12, 12);
            this.grbQuanLiDG.Name = "grbQuanLiDG";
            this.grbQuanLiDG.Size = new System.Drawing.Size(187, 247);
            this.grbQuanLiDG.TabIndex = 0;
            this.grbQuanLiDG.TabStop = false;
            this.grbQuanLiDG.Text = "Quản lí độc giả";
            // 
            // btnBaoCaoLoaiDocGia
            // 
            this.btnBaoCaoLoaiDocGia.Location = new System.Drawing.Point(20, 186);
            this.btnBaoCaoLoaiDocGia.Name = "btnBaoCaoLoaiDocGia";
            this.btnBaoCaoLoaiDocGia.Size = new System.Drawing.Size(145, 43);
            this.btnBaoCaoLoaiDocGia.TabIndex = 5;
            this.btnBaoCaoLoaiDocGia.Text = "Báo cáo loại độc giả";
            this.btnBaoCaoLoaiDocGia.UseVisualStyleBackColor = true;
            this.btnBaoCaoLoaiDocGia.Click += new System.EventHandler(this.btnBaoCaoLoaiDocGia_Click);
            // 
            // btnBaoCaoDocGia
            // 
            this.btnBaoCaoDocGia.Location = new System.Drawing.Point(20, 137);
            this.btnBaoCaoDocGia.Name = "btnBaoCaoDocGia";
            this.btnBaoCaoDocGia.Size = new System.Drawing.Size(145, 43);
            this.btnBaoCaoDocGia.TabIndex = 4;
            this.btnBaoCaoDocGia.Text = "Báo cáo độc giả";
            this.btnBaoCaoDocGia.UseVisualStyleBackColor = true;
            this.btnBaoCaoDocGia.Click += new System.EventHandler(this.btnBaoCaoDocGia_Click);
            // 
            // btnTimKiemDG
            // 
            this.btnTimKiemDG.Location = new System.Drawing.Point(20, 88);
            this.btnTimKiemDG.Name = "btnTimKiemDG";
            this.btnTimKiemDG.Size = new System.Drawing.Size(145, 43);
            this.btnTimKiemDG.TabIndex = 1;
            this.btnTimKiemDG.Text = "Tìm kiếm độc giả";
            this.btnTimKiemDG.UseVisualStyleBackColor = true;
            this.btnTimKiemDG.Click += new System.EventHandler(this.btnTimKiemDG_Click);
            // 
            // btnDangKyDG
            // 
            this.btnDangKyDG.Location = new System.Drawing.Point(20, 39);
            this.btnDangKyDG.Name = "btnDangKyDG";
            this.btnDangKyDG.Size = new System.Drawing.Size(145, 43);
            this.btnDangKyDG.TabIndex = 0;
            this.btnDangKyDG.Text = "Đăng ký độc giả";
            this.btnDangKyDG.UseVisualStyleBackColor = true;
            this.btnDangKyDG.Click += new System.EventHandler(this.btnDangKyDG_Click);
            // 
            // grbQuanLiTL
            // 
            this.grbQuanLiTL.Controls.Add(this.btnBaoCaoLoaiTaiLieu);
            this.grbQuanLiTL.Controls.Add(this.btnBaoCaoTaiLieu);
            this.grbQuanLiTL.Controls.Add(this.btnTimKiemTL);
            this.grbQuanLiTL.Controls.Add(this.btnTiepNhanTL);
            this.grbQuanLiTL.Enabled = false;
            this.grbQuanLiTL.Location = new System.Drawing.Point(205, 12);
            this.grbQuanLiTL.Name = "grbQuanLiTL";
            this.grbQuanLiTL.Size = new System.Drawing.Size(187, 247);
            this.grbQuanLiTL.TabIndex = 2;
            this.grbQuanLiTL.TabStop = false;
            this.grbQuanLiTL.Text = "Quản lí tài liệu";
            // 
            // btnBaoCaoLoaiTaiLieu
            // 
            this.btnBaoCaoLoaiTaiLieu.Location = new System.Drawing.Point(20, 186);
            this.btnBaoCaoLoaiTaiLieu.Name = "btnBaoCaoLoaiTaiLieu";
            this.btnBaoCaoLoaiTaiLieu.Size = new System.Drawing.Size(145, 43);
            this.btnBaoCaoLoaiTaiLieu.TabIndex = 5;
            this.btnBaoCaoLoaiTaiLieu.Text = "Báo cáo loại tài liệu";
            this.btnBaoCaoLoaiTaiLieu.UseVisualStyleBackColor = true;
            this.btnBaoCaoLoaiTaiLieu.Click += new System.EventHandler(this.btnBaoCaoLoaiTaiLieu_Click);
            // 
            // btnBaoCaoTaiLieu
            // 
            this.btnBaoCaoTaiLieu.Location = new System.Drawing.Point(20, 137);
            this.btnBaoCaoTaiLieu.Name = "btnBaoCaoTaiLieu";
            this.btnBaoCaoTaiLieu.Size = new System.Drawing.Size(145, 43);
            this.btnBaoCaoTaiLieu.TabIndex = 4;
            this.btnBaoCaoTaiLieu.Text = "Báo cáo tài liệu";
            this.btnBaoCaoTaiLieu.UseVisualStyleBackColor = true;
            this.btnBaoCaoTaiLieu.Click += new System.EventHandler(this.btnBaoCaoTaiLieu_Click);
            // 
            // btnTimKiemTL
            // 
            this.btnTimKiemTL.Location = new System.Drawing.Point(20, 88);
            this.btnTimKiemTL.Name = "btnTimKiemTL";
            this.btnTimKiemTL.Size = new System.Drawing.Size(145, 43);
            this.btnTimKiemTL.TabIndex = 1;
            this.btnTimKiemTL.Text = "Tìm kiếm tài liệu";
            this.btnTimKiemTL.UseVisualStyleBackColor = true;
            this.btnTimKiemTL.Click += new System.EventHandler(this.btnTimKiemTL_Click);
            // 
            // btnTiepNhanTL
            // 
            this.btnTiepNhanTL.Location = new System.Drawing.Point(20, 39);
            this.btnTiepNhanTL.Name = "btnTiepNhanTL";
            this.btnTiepNhanTL.Size = new System.Drawing.Size(145, 43);
            this.btnTiepNhanTL.TabIndex = 0;
            this.btnTiepNhanTL.Text = "Tiếp nhận tài liệu";
            this.btnTiepNhanTL.UseVisualStyleBackColor = true;
            this.btnTiepNhanTL.Click += new System.EventHandler(this.btnTiepNhanTL_Click);
            // 
            // grbQuanLiMT
            // 
            this.grbQuanLiMT.Controls.Add(this.btnTimKiemPT);
            this.grbQuanLiMT.Controls.Add(this.btnLapPT);
            this.grbQuanLiMT.Controls.Add(this.btnTimKiemPM);
            this.grbQuanLiMT.Controls.Add(this.btnLapPM);
            this.grbQuanLiMT.Enabled = false;
            this.grbQuanLiMT.Location = new System.Drawing.Point(398, 12);
            this.grbQuanLiMT.Name = "grbQuanLiMT";
            this.grbQuanLiMT.Size = new System.Drawing.Size(187, 247);
            this.grbQuanLiMT.TabIndex = 3;
            this.grbQuanLiMT.TabStop = false;
            this.grbQuanLiMT.Text = "Quản lí mượn - trả";
            // 
            // btnTimKiemPT
            // 
            this.btnTimKiemPT.Location = new System.Drawing.Point(20, 186);
            this.btnTimKiemPT.Name = "btnTimKiemPT";
            this.btnTimKiemPT.Size = new System.Drawing.Size(145, 43);
            this.btnTimKiemPT.TabIndex = 3;
            this.btnTimKiemPT.Text = "Tìm kiếm phiếu trả";
            this.btnTimKiemPT.UseVisualStyleBackColor = true;
            this.btnTimKiemPT.Click += new System.EventHandler(this.btnTimKiemPT_Click);
            // 
            // btnLapPT
            // 
            this.btnLapPT.Location = new System.Drawing.Point(20, 137);
            this.btnLapPT.Name = "btnLapPT";
            this.btnLapPT.Size = new System.Drawing.Size(145, 43);
            this.btnLapPT.TabIndex = 2;
            this.btnLapPT.Text = "Lập phiếu trả";
            this.btnLapPT.UseVisualStyleBackColor = true;
            this.btnLapPT.Click += new System.EventHandler(this.btnLapPT_Click);
            // 
            // btnTimKiemPM
            // 
            this.btnTimKiemPM.Location = new System.Drawing.Point(20, 88);
            this.btnTimKiemPM.Name = "btnTimKiemPM";
            this.btnTimKiemPM.Size = new System.Drawing.Size(145, 43);
            this.btnTimKiemPM.TabIndex = 1;
            this.btnTimKiemPM.Text = "Tìm kiếm phiếu mượn";
            this.btnTimKiemPM.UseVisualStyleBackColor = true;
            this.btnTimKiemPM.Click += new System.EventHandler(this.btnTimKiemPM_Click);
            // 
            // btnLapPM
            // 
            this.btnLapPM.Location = new System.Drawing.Point(20, 39);
            this.btnLapPM.Name = "btnLapPM";
            this.btnLapPM.Size = new System.Drawing.Size(145, 43);
            this.btnLapPM.TabIndex = 0;
            this.btnLapPM.Text = "Lập phiếu mượn";
            this.btnLapPM.UseVisualStyleBackColor = true;
            this.btnLapPM.Click += new System.EventHandler(this.btnLapPM_Click);
            // 
            // grbQuanLiND
            // 
            this.grbQuanLiND.Controls.Add(this.btnQuanLiND);
            this.grbQuanLiND.Enabled = false;
            this.grbQuanLiND.Location = new System.Drawing.Point(591, 12);
            this.grbQuanLiND.Name = "grbQuanLiND";
            this.grbQuanLiND.Size = new System.Drawing.Size(187, 247);
            this.grbQuanLiND.TabIndex = 3;
            this.grbQuanLiND.TabStop = false;
            this.grbQuanLiND.Text = "Quản lí người dùng";
            // 
            // btnQuanLiND
            // 
            this.btnQuanLiND.Location = new System.Drawing.Point(20, 39);
            this.btnQuanLiND.Name = "btnQuanLiND";
            this.btnQuanLiND.Size = new System.Drawing.Size(145, 43);
            this.btnQuanLiND.TabIndex = 0;
            this.btnQuanLiND.Text = "Quản lí người dùng";
            this.btnQuanLiND.UseVisualStyleBackColor = true;
            this.btnQuanLiND.Click += new System.EventHandler(this.btnQuanLiND_Click);
            // 
            // btnDangXuat
            // 
            this.btnDangXuat.Location = new System.Drawing.Point(12, 265);
            this.btnDangXuat.Name = "btnDangXuat";
            this.btnDangXuat.Size = new System.Drawing.Size(156, 37);
            this.btnDangXuat.TabIndex = 4;
            this.btnDangXuat.Text = "Đăng xuất";
            this.btnDangXuat.UseVisualStyleBackColor = true;
            this.btnDangXuat.Click += new System.EventHandler(this.btnDangXuat_Click);
            // 
            // frmManHinhChucNang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 318);
            this.Controls.Add(this.btnDangXuat);
            this.Controls.Add(this.grbQuanLiND);
            this.Controls.Add(this.grbQuanLiMT);
            this.Controls.Add(this.grbQuanLiTL);
            this.Controls.Add(this.grbQuanLiDG);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmManHinhChucNang";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Màn hình chức năng";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmManHinhChucNang_FormClosed);
            this.grbQuanLiDG.ResumeLayout(false);
            this.grbQuanLiTL.ResumeLayout(false);
            this.grbQuanLiMT.ResumeLayout(false);
            this.grbQuanLiND.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grbQuanLiDG;
        private System.Windows.Forms.Button btnTimKiemDG;
        private System.Windows.Forms.Button btnDangKyDG;
        private System.Windows.Forms.GroupBox grbQuanLiTL;
        private System.Windows.Forms.Button btnTimKiemTL;
        private System.Windows.Forms.Button btnTiepNhanTL;
        private System.Windows.Forms.GroupBox grbQuanLiMT;
        private System.Windows.Forms.Button btnTimKiemPM;
        private System.Windows.Forms.Button btnLapPM;
        private System.Windows.Forms.Button btnTimKiemPT;
        private System.Windows.Forms.Button btnLapPT;
        private System.Windows.Forms.GroupBox grbQuanLiND;
        private System.Windows.Forms.Button btnQuanLiND;
        private System.Windows.Forms.Button btnDangXuat;
        private System.Windows.Forms.Button btnBaoCaoLoaiDocGia;
        private System.Windows.Forms.Button btnBaoCaoDocGia;
        private System.Windows.Forms.Button btnBaoCaoLoaiTaiLieu;
        private System.Windows.Forms.Button btnBaoCaoTaiLieu;

    }
}