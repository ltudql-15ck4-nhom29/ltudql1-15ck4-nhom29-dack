﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BLL;

namespace GUI
{
    public partial class frmQuanLiND : Form
    {
        //Để dùng một Class nào đó, cần chú ý yêu cầu "using",
        //ví dụ để dùng DTO thì "using DTO", hay DAL thì "using DAL", hay BLL thì "using BLL".
        
        //Để cho chương trình khởi động một form nào đó thì thay đổi dòng
        //"Application.Run()" trong tập tin Program.cs

        List<NguoiDungDTO> lstNguoiDung = null;
        List<LoaiCapBacDTO> lstLoaiCapBac = null;

        public frmQuanLiND()
        {
            InitializeComponent();
            
            //Chạy tất cả các hàm "nạp" trước khi chạy các hàm "hiển thị"
            //như vậy mới đủ dữ liệu để hiển thị.
            napListNguoiDung();
            napListLoaiCapBac();

            hienThiLvNguoiDung();
            hienThiCboLoaiCapBac();

            lblMaND.Text = "";
        }

        //***** TẤT CẢ CÁC HÀM KHÔNG LIÊN QUAN ĐẾN CONTROL ĐẶT Ở SAU HÀM KHỞI TẠO FORM *****
        //***** VÀ TRƯỚC CÁC HÀM LIÊN QUAN ĐẾN CONTROL *****

        void napListNguoiDung()
        {
            NguoiDungBLL bll = new NguoiDungBLL();
            lstNguoiDung = bll.layDSNguoiDung();
        }

        void hienThiLvNguoiDung()
        {
            lvDSND.Items.Clear();
            for (int i = 0; i < lstNguoiDung.Count; i++)
            {
                NguoiDungDTO nd = lstNguoiDung[i];
                ListViewItem item = new ListViewItem(nd.MaNguoiDung.ToString());
                item.SubItems.Add(nd.TenNguoiDung);
                LoaiCapBacDTO lcb = LoaiCapBacDTO.layLoaiCapBac(nd.CapBac, lstLoaiCapBac);
                item.SubItems.Add(lcb.ToString());
                lvDSND.Items.Add(item);
            }
        }

        void napListLoaiCapBac()
        {
            LoaiCapBacBLL bll = new LoaiCapBacBLL();
            lstLoaiCapBac = bll.layDSLoaiCapBac();
        }

        void hienThiCboLoaiCapBac()
        {
            cboCapBacND.Items.Clear();
            for(int i = 0; i < lstLoaiCapBac.Count; i++)
            {
                //ComboBox khi thêm một item sẽ tự gọi hàm ToString() của Class đó
                //để hiển thị. Vì vậy cần phải override hàm ToString() của 1 Class
                //nếu dùng class đó để hiển thị trong ComboBox (xem thêm LoaiCapBacDTO.cs).
                cboCapBacND.Items.Add(lstLoaiCapBac[i]);
            }
        }

        NguoiDungDTO taoNguoiDung()
        {
            NguoiDungDTO nd = new NguoiDungDTO();
            if (RegularExpression.laTenTaiKhoan(txtTenND.Text) == false)
            {
                MessageBox.Show("Tên người dùng không hợp lệ! Tên người dùng chỉ chứa kí tự chữ và số, và phải bắt đầu bằng ký tự chữ!", "Lỗi nhập liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
            if (RegularExpression.laMatKhau(txtMatKhauNDMoi.Text) == false)
            {
                MessageBox.Show("Mật khẩu không hợp lệ! Mật khẩu phải chứa ít nhất một ký tự chữ thường, một ký tự chữ hoa và một số!", "Lỗi nhập liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
            int n;
            if(int.TryParse(lblMaND.Text, out n))
            {
                nd.MaNguoiDung = n;
            }
            nd.TenNguoiDung = txtTenND.Text;
            nd.MatKhau = txtMatKhauNDMoi.Text;
            nd.CapBac = cboCapBacND.SelectedIndex + 1;
            return nd;
        }
        
        //[CHƯA CÓ HÀM KIỂM TRA THÔNG TIN NGƯỜI DÙNG (kiểm tra TextBox, ComboBox)]

        //***** TẤT CẢ CÁC HÀM KHÔNG LIÊN QUAN ĐẾN CONTROL ĐẶT Ở SAU HÀM KHỞI TẠO FORM *****
        //***** VÀ TRƯỚC CÁC HÀM LIÊN QUAN ĐẾN CONTROL *****

        private void btnQuayVe_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmQuanLiND_FormClosed(object sender, FormClosedEventArgs e)
        {
            Owner.Show();
        }

        private void lvDSND_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(lvDSND.SelectedItems.Count > 0)
            {
                //SelectedIndices[0] là lấy index của item đầu tiên được chọn trong ListView
                NguoiDungDTO nd = lstNguoiDung[lvDSND.SelectedIndices[0]];
                lblMaND.Text = nd.MaNguoiDung.ToString();
                txtTenND.Text = nd.TenNguoiDung;
                txtMatKhauNDMoi.Text = "";
                cboCapBacND.SelectedIndex = nd.CapBac - 1;
            }
            else
            {
                lblMaND.Text = "";
                txtTenND.Text = "";
                txtMatKhauNDMoi.Text = "";
                cboCapBacND.SelectedIndex = -1;
            }
        }

        private void btnThemND_Click(object sender, EventArgs e)
        {
            NguoiDungBLL bll = new NguoiDungBLL();
            NguoiDungDTO nd = taoNguoiDung();

            if(nd == null)
            {
                return;
            }
            //Kiểm tra người dùng đã tồn tại hay chưa
            if(bll.layNguoiDung(nd.TenNguoiDung) != null)
            {
                MessageBox.Show("Tên người dùng đã tồn tại!", "Thêm người dùng", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //Sau khi INSERT, UPDATE, DELETE
            //Kiểm tra giá trị trả về (số dòng bị ảnh hưởng - xem thêm trong NguoiDungDAL.cs)
            //Nếu bằng 1 là thành công
            if(bll.themNguoiDung(nd) == 1)
            {
                //Yêu cầu sử dụng MessageBox đầy đủ 4 thành phần:
                //Nội dung, Tiêu đề, Các button, Icon
                MessageBox.Show("Đã thêm người dùng thành công!", "Thêm người dùng", MessageBoxButtons.OK, MessageBoxIcon.Information);

                //Sau khi thực hiện các lệnh thêm, cập nhật, xóa người dùng cần load lại ListView
                napListNguoiDung();
                hienThiLvNguoiDung();
            }
            else
            {
                MessageBox.Show("Đã xảy ra lỗi khi thêm người dùng!", "Thêm người dùng", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCapNhatND_Click(object sender, EventArgs e)
        {
            //Kiểm tra ListView có item nào được chọn hay không
            //Nếu count = 0 thì báo lỗi và return
            if(lvDSND.SelectedItems.Count == 0)
            {
                MessageBox.Show("Không có người dùng nào được chọn!", "Cập nhật người dùng", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            NguoiDungBLL bll = new NguoiDungBLL();
            NguoiDungDTO nd = taoNguoiDung();
            if(nd == null)
            {
                return;
            }
            if (bll.capNhatNguoiDung(nd) == 1)
            {
                MessageBox.Show("Đã cập nhật người dùng thành công!", "Cập nhật người dùng", MessageBoxButtons.OK, MessageBoxIcon.Information);

                napListNguoiDung();
                hienThiLvNguoiDung();
            }
            else
            {
                MessageBox.Show("Đã xảy ra lỗi khi cập nhật người dùng!", "Cập nhật người dùng", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnXoaND_Click(object sender, EventArgs e)
        {
            if (lvDSND.SelectedItems.Count == 0)
            {
                MessageBox.Show("Không có người dùng nào được chọn!", "Xóa người dùng", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //Hiển thị thông báo yêu cầu xác nhận xóa
            //Nếu người dùng chọn NO, return
            if(DialogResult.No == MessageBox.Show("Bạn có chắc muốn xóa người dùng này không?", "Xóa người dùng", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                return;
            }

            NguoiDungBLL bll = new NguoiDungBLL();
            NguoiDungDTO nd = new NguoiDungDTO();
            nd.MaNguoiDung = int.Parse(lblMaND.Text);
            if (bll.xoaNguoiDung(nd) == 1)
            {
                MessageBox.Show("Đã xóa người dùng thành công!", "Xóa người dùng", MessageBoxButtons.OK, MessageBoxIcon.Information);

                napListNguoiDung();
                hienThiLvNguoiDung();
            }
            else
            {
                MessageBox.Show("Đã xảy ra lỗi khi xóa người dùng!", "Xóa người dùng", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
