﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BLL;

namespace GUI
{
    public partial class frmDangNhap : Form
    {
        public frmDangNhap()
        {
            InitializeComponent();
        }

        private void btnDangNhap_Click(object sender, EventArgs e)
        {
            NguoiDungDTO nd = new NguoiDungDTO();
            NguoiDungBLL bll = new NguoiDungBLL();
            nd = bll.layNguoiDung(txtTenDangNhap.Text, txtMatKhau.Text);
            if (nd != null)
            {
                frmManHinhChucNang f = new frmManHinhChucNang(nd);
                f.Show(this);
                Hide();
            }
            else
            {
                MessageBox.Show("Sai tên đăng nhập hoặc mật khẩu.", "Không tìm thấy người dùng", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void frmDangNhap_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
