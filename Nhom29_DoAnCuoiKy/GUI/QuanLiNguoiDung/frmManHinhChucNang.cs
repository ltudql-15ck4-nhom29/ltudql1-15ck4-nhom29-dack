﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;

namespace GUI
{
    public partial class frmManHinhChucNang : Form
    {
        NguoiDungDTO nguoiDung = null;

        public frmManHinhChucNang(NguoiDungDTO nd)
        {
            InitializeComponent();
            nguoiDung = nd;
            switch(nguoiDung.CapBac)
            {
                case 1:
                    grbQuanLiDG.Enabled = true;
                    grbQuanLiTL.Enabled = true;
                    grbQuanLiMT.Enabled = true;
                    grbQuanLiND.Enabled = true;
                    break;
                case 2:
                    grbQuanLiDG.Enabled = true;
                    grbQuanLiTL.Enabled = true;
                    grbQuanLiMT.Enabled = true;
                    break;
                case 3:
                    grbQuanLiND.Enabled = true;
                    break;
            }
        }

        private void refreshForm(object sender, EventArgs e)
        {
            Refresh();
        }

        private void btnDangXuat_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmManHinhChucNang_FormClosed(object sender, FormClosedEventArgs e)
        {
            Owner.Show();
        }

        private void btnDangKyDG_Click(object sender, EventArgs e)
        {
            frmDangKyDG f = new frmDangKyDG();
            f.Show(this);
            Hide();
        }

        private void btnTimKiemDG_Click(object sender, EventArgs e)
        {
            frmTimKiemDG f = new frmTimKiemDG();
            f.Show(this);
            Hide();
        }

        private void btnTiepNhanTL_Click(object sender, EventArgs e)
        {
            frmTiepNhanTL f = new frmTiepNhanTL(null);
            f.ShowDialog();
        }

        private void btnTimKiemTL_Click(object sender, EventArgs e)
        {
            frmTimKiemTL f = new frmTimKiemTL();
            f.Show(this);
            Hide();
        }

        private void btnLapPM_Click(object sender, EventArgs e)
        {
            frmLapPM f = new frmLapPM();
            f.Show(this);
            Hide();
        }

        private void btnTimKiemPM_Click(object sender, EventArgs e)
        {
            frmTimKiemPM f = new frmTimKiemPM();
            f.Show(this);
            Hide();
        }

        private void btnLapPT_Click(object sender, EventArgs e)
        {
            frmLapPT f = new frmLapPT();
            f.Show(this);
            Hide();
        }

        private void btnTimKiemPT_Click(object sender, EventArgs e)
        {
            frmTimKiemPT f = new frmTimKiemPT();
            f.Show(this);
            Hide();
        }

        private void btnQuanLiND_Click(object sender, EventArgs e)
        {
            frmQuanLiND f = new frmQuanLiND();
            f.Show(this);
            Hide();
        }

        private void btnBaoCaoDocGia_Click(object sender, EventArgs e)
        {
            frmBaoCaoDocGia f = new frmBaoCaoDocGia();
            f.FormClosed += refreshForm;
            f.Show(this);
            Hide();
        }

        private void btnBaoCaoLoaiDocGia_Click(object sender, EventArgs e)
        {
            frmBaoCaoLoaiDocGia f = new frmBaoCaoLoaiDocGia();
            f.FormClosed += refreshForm;
            f.Show(this);
            Hide();
        }

        private void btnBaoCaoTaiLieu_Click(object sender, EventArgs e)
        {
            frmBaoCaoTaiLieu f = new frmBaoCaoTaiLieu();
            f.FormClosed += refreshForm;
            f.Show(this);
            Hide();
        }

        private void btnBaoCaoLoaiTaiLieu_Click(object sender, EventArgs e)
        {
            frmBaoCaoLoaiTaiLieu f = new frmBaoCaoLoaiTaiLieu();
            f.FormClosed += refreshForm;
            f.Show(this);
            Hide();
        }
    }
}
