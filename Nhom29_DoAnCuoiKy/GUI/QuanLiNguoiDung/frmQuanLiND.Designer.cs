﻿namespace GUI
{
    partial class frmQuanLiND
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnThemND = new System.Windows.Forms.Button();
            this.btnCapNhatND = new System.Windows.Forms.Button();
            this.lblMaND = new System.Windows.Forms.Label();
            this.txtMatKhauNDMoi = new System.Windows.Forms.TextBox();
            this.txtTenND = new System.Windows.Forms.TextBox();
            this.cboCapBacND = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnXoaND = new System.Windows.Forms.Button();
            this.lvDSND = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnQuayVe = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnThemND);
            this.groupBox1.Controls.Add(this.btnCapNhatND);
            this.groupBox1.Controls.Add(this.lblMaND);
            this.groupBox1.Controls.Add(this.txtMatKhauNDMoi);
            this.groupBox1.Controls.Add(this.txtTenND);
            this.groupBox1.Controls.Add(this.cboCapBacND);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(515, 200);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin người dùng";
            // 
            // btnThemND
            // 
            this.btnThemND.Location = new System.Drawing.Point(103, 141);
            this.btnThemND.Name = "btnThemND";
            this.btnThemND.Size = new System.Drawing.Size(123, 30);
            this.btnThemND.TabIndex = 15;
            this.btnThemND.Text = "Thêm mới";
            this.btnThemND.UseVisualStyleBackColor = true;
            this.btnThemND.Click += new System.EventHandler(this.btnThemND_Click);
            // 
            // btnCapNhatND
            // 
            this.btnCapNhatND.Location = new System.Drawing.Point(278, 141);
            this.btnCapNhatND.Name = "btnCapNhatND";
            this.btnCapNhatND.Size = new System.Drawing.Size(123, 30);
            this.btnCapNhatND.TabIndex = 14;
            this.btnCapNhatND.Text = "Cập nhật";
            this.btnCapNhatND.UseVisualStyleBackColor = true;
            this.btnCapNhatND.Click += new System.EventHandler(this.btnCapNhatND_Click);
            // 
            // lblMaND
            // 
            this.lblMaND.AutoSize = true;
            this.lblMaND.Location = new System.Drawing.Point(209, 29);
            this.lblMaND.Name = "lblMaND";
            this.lblMaND.Size = new System.Drawing.Size(75, 13);
            this.lblMaND.TabIndex = 13;
            this.lblMaND.Text = "maNguoiDung";
            // 
            // txtMatKhauNDMoi
            // 
            this.txtMatKhauNDMoi.Location = new System.Drawing.Point(212, 78);
            this.txtMatKhauNDMoi.Name = "txtMatKhauNDMoi";
            this.txtMatKhauNDMoi.PasswordChar = '*';
            this.txtMatKhauNDMoi.Size = new System.Drawing.Size(189, 20);
            this.txtMatKhauNDMoi.TabIndex = 12;
            // 
            // txtTenND
            // 
            this.txtTenND.Location = new System.Drawing.Point(212, 52);
            this.txtTenND.Name = "txtTenND";
            this.txtTenND.Size = new System.Drawing.Size(189, 20);
            this.txtTenND.TabIndex = 11;
            // 
            // cboCapBacND
            // 
            this.cboCapBacND.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCapBacND.FormattingEnabled = true;
            this.cboCapBacND.Location = new System.Drawing.Point(212, 104);
            this.cboCapBacND.Name = "cboCapBacND";
            this.cboCapBacND.Size = new System.Drawing.Size(189, 21);
            this.cboCapBacND.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(100, 107);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Cấp bậc:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(100, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Mật khẩu mới:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(100, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tên người dùng:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(100, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mã người dùng:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnXoaND);
            this.groupBox2.Controls.Add(this.lvDSND);
            this.groupBox2.Location = new System.Drawing.Point(12, 218);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(515, 277);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Danh sách người dùng";
            // 
            // btnXoaND
            // 
            this.btnXoaND.Location = new System.Drawing.Point(389, 241);
            this.btnXoaND.Name = "btnXoaND";
            this.btnXoaND.Size = new System.Drawing.Size(120, 27);
            this.btnXoaND.TabIndex = 1;
            this.btnXoaND.Text = "Xóa người dùng";
            this.btnXoaND.UseVisualStyleBackColor = true;
            this.btnXoaND.Click += new System.EventHandler(this.btnXoaND_Click);
            // 
            // lvDSND
            // 
            this.lvDSND.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader4});
            this.lvDSND.FullRowSelect = true;
            this.lvDSND.Location = new System.Drawing.Point(6, 19);
            this.lvDSND.MultiSelect = false;
            this.lvDSND.Name = "lvDSND";
            this.lvDSND.Size = new System.Drawing.Size(503, 216);
            this.lvDSND.TabIndex = 0;
            this.lvDSND.UseCompatibleStateImageBehavior = false;
            this.lvDSND.View = System.Windows.Forms.View.Details;
            this.lvDSND.SelectedIndexChanged += new System.EventHandler(this.lvDSND_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Mã người dùng";
            this.columnHeader1.Width = 90;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Tên người dùng";
            this.columnHeader2.Width = 200;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Cấp bậc";
            this.columnHeader4.Width = 180;
            // 
            // btnQuayVe
            // 
            this.btnQuayVe.Location = new System.Drawing.Point(12, 501);
            this.btnQuayVe.Name = "btnQuayVe";
            this.btnQuayVe.Size = new System.Drawing.Size(121, 41);
            this.btnQuayVe.TabIndex = 2;
            this.btnQuayVe.Text = "Quay về";
            this.btnQuayVe.UseVisualStyleBackColor = true;
            this.btnQuayVe.Click += new System.EventHandler(this.btnQuayVe_Click);
            // 
            // frmQuanLiND
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(539, 550);
            this.Controls.Add(this.btnQuayVe);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmQuanLiND";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quản lí người dùng";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmQuanLiND_FormClosed);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnThemND;
        private System.Windows.Forms.Button btnCapNhatND;
        private System.Windows.Forms.Label lblMaND;
        private System.Windows.Forms.TextBox txtMatKhauNDMoi;
        private System.Windows.Forms.TextBox txtTenND;
        private System.Windows.Forms.ComboBox cboCapBacND;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnXoaND;
        private System.Windows.Forms.ListView lvDSND;
        private System.Windows.Forms.Button btnQuayVe;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader4;
    }
}