﻿namespace GUI
{
    partial class frmChiTietTL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnChinhSua = new System.Windows.Forms.Button();
            this.btnQuayVe = new System.Windows.Forms.Button();
            this.btnLuu = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cboLaSachHiem = new System.Windows.Forms.ComboBox();
            this.cboChoGVCB = new System.Windows.Forms.ComboBox();
            this.lblSoTLCoTheMuon = new System.Windows.Forms.Label();
            this.lblSoTLDangMuon = new System.Windows.Forms.Label();
            this.lblSoBanSao = new System.Windows.Forms.Label();
            this.cboLoaiTL = new System.Windows.Forms.ComboBox();
            this.lblMaTL = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTenTL = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lvDanhSachBanSao = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnChinhSua
            // 
            this.btnChinhSua.Location = new System.Drawing.Point(253, 514);
            this.btnChinhSua.Name = "btnChinhSua";
            this.btnChinhSua.Size = new System.Drawing.Size(133, 37);
            this.btnChinhSua.TabIndex = 54;
            this.btnChinhSua.Text = "Chỉnh sửa";
            this.btnChinhSua.UseVisualStyleBackColor = true;
            this.btnChinhSua.Click += new System.EventHandler(this.btnChinhSua_Click);
            // 
            // btnQuayVe
            // 
            this.btnQuayVe.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnQuayVe.Location = new System.Drawing.Point(15, 514);
            this.btnQuayVe.Name = "btnQuayVe";
            this.btnQuayVe.Size = new System.Drawing.Size(133, 37);
            this.btnQuayVe.TabIndex = 52;
            this.btnQuayVe.Text = "Quay về";
            this.btnQuayVe.UseVisualStyleBackColor = true;
            this.btnQuayVe.Click += new System.EventHandler(this.btnQuayVe_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Enabled = false;
            this.btnLuu.Location = new System.Drawing.Point(392, 514);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(133, 37);
            this.btnLuu.TabIndex = 51;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.UseVisualStyleBackColor = true;
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cboLaSachHiem);
            this.groupBox1.Controls.Add(this.cboChoGVCB);
            this.groupBox1.Controls.Add(this.lblSoTLCoTheMuon);
            this.groupBox1.Controls.Add(this.lblSoTLDangMuon);
            this.groupBox1.Controls.Add(this.lblSoBanSao);
            this.groupBox1.Controls.Add(this.cboLoaiTL);
            this.groupBox1.Controls.Add(this.lblMaTL);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtTenTL);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(11, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(514, 269);
            this.groupBox1.TabIndex = 60;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin tài liệu";
            // 
            // cboLaSachHiem
            // 
            this.cboLaSachHiem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboLaSachHiem.FormattingEnabled = true;
            this.cboLaSachHiem.Items.AddRange(new object[] {
            "không",
            "có"});
            this.cboLaSachHiem.Location = new System.Drawing.Point(202, 147);
            this.cboLaSachHiem.Name = "cboLaSachHiem";
            this.cboLaSachHiem.Size = new System.Drawing.Size(265, 21);
            this.cboLaSachHiem.TabIndex = 77;
            // 
            // cboChoGVCB
            // 
            this.cboChoGVCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboChoGVCB.FormattingEnabled = true;
            this.cboChoGVCB.Items.AddRange(new object[] {
            "không",
            "có"});
            this.cboChoGVCB.Location = new System.Drawing.Point(202, 115);
            this.cboChoGVCB.Name = "cboChoGVCB";
            this.cboChoGVCB.Size = new System.Drawing.Size(265, 21);
            this.cboChoGVCB.TabIndex = 76;
            // 
            // lblSoTLCoTheMuon
            // 
            this.lblSoTLCoTheMuon.AutoSize = true;
            this.lblSoTLCoTheMuon.Location = new System.Drawing.Point(199, 228);
            this.lblSoTLCoTheMuon.Name = "lblSoTLCoTheMuon";
            this.lblSoTLCoTheMuon.Size = new System.Drawing.Size(77, 13);
            this.lblSoTLCoTheMuon.TabIndex = 75;
            this.lblSoTLCoTheMuon.Text = "soCoTheMuon";
            // 
            // lblSoTLDangMuon
            // 
            this.lblSoTLDangMuon.AutoSize = true;
            this.lblSoTLDangMuon.Location = new System.Drawing.Point(199, 202);
            this.lblSoTLDangMuon.Name = "lblSoTLDangMuon";
            this.lblSoTLDangMuon.Size = new System.Drawing.Size(97, 13);
            this.lblSoTLDangMuon.TabIndex = 74;
            this.lblSoTLDangMuon.Text = "soDangDuocMuon";
            // 
            // lblSoBanSao
            // 
            this.lblSoBanSao.AutoSize = true;
            this.lblSoBanSao.Location = new System.Drawing.Point(199, 176);
            this.lblSoBanSao.Name = "lblSoBanSao";
            this.lblSoBanSao.Size = new System.Drawing.Size(56, 13);
            this.lblSoBanSao.TabIndex = 73;
            this.lblSoBanSao.Text = "soBanSao";
            // 
            // cboLoaiTL
            // 
            this.cboLoaiTL.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboLoaiTL.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboLoaiTL.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboLoaiTL.FormattingEnabled = true;
            this.cboLoaiTL.Location = new System.Drawing.Point(202, 85);
            this.cboLoaiTL.Name = "cboLoaiTL";
            this.cboLoaiTL.Size = new System.Drawing.Size(265, 21);
            this.cboLoaiTL.TabIndex = 72;
            // 
            // lblMaTL
            // 
            this.lblMaTL.AutoSize = true;
            this.lblMaTL.Location = new System.Drawing.Point(199, 36);
            this.lblMaTL.Name = "lblMaTL";
            this.lblMaTL.Size = new System.Drawing.Size(56, 13);
            this.lblMaTL.TabIndex = 71;
            this.lblMaTL.Text = "maTaiLieu";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(54, 228);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 13);
            this.label8.TabIndex = 70;
            this.label8.Text = "Có thể mượn:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(54, 202);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(93, 13);
            this.label9.TabIndex = 69;
            this.label9.Text = "Đang được mượn:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(54, 176);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 13);
            this.label2.TabIndex = 67;
            this.label2.Text = "Số lượng bản sao:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(54, 150);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 65;
            this.label5.Text = "Là sách hiếm:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(54, 115);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 26);
            this.label6.TabIndex = 64;
            this.label6.Text = "Chỉ dành cho giảng\r\nviên/cán bộ trường:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(54, 88);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 63;
            this.label4.Text = "Loại tài liệu:";
            // 
            // txtTenTL
            // 
            this.txtTenTL.Location = new System.Drawing.Point(202, 59);
            this.txtTenTL.Name = "txtTenTL";
            this.txtTenTL.Size = new System.Drawing.Size(265, 20);
            this.txtTenTL.TabIndex = 62;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(54, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 61;
            this.label3.Text = "Tên tài liệu:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(54, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 60;
            this.label1.Text = "Mã tài liệu:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lvDanhSachBanSao);
            this.groupBox2.Location = new System.Drawing.Point(12, 287);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(513, 203);
            this.groupBox2.TabIndex = 61;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Danh sách bản sao";
            // 
            // lvDanhSachBanSao
            // 
            this.lvDanhSachBanSao.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.lvDanhSachBanSao.FullRowSelect = true;
            this.lvDanhSachBanSao.Location = new System.Drawing.Point(6, 19);
            this.lvDanhSachBanSao.MultiSelect = false;
            this.lvDanhSachBanSao.Name = "lvDanhSachBanSao";
            this.lvDanhSachBanSao.Size = new System.Drawing.Size(501, 178);
            this.lvDanhSachBanSao.TabIndex = 0;
            this.lvDanhSachBanSao.UseCompatibleStateImageBehavior = false;
            this.lvDanhSachBanSao.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Mã bản sao";
            this.columnHeader1.Width = 70;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Mã độc giả đang mượn";
            this.columnHeader2.Width = 130;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Tên độc giả đang mượn";
            this.columnHeader3.Width = 270;
            // 
            // frmChiTietTL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(537, 563);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnChinhSua);
            this.Controls.Add(this.btnQuayVe);
            this.Controls.Add(this.btnLuu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmChiTietTL";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chi tiết tài liệu";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnChinhSua;
        private System.Windows.Forms.Button btnQuayVe;
        private System.Windows.Forms.Button btnLuu;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblSoTLCoTheMuon;
        private System.Windows.Forms.Label lblSoTLDangMuon;
        private System.Windows.Forms.Label lblSoBanSao;
        private System.Windows.Forms.ComboBox cboLoaiTL;
        private System.Windows.Forms.Label lblMaTL;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTenTL;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListView lvDanhSachBanSao;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ComboBox cboLaSachHiem;
        private System.Windows.Forms.ComboBox cboChoGVCB;
    }
}