﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BLL;

namespace GUI
{
    public partial class frmTiepNhanTL : Form
    {
        List<TaiLieuDTO> lstTaiLieu = null;
        List<LoaiTaiLieuDTO> lstLoaiTaiLieu = null;

        int soBanSao = 0;

        public frmTiepNhanTL(TaiLieuDTO tl)
        {
            InitializeComponent();

            napDSTaiLieu();
            napDSLoaiTaiLieu();

            hienThiCboTaiLieu(tl);
            hienThiCboLoaiTaiLieu();

            cboDanhChoGVCB.SelectedIndex = 0;
            cboLaSachHiem.SelectedIndex = 0;
        }

        void napDSTaiLieu()
        {
            TaiLieuBLL bll = new TaiLieuBLL();
            lstTaiLieu = bll.layDSTaiLieu();
        }

        void napDSLoaiTaiLieu()
        {
            LoaiTaiLieuBLL bll = new LoaiTaiLieuBLL();
            lstLoaiTaiLieu = bll.layDSLoaiTaiLieu();
        }

        void hienThiCboTaiLieu(TaiLieuDTO tl)
        {
            cboMaTLCoSan.Items.Clear();
            cboTenTLCoSan.Items.Clear();
            cboMaTLCoSan.DisplayMember = "MaTaiLieu";
            cboTenTLCoSan.DisplayMember = "TenTaiLieu";
            for(int i = 0; i < lstTaiLieu.Count; i++)
            {
                cboMaTLCoSan.Items.Add(lstTaiLieu[i]);
                cboTenTLCoSan.Items.Add(lstTaiLieu[i]);
                if(tl != null && lstTaiLieu[i].MaTaiLieu == tl.MaTaiLieu)
                {
                    cboMaTLCoSan.SelectedIndex = cboMaTLCoSan.Items.Count - 1;
                }
            }
        }

        void hienThiCboLoaiTaiLieu()
        {
            cboLoaiTL.Items.Clear();
            for(int i = 0; i < lstLoaiTaiLieu.Count; i++)
            {
                cboLoaiTL.Items.Add(lstLoaiTaiLieu[i]);
            }
        }

        TaiLieuDTO taoTaiLieuMoi()
        {
            TaiLieuDTO tl = new TaiLieuDTO();
            tl.TenTaiLieu = txtTenTL.Text;
            tl.LoaiTaiLieu = ((LoaiTaiLieuDTO)cboLoaiTL.SelectedItem).MaLoai;
            tl.ChoGVCB = Convert.ToBoolean(cboDanhChoGVCB.SelectedIndex);
            tl.LaSachHiem = Convert.ToBoolean(cboLaSachHiem.SelectedIndex);
            soBanSao = Convert.ToInt32(nudSoLuong.Value);
            return tl;
        }

        TaiLieuDTO taoTaiLieuCoSan()
        {
            TaiLieuDTO tl = (TaiLieuDTO)cboMaTLCoSan.SelectedItem;
            soBanSao = Convert.ToInt32(nudSoLuongCoSan.Value);
            return tl;
        }

        private void btnQuayVe_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cboMaTLCoSan_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboTenTLCoSan.SelectedIndex = cboMaTLCoSan.SelectedIndex;
        }

        private void cboTenTLCoSan_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboMaTLCoSan.SelectedIndex = cboTenTLCoSan.SelectedIndex;
        }

        private void btnNhap_Click(object sender, EventArgs e)
        {
            if(txtTenTL.Text == "")
            {
                MessageBox.Show("Tên tài liệu trống!", "Nhập", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if(cboLoaiTL.SelectedIndex == -1)
            {
                MessageBox.Show("Loại tài liệu chưa được chọn!", "Nhập", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if(nudSoLuong.Value == 0)
            {
                MessageBox.Show("Số lượng phải khác 0!", "Nhập", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            TaiLieuDTO tl = taoTaiLieuMoi();
            TaiLieuBLL bll = new TaiLieuBLL();
            if (bll.kiemTraTenTaiLieu(tl))
            {
                MessageBox.Show("Tên tài liệu này đã tồn tại!", "Nhập", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }            
            if(DialogResult.Yes == MessageBox.Show("Bạn có chắc muốn nhập tài liệu này?", "Nhập", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                if(bll.themTaiLieuCoSoBanSao(tl, soBanSao) == soBanSao + 1)
                {
                    MessageBox.Show("Nhập tài liệu thành công!", "Nhập", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    napDSTaiLieu();
                    hienThiCboTaiLieu(null);
                }
                else
                {
                    MessageBox.Show("Nhập tài liệu thất bại!", "Nhập", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnNhapCoSan_Click(object sender, EventArgs e)
        {
            if (cboMaTLCoSan.SelectedIndex == -1 || cboTenTLCoSan.SelectedIndex == -1)
            {
                MessageBox.Show("Tài liệu chưa được chọn!", "Nhập", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (nudSoLuongCoSan.Value == 0)
            {
                MessageBox.Show("Số lượng phải khác 0!", "Nhập", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (DialogResult.Yes == MessageBox.Show("Bạn có chắc muốn nhập tài liệu này?", "Nhập", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                TaiLieuDTO tl = taoTaiLieuCoSan();
                TaiLieuBLL bll = new TaiLieuBLL();
                if (bll.capNhatTaiLieuCoSoBanSao(tl, soBanSao) == soBanSao + 1)
                {
                    MessageBox.Show("Nhập tài liệu thành công!", "Nhập", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Nhập tài liệu thất bại!", "Nhập", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
