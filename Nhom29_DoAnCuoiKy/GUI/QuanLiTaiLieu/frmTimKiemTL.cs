﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BLL;
namespace GUI
{
    public partial class frmTimKiemTL : Form
    {
        List<TaiLieuDTO> lstTaiLieu = null;
        List<LoaiTaiLieuDTO> lstLoaiTaiLieu = null;
        List<TaiLieuDTO> lstHienThi = null;
        
        public frmTimKiemTL()
        {
            InitializeComponent();

            napDSTaiLieu();
            napDSLoaiTaiLieu();

            hienThiCboLoaiLinhVuc();
            hienThiCboTimKiemNCLoaiTL();

            lstHienThi = lstTaiLieu;
            hienThiLvTaiLieu();

            hienThiMacDinh();
        }

        void hienThiMacDinh()
        {
            cboLoaiTL.DisplayMember = "TenLoai";
            cboLoaiLinhVuc.DisplayMember = "TenLoai";
            cboLoaiTL.SelectedIndex = 0;
            cboChoGVCB.SelectedIndex = 0;
            cboSachHiem.SelectedIndex = 0;
            cboDKSoBanSao.SelectedIndex = 0;
            cboDKDangMuon.SelectedIndex = 0;
            cboDKCoTheMuon.SelectedIndex = 0;
        }

        void napDSTaiLieu()
        {
            TaiLieuBLL bll = new TaiLieuBLL();
            lstTaiLieu = bll.layDSTaiLieuDayDu();
        }

        void napDSLoaiTaiLieu()
        {
            LoaiTaiLieuBLL bll = new LoaiTaiLieuBLL();
            lstLoaiTaiLieu = bll.layDSLoaiTaiLieu();
        }

        void hienThiCboLoaiLinhVuc()
        {
            cboLoaiLinhVuc.Items.Clear();
            for(int i = 0; i < lstLoaiTaiLieu.Count; i++)
            {
                cboLoaiLinhVuc.Items.Add(lstLoaiTaiLieu[i]);
            }
        }

        void hienThiCboTimKiemNCLoaiTL()
        {
            cboLoaiTL.Items.Clear();
            cboLoaiTL.Items.Add("(không quan tâm)");
            for (int i = 0; i < lstLoaiTaiLieu.Count; i++)
            {
                cboLoaiTL.Items.Add(lstLoaiTaiLieu[i]);
            }
        }

        void hienThiLvTaiLieu()
        {
            lvDSTL.Items.Clear();
            for(int i = 0; i < lstHienThi.Count; i++)
            {
                TaiLieuDTO tl = lstHienThi[i];
                LoaiTaiLieuDTO ltl = LoaiTaiLieuDTO.layLoaiTaiLieuTheoTaiLieu(tl, lstLoaiTaiLieu);
                ListViewItem item = new ListViewItem(tl.MaTaiLieu.ToString());
                item.SubItems.Add(tl.TenTaiLieu);
                item.SubItems.Add(ltl.TenLoai);
                if(tl.ChoGVCB)
                {
                    item.SubItems.Add("Có");
                }
                else
                {
                    item.SubItems.Add("Không");
                }
                if(tl.LaSachHiem)
                {
                    item.SubItems.Add("Có");
                }
                else
                {
                    item.SubItems.Add("Không");
                }
                item.SubItems.Add(tl.LstBanSao.Count.ToString());
                lvDSTL.Items.Add(item);
            }
        }

        void timKiemMaTL()
        {
            lstHienThi = new List<TaiLieuDTO>();
            int iSearch = int.Parse(txtNoiDungLocCB.Text);
            for(int i = 0; i < lstTaiLieu.Count; i++)
            {
                if(lstTaiLieu[i].MaTaiLieu == iSearch)
                {
                    lstHienThi.Add(lstTaiLieu[i]);
                }
            }
            hienThiLvTaiLieu();
        }

        void timKiemTenTL()
        {
            lstHienThi = new List<TaiLieuDTO>();
            for (int i = 0; i < lstTaiLieu.Count; i++)
            {
                if (lstTaiLieu[i].TenTaiLieu.Contains(txtNoiDungLocCB.Text))
                {
                    lstHienThi.Add(lstTaiLieu[i]);
                }
            }
            hienThiLvTaiLieu();
        }

        void timKiemLoaiTL()
        {
            lstHienThi = new List<TaiLieuDTO>();
            for (int i = 0; i < lstTaiLieu.Count; i++)
            {
                LoaiTaiLieuDTO ltl = LoaiTaiLieuDTO.layLoaiTaiLieuTheoTaiLieu(lstTaiLieu[i], lstLoaiTaiLieu);
                if (ltl.TenLoai.Contains(txtNoiDungLocCB.Text))
                {
                    lstHienThi.Add(lstTaiLieu[i]);
                }
            }
            hienThiLvTaiLieu();
        }

        void timKiemSoBS()
        {
            lstHienThi = new List<TaiLieuDTO>();
            int iSearch = int.Parse(txtNoiDungLocCB.Text);
            for (int i = 0; i < lstTaiLieu.Count; i++)
            {
                if (lstTaiLieu[i].LstBanSao.Count == iSearch)
                {
                    lstHienThi.Add(lstTaiLieu[i]);
                }
            }
            hienThiLvTaiLieu();
        }

        bool kiemTraTheoDieuKien(int so1, int so2, string dieuKien)
        {
            switch(dieuKien)
            {
                case ">": return (so1 > so2);
                case "<": return (so1 < so2);
                case "bằng": return (so1 == so2);
                case "khác": return (so1 != so2);
            }
            return false;
        }

        bool kiemTraCoBanSaoDangDuocMuon(TaiLieuDTO tl)
        {
            List<BanSaoDTO> lstBanSao = tl.LstBanSao;
            for(int i = 0; i < lstBanSao.Count; i++)
            {
                BanSaoDTO bs = lstBanSao[i];
                if(bs.MaDocGiaDangMuon != -1)
                {
                    return true;
                }
            }
            return false;
        }

        private void capNhatForm(object sender, EventArgs e)
        {
            napDSTaiLieu();

            lstHienThi = lstTaiLieu;
            hienThiLvTaiLieu();
        }

        private void frmTimKiemTL_FormClosed(object sender, FormClosedEventArgs e)
        {
            Owner.Show();
        }

        private void btnQuayVe_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnXemChiTiet_Click(object sender, EventArgs e)
        {
            if(lvDSTL.SelectedItems.Count > 0)
            {
                TaiLieuDTO tl = lstHienThi[lvDSTL.SelectedIndices[0]];
                frmChiTietTL f = new frmChiTietTL(tl);
                f.ShowDialog();
                capNhatForm(this, new EventArgs());
            }
            else
            {
                MessageBox.Show("Không có tài liệu nào được chọn!", "Xem chi tiết thông tin tài liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void btnYeuCauTL_Click(object sender, EventArgs e)
        {
            if (lvDSTL.SelectedItems.Count == 0)
            {
                //MessageBox.Show("Không có tài liệu nào được chọn!", "Yêu cầu tài liệu mới", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //return;
                frmTiepNhanTL f = new frmTiepNhanTL(null);
                f.ShowDialog();
                capNhatForm(this, new EventArgs());
            }
            else
            {
                TaiLieuDTO tl = lstHienThi[lvDSTL.SelectedIndices[0]];
                frmTiepNhanTL f = new frmTiepNhanTL(tl);
                f.ShowDialog();
                capNhatForm(this, new EventArgs());

            }
        }

        private void btnLapPhieuMuon_Click(object sender, EventArgs e)
        {
            frmLapPM f = new frmLapPM();
            f.FormClosed += capNhatForm;
            f.Show(this);            
            Hide();
        }

        private void btnXemHet1_Click(object sender, EventArgs e)
        {
            lstHienThi = lstTaiLieu;
            hienThiLvTaiLieu();
        }

        private void btnXemHet2_Click(object sender, EventArgs e)
        {
            lstHienThi = lstTaiLieu;
            hienThiLvTaiLieu();
        }

        private void btnXemHet3_Click(object sender, EventArgs e)
        {
            lstHienThi = lstTaiLieu;
            hienThiLvTaiLieu();
        }

        private void btnXemTheoTheLoai_Click(object sender, EventArgs e)
        {
            if(cboLoaiLinhVuc.SelectedIndex == -1)
            {
                MessageBox.Show("Chưa có loại/lĩnh vực nào được chọn!", "Xem theo loại/lĩnh vực", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            lstHienThi = new List<TaiLieuDTO>();
            for(int i = 0; i < lstTaiLieu.Count; i++)
            {
                if(lstTaiLieu[i].LoaiTaiLieu == ((LoaiTaiLieuDTO)cboLoaiLinhVuc.SelectedItem).MaLoai)
                {
                    lstHienThi.Add(lstTaiLieu[i]);
                }
            }
            hienThiLvTaiLieu();
        }

        private void btnTimKiemCB_Click(object sender, EventArgs e)
        {
            if (cboLoaiBoLocCB.SelectedIndex == -1)
            {
                MessageBox.Show("Loại bộ lọc chưa được chọn!", "Tìm kiếm", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            switch (cboLoaiBoLocCB.SelectedIndex)
            {
                case 0:
                    if (!RegularExpression.laSo(txtNoiDungLocCB.Text))
                    {
                        MessageBox.Show("Nội dung tìm kiếm phải là số!", "Tìm kiếm", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    timKiemMaTL();
                    break;
                case 1:
                    timKiemTenTL();
                    break;
                //case 2:
                    //timKiemLoaiTL();
                    //break;
                case 2:
                    if (!RegularExpression.laSo(txtNoiDungLocCB.Text))
                    {
                        MessageBox.Show("Nội dung tìm kiếm phải là số!", "Tìm kiếm", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    timKiemSoBS();
                    break;
            }
        }

        private void btnTimKiemNC_Click(object sender, EventArgs e)
        {
            lstHienThi = lstTaiLieu;

            if(txtTenTL.Text != "")
            {
                List<TaiLieuDTO> lstKiemTra = lstHienThi;
                lstHienThi = new List<TaiLieuDTO>();
                for(int i = 0; i < lstKiemTra.Count; i++)
                {
                    if(lstKiemTra[i].TenTaiLieu.Contains(txtTenTL.Text))
                    {
                        lstHienThi.Add(lstKiemTra[i]);
                    }
                }
            }

            if(cboLoaiTL.SelectedIndex > 0)
            {
                List<TaiLieuDTO> lstKiemTra = lstHienThi;
                lstHienThi = new List<TaiLieuDTO>();
                for (int i = 0; i < lstKiemTra.Count; i++)
                {
                    LoaiTaiLieuDTO ltl = LoaiTaiLieuDTO.layLoaiTaiLieuTheoTaiLieu(lstKiemTra[i], lstLoaiTaiLieu);
                    if (ltl.MaLoai == ((LoaiTaiLieuDTO)cboLoaiTL.SelectedItem).MaLoai)
                    {
                        lstHienThi.Add(lstKiemTra[i]);
                    }
                }
            }

            if (cboChoGVCB.SelectedIndex > 0)
            {
                List<TaiLieuDTO> lstKiemTra = lstHienThi;
                lstHienThi = new List<TaiLieuDTO>();
                for (int i = 0; i < lstKiemTra.Count; i++)
                {
                    if ((cboChoGVCB.SelectedIndex == 1 && lstKiemTra[i].ChoGVCB) || (cboChoGVCB.SelectedIndex == 2 && !lstKiemTra[i].ChoGVCB))
                    {
                        lstHienThi.Add(lstKiemTra[i]);
                    }
                }
            }

            if (cboSachHiem.SelectedIndex > 0)
            {
                List<TaiLieuDTO> lstKiemTra = lstHienThi;
                lstHienThi = new List<TaiLieuDTO>();
                for (int i = 0; i < lstKiemTra.Count; i++)
                {
                    if ((cboSachHiem.SelectedIndex == 1 && lstKiemTra[i].LaSachHiem) || (cboSachHiem.SelectedIndex == 2 && !lstKiemTra[i].LaSachHiem))
                    {
                        lstHienThi.Add(lstKiemTra[i]);
                    }
                }
            }

            if(cboDKSoBanSao.SelectedIndex > 0)
            {
                List<TaiLieuDTO> lstKiemTra = lstHienThi;
                lstHienThi = new List<TaiLieuDTO>();
                for (int i = 0; i < lstKiemTra.Count; i++)
                {
                    if(kiemTraTheoDieuKien(lstKiemTra[i].LstBanSao.Count(), Convert.ToInt32(nudSoBanSao.Value), cboDKSoBanSao.SelectedItem.ToString()))
                    {
                        lstHienThi.Add(lstKiemTra[i]);
                    }
                }
            }

            if (cboDKDangMuon.SelectedIndex > 0)
            {
                List<TaiLieuDTO> lstKiemTra = lstHienThi;
                lstHienThi = new List<TaiLieuDTO>();
                for (int i = 0; i < lstKiemTra.Count; i++)
                {
                    if (kiemTraTheoDieuKien(lstKiemTra[i].laySoBanSaoDangDuocMuon(), Convert.ToInt32(nudDangMuon.Value), cboDKDangMuon.SelectedItem.ToString()))
                    {
                        lstHienThi.Add(lstKiemTra[i]);
                    }
                }
            }

            if (cboDKCoTheMuon.SelectedIndex > 0)
            {
                List<TaiLieuDTO> lstKiemTra = lstHienThi;
                lstHienThi = new List<TaiLieuDTO>();
                for (int i = 0; i < lstKiemTra.Count; i++)
                {
                    if (kiemTraTheoDieuKien(lstKiemTra[i].laySoBanSaoCoTheMuon(), Convert.ToInt32(nudCoTheMuon.Value), cboDKCoTheMuon.SelectedItem.ToString()))
                    {
                        lstHienThi.Add(lstKiemTra[i]);
                    }
                }
            }

            hienThiLvTaiLieu();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if(lvDSTL.SelectedItems.Count == 0)
            {
                MessageBox.Show("Không có tài liệu nào được chọn!", "Xóa tài liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            TaiLieuDTO tl = lstHienThi[lvDSTL.SelectedIndices[0]];
            if (kiemTraCoBanSaoDangDuocMuon(tl))
            {
                MessageBox.Show("Không thể xóa tài liệu này vì tài liệu này có ít nhất một bản sao đang được mượn!", "Xóa tài liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if(DialogResult.Yes == MessageBox.Show("Bạn có chắc muốn xóa tài liệu này?", "Xóa tài liệu", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                
                TaiLieuBLL bll = new TaiLieuBLL();
                if(bll.xoaTaiLieu(tl) == 1)
                {
                    MessageBox.Show("Xóa tài liệu thành công!", "Xóa tài liệu", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lstHienThi.Remove(tl);
                    lstTaiLieu.Remove(tl);
                    hienThiLvTaiLieu();
                }
                else
                {
                    MessageBox.Show("Xóa tài liệu thất bại!", "Xóa tài liệu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnThemVaoPhieuMuon_Click(object sender, EventArgs e)
        {
            if (lvDSTL.SelectedItems.Count == 0)
            {
                MessageBox.Show("Không có tài liệu nào được chọn!", "Thêm vào phiếu mượn", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            TaiLieuDTO tl = lstHienThi[lvDSTL.SelectedIndices[0]];
            if(tl.LaSachHiem)
            {
                MessageBox.Show("Tài liệu là sách hiếm nên không thể mượn!", "Thêm vào phiếu mượn", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if(tl.laySoBanSaoCoTheMuon() == 0)
            {
                MessageBox.Show("Tất cả các bản sao của tài liệu này đã được mượn!", "Thêm vào phiếu mượn", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            frmThemVaoPM f = new frmThemVaoPM(tl);
            f.ShowDialog();
            capNhatForm(this, new EventArgs());
        }
    }
}
