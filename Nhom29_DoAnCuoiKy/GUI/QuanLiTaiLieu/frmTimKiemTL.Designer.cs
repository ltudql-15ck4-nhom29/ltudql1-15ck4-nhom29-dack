﻿namespace GUI
{
    partial class frmTimKiemTL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lvDSTL = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnXemChiTiet = new System.Windows.Forms.Button();
            this.btnYeuCauTL = new System.Windows.Forms.Button();
            this.btnLapPhieuMuon = new System.Windows.Forms.Button();
            this.btnThemVaoPhieuMuon = new System.Windows.Forms.Button();
            this.btnQuayVe = new System.Windows.Forms.Button();
            this.btnXoa = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnXemHet1 = new System.Windows.Forms.Button();
            this.btnTimKiemCB = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNoiDungLocCB = new System.Windows.Forms.TextBox();
            this.cboLoaiBoLocCB = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnXemHet2 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.nudCoTheMuon = new System.Windows.Forms.NumericUpDown();
            this.cboDKDangMuon = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnTimKiemNC = new System.Windows.Forms.Button();
            this.nudDangMuon = new System.Windows.Forms.NumericUpDown();
            this.cboDKCoTheMuon = new System.Windows.Forms.ComboBox();
            this.nudSoBanSao = new System.Windows.Forms.NumericUpDown();
            this.cboDKSoBanSao = new System.Windows.Forms.ComboBox();
            this.cboSachHiem = new System.Windows.Forms.ComboBox();
            this.cboChoGVCB = new System.Windows.Forms.ComboBox();
            this.cboLoaiTL = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTenTL = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnXemHet3 = new System.Windows.Forms.Button();
            this.btnXemTheoTheLoai = new System.Windows.Forms.Button();
            this.cboLoaiLinhVuc = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCoTheMuon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDangMuon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSoBanSao)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lvDSTL);
            this.groupBox2.Location = new System.Drawing.Point(12, 232);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(637, 286);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Danh sách tài liệu";
            // 
            // lvDSTL
            // 
            this.lvDSTL.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6});
            this.lvDSTL.FullRowSelect = true;
            this.lvDSTL.Location = new System.Drawing.Point(6, 19);
            this.lvDSTL.MultiSelect = false;
            this.lvDSTL.Name = "lvDSTL";
            this.lvDSTL.Size = new System.Drawing.Size(625, 261);
            this.lvDSTL.TabIndex = 0;
            this.lvDSTL.UseCompatibleStateImageBehavior = false;
            this.lvDSTL.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Mã tài liệu";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Tên tài liệu";
            this.columnHeader2.Width = 140;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Loại tài liệu";
            this.columnHeader3.Width = 120;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Dành cho GV/CB";
            this.columnHeader4.Width = 100;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Là sách hiếm";
            this.columnHeader5.Width = 90;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Số bản sao";
            this.columnHeader6.Width = 80;
            // 
            // btnXemChiTiet
            // 
            this.btnXemChiTiet.Location = new System.Drawing.Point(520, 524);
            this.btnXemChiTiet.Name = "btnXemChiTiet";
            this.btnXemChiTiet.Size = new System.Drawing.Size(129, 40);
            this.btnXemChiTiet.TabIndex = 2;
            this.btnXemChiTiet.Text = "Xem chi tiết thông tin\r\ntài liệu";
            this.btnXemChiTiet.UseVisualStyleBackColor = true;
            this.btnXemChiTiet.Click += new System.EventHandler(this.btnXemChiTiet_Click);
            // 
            // btnYeuCauTL
            // 
            this.btnYeuCauTL.Location = new System.Drawing.Point(131, 524);
            this.btnYeuCauTL.Name = "btnYeuCauTL";
            this.btnYeuCauTL.Size = new System.Drawing.Size(116, 40);
            this.btnYeuCauTL.TabIndex = 3;
            this.btnYeuCauTL.Text = "Yêu cầu tài liệu mới";
            this.btnYeuCauTL.UseVisualStyleBackColor = true;
            this.btnYeuCauTL.Click += new System.EventHandler(this.btnYeuCauTL_Click);
            // 
            // btnLapPhieuMuon
            // 
            this.btnLapPhieuMuon.Location = new System.Drawing.Point(253, 524);
            this.btnLapPhieuMuon.Name = "btnLapPhieuMuon";
            this.btnLapPhieuMuon.Size = new System.Drawing.Size(126, 40);
            this.btnLapPhieuMuon.TabIndex = 4;
            this.btnLapPhieuMuon.Text = "Lập phiếu mượn sách";
            this.btnLapPhieuMuon.UseVisualStyleBackColor = true;
            this.btnLapPhieuMuon.Click += new System.EventHandler(this.btnLapPhieuMuon_Click);
            // 
            // btnThemVaoPhieuMuon
            // 
            this.btnThemVaoPhieuMuon.Location = new System.Drawing.Point(385, 524);
            this.btnThemVaoPhieuMuon.Name = "btnThemVaoPhieuMuon";
            this.btnThemVaoPhieuMuon.Size = new System.Drawing.Size(129, 40);
            this.btnThemVaoPhieuMuon.TabIndex = 5;
            this.btnThemVaoPhieuMuon.Text = "Thêm vào phiếu\r\nmượn sách";
            this.btnThemVaoPhieuMuon.UseVisualStyleBackColor = true;
            this.btnThemVaoPhieuMuon.Click += new System.EventHandler(this.btnThemVaoPhieuMuon_Click);
            // 
            // btnQuayVe
            // 
            this.btnQuayVe.Location = new System.Drawing.Point(12, 570);
            this.btnQuayVe.Name = "btnQuayVe";
            this.btnQuayVe.Size = new System.Drawing.Size(183, 40);
            this.btnQuayVe.TabIndex = 6;
            this.btnQuayVe.Text = "Quay về";
            this.btnQuayVe.UseVisualStyleBackColor = true;
            this.btnQuayVe.Click += new System.EventHandler(this.btnQuayVe_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Location = new System.Drawing.Point(12, 524);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(113, 40);
            this.btnXoa.TabIndex = 7;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.UseVisualStyleBackColor = true;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(637, 214);
            this.tabControl1.TabIndex = 8;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnXemHet1);
            this.tabPage1.Controls.Add(this.btnTimKiemCB);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.txtNoiDungLocCB);
            this.tabPage1.Controls.Add(this.cboLoaiBoLocCB);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(629, 188);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Tìm kiếm cơ bản";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnXemHet1
            // 
            this.btnXemHet1.Location = new System.Drawing.Point(215, 71);
            this.btnXemHet1.Name = "btnXemHet1";
            this.btnXemHet1.Size = new System.Drawing.Size(100, 31);
            this.btnXemHet1.TabIndex = 10;
            this.btnXemHet1.Text = "Xem hết";
            this.btnXemHet1.UseVisualStyleBackColor = true;
            this.btnXemHet1.Click += new System.EventHandler(this.btnXemHet1_Click);
            // 
            // btnTimKiemCB
            // 
            this.btnTimKiemCB.Location = new System.Drawing.Point(325, 71);
            this.btnTimKiemCB.Name = "btnTimKiemCB";
            this.btnTimKiemCB.Size = new System.Drawing.Size(100, 31);
            this.btnTimKiemCB.TabIndex = 9;
            this.btnTimKiemCB.Text = "Tìm kiếm cơ bản";
            this.btnTimKiemCB.UseVisualStyleBackColor = true;
            this.btnTimKiemCB.Click += new System.EventHandler(this.btnTimKiemCB_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(156, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Nội dung:";
            // 
            // txtNoiDungLocCB
            // 
            this.txtNoiDungLocCB.Location = new System.Drawing.Point(215, 45);
            this.txtNoiDungLocCB.Name = "txtNoiDungLocCB";
            this.txtNoiDungLocCB.Size = new System.Drawing.Size(210, 20);
            this.txtNoiDungLocCB.TabIndex = 7;
            // 
            // cboLoaiBoLocCB
            // 
            this.cboLoaiBoLocCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboLoaiBoLocCB.FormattingEnabled = true;
            this.cboLoaiBoLocCB.Items.AddRange(new object[] {
            "Mã tài liệu",
            "Tên tài liệu",
            "Số bản sao"});
            this.cboLoaiBoLocCB.Location = new System.Drawing.Point(215, 18);
            this.cboLoaiBoLocCB.Name = "cboLoaiBoLocCB";
            this.cboLoaiBoLocCB.Size = new System.Drawing.Size(210, 21);
            this.cboLoaiBoLocCB.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(133, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Tìm kiếm theo:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnXemHet2);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.nudCoTheMuon);
            this.tabPage2.Controls.Add(this.cboDKDangMuon);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.btnTimKiemNC);
            this.tabPage2.Controls.Add(this.nudDangMuon);
            this.tabPage2.Controls.Add(this.cboDKCoTheMuon);
            this.tabPage2.Controls.Add(this.nudSoBanSao);
            this.tabPage2.Controls.Add(this.cboDKSoBanSao);
            this.tabPage2.Controls.Add(this.cboSachHiem);
            this.tabPage2.Controls.Add(this.cboChoGVCB);
            this.tabPage2.Controls.Add(this.cboLoaiTL);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.txtTenTL);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(629, 188);
            this.tabPage2.TabIndex = 2;
            this.tabPage2.Text = "Tìm kiếm nâng cao";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnXemHet2
            // 
            this.btnXemHet2.Location = new System.Drawing.Point(196, 137);
            this.btnXemHet2.Name = "btnXemHet2";
            this.btnXemHet2.Size = new System.Drawing.Size(115, 36);
            this.btnXemHet2.TabIndex = 24;
            this.btnXemHet2.Text = "Xem hết";
            this.btnXemHet2.UseVisualStyleBackColor = true;
            this.btnXemHet2.Click += new System.EventHandler(this.btnXemHet2_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(314, 69);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(70, 13);
            this.label11.TabIndex = 23;
            this.label11.Text = "Có thể mượn:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(314, 43);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(93, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "Đang được mượn:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(314, 18);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "Số bản sao:";
            // 
            // nudCoTheMuon
            // 
            this.nudCoTheMuon.Location = new System.Drawing.Point(473, 67);
            this.nudCoTheMuon.Name = "nudCoTheMuon";
            this.nudCoTheMuon.Size = new System.Drawing.Size(120, 20);
            this.nudCoTheMuon.TabIndex = 20;
            // 
            // cboDKDangMuon
            // 
            this.cboDKDangMuon.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDKDangMuon.FormattingEnabled = true;
            this.cboDKDangMuon.Items.AddRange(new object[] {
            "(trống)",
            ">",
            "<",
            "bằng",
            "khác"});
            this.cboDKDangMuon.Location = new System.Drawing.Point(413, 41);
            this.cboDKDangMuon.Name = "cboDKDangMuon";
            this.cboDKDangMuon.Size = new System.Drawing.Size(54, 21);
            this.cboDKDangMuon.TabIndex = 19;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(25, 105);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Là sách hiếm:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(25, 70);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 26);
            this.label7.TabIndex = 17;
            this.label7.Text = "Chỉ dành cho giảng\r\nviên/cán bộ trường:";
            // 
            // btnTimKiemNC
            // 
            this.btnTimKiemNC.Location = new System.Drawing.Point(317, 137);
            this.btnTimKiemNC.Name = "btnTimKiemNC";
            this.btnTimKiemNC.Size = new System.Drawing.Size(115, 36);
            this.btnTimKiemNC.TabIndex = 16;
            this.btnTimKiemNC.Text = "Tìm kiếm nâng cao";
            this.btnTimKiemNC.UseVisualStyleBackColor = true;
            this.btnTimKiemNC.Click += new System.EventHandler(this.btnTimKiemNC_Click);
            // 
            // nudDangMuon
            // 
            this.nudDangMuon.Location = new System.Drawing.Point(473, 41);
            this.nudDangMuon.Name = "nudDangMuon";
            this.nudDangMuon.Size = new System.Drawing.Size(120, 20);
            this.nudDangMuon.TabIndex = 15;
            // 
            // cboDKCoTheMuon
            // 
            this.cboDKCoTheMuon.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDKCoTheMuon.FormattingEnabled = true;
            this.cboDKCoTheMuon.Items.AddRange(new object[] {
            "(trống)",
            ">",
            "<",
            "bằng",
            "khác"});
            this.cboDKCoTheMuon.Location = new System.Drawing.Point(413, 67);
            this.cboDKCoTheMuon.Name = "cboDKCoTheMuon";
            this.cboDKCoTheMuon.Size = new System.Drawing.Size(54, 21);
            this.cboDKCoTheMuon.TabIndex = 14;
            // 
            // nudSoBanSao
            // 
            this.nudSoBanSao.Location = new System.Drawing.Point(473, 15);
            this.nudSoBanSao.Name = "nudSoBanSao";
            this.nudSoBanSao.Size = new System.Drawing.Size(120, 20);
            this.nudSoBanSao.TabIndex = 13;
            // 
            // cboDKSoBanSao
            // 
            this.cboDKSoBanSao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDKSoBanSao.FormattingEnabled = true;
            this.cboDKSoBanSao.Items.AddRange(new object[] {
            "(trống)",
            ">",
            "<",
            "bằng",
            "khác"});
            this.cboDKSoBanSao.Location = new System.Drawing.Point(413, 15);
            this.cboDKSoBanSao.Name = "cboDKSoBanSao";
            this.cboDKSoBanSao.Size = new System.Drawing.Size(54, 21);
            this.cboDKSoBanSao.TabIndex = 12;
            // 
            // cboSachHiem
            // 
            this.cboSachHiem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSachHiem.FormattingEnabled = true;
            this.cboSachHiem.Items.AddRange(new object[] {
            "(không quan tâm)",
            "có",
            "không"});
            this.cboSachHiem.Location = new System.Drawing.Point(141, 102);
            this.cboSachHiem.Name = "cboSachHiem";
            this.cboSachHiem.Size = new System.Drawing.Size(158, 21);
            this.cboSachHiem.TabIndex = 11;
            // 
            // cboChoGVCB
            // 
            this.cboChoGVCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboChoGVCB.FormattingEnabled = true;
            this.cboChoGVCB.Items.AddRange(new object[] {
            "(không quan tâm)",
            "có",
            "không"});
            this.cboChoGVCB.Location = new System.Drawing.Point(141, 70);
            this.cboChoGVCB.Name = "cboChoGVCB";
            this.cboChoGVCB.Size = new System.Drawing.Size(158, 21);
            this.cboChoGVCB.TabIndex = 10;
            // 
            // cboLoaiTL
            // 
            this.cboLoaiTL.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboLoaiTL.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboLoaiTL.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboLoaiTL.FormattingEnabled = true;
            this.cboLoaiTL.Location = new System.Drawing.Point(141, 41);
            this.cboLoaiTL.Name = "cboLoaiTL";
            this.cboLoaiTL.Size = new System.Drawing.Size(158, 21);
            this.cboLoaiTL.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(25, 44);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Loại tài liệu:";
            // 
            // txtTenTL
            // 
            this.txtTenTL.Location = new System.Drawing.Point(141, 15);
            this.txtTenTL.Name = "txtTenTL";
            this.txtTenTL.Size = new System.Drawing.Size(158, 20);
            this.txtTenTL.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Tên tài liệu:";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btnXemHet3);
            this.tabPage3.Controls.Add(this.btnXemTheoTheLoai);
            this.tabPage3.Controls.Add(this.cboLoaiLinhVuc);
            this.tabPage3.Controls.Add(this.label3);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(629, 188);
            this.tabPage3.TabIndex = 1;
            this.tabPage3.Text = "Xem theo loại/lĩnh vực";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // btnXemHet3
            // 
            this.btnXemHet3.Location = new System.Drawing.Point(213, 49);
            this.btnXemHet3.Name = "btnXemHet3";
            this.btnXemHet3.Size = new System.Drawing.Size(100, 26);
            this.btnXemHet3.TabIndex = 11;
            this.btnXemHet3.Text = "Xem hết";
            this.btnXemHet3.UseVisualStyleBackColor = true;
            this.btnXemHet3.Click += new System.EventHandler(this.btnXemHet3_Click);
            // 
            // btnXemTheoTheLoai
            // 
            this.btnXemTheoTheLoai.Location = new System.Drawing.Point(323, 49);
            this.btnXemTheoTheLoai.Name = "btnXemTheoTheLoai";
            this.btnXemTheoTheLoai.Size = new System.Drawing.Size(100, 26);
            this.btnXemTheoTheLoai.TabIndex = 10;
            this.btnXemTheoTheLoai.Text = "Xem";
            this.btnXemTheoTheLoai.UseVisualStyleBackColor = true;
            this.btnXemTheoTheLoai.Click += new System.EventHandler(this.btnXemTheoTheLoai_Click);
            // 
            // cboLoaiLinhVuc
            // 
            this.cboLoaiLinhVuc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboLoaiLinhVuc.FormattingEnabled = true;
            this.cboLoaiLinhVuc.Location = new System.Drawing.Point(213, 22);
            this.cboLoaiLinhVuc.Name = "cboLoaiLinhVuc";
            this.cboLoaiLinhVuc.Size = new System.Drawing.Size(210, 21);
            this.cboLoaiLinhVuc.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(132, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Loại/lĩnh vực:";
            // 
            // frmTimKiemTL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(661, 623);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnXoa);
            this.Controls.Add(this.btnQuayVe);
            this.Controls.Add(this.btnThemVaoPhieuMuon);
            this.Controls.Add(this.btnLapPhieuMuon);
            this.Controls.Add(this.btnYeuCauTL);
            this.Controls.Add(this.btnXemChiTiet);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmTimKiemTL";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tìm kiếm tài liệu";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmTimKiemTL_FormClosed);
            this.groupBox2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCoTheMuon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDangMuon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSoBanSao)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListView lvDSTL;
        private System.Windows.Forms.Button btnXemChiTiet;
        private System.Windows.Forms.Button btnYeuCauTL;
        private System.Windows.Forms.Button btnLapPhieuMuon;
        private System.Windows.Forms.Button btnThemVaoPhieuMuon;
        private System.Windows.Forms.Button btnQuayVe;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.Button btnXoa;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button btnXemHet1;
        private System.Windows.Forms.Button btnTimKiemCB;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNoiDungLocCB;
        private System.Windows.Forms.ComboBox cboLoaiBoLocCB;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown nudCoTheMuon;
        private System.Windows.Forms.ComboBox cboDKDangMuon;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnTimKiemNC;
        private System.Windows.Forms.NumericUpDown nudDangMuon;
        private System.Windows.Forms.ComboBox cboDKCoTheMuon;
        private System.Windows.Forms.NumericUpDown nudSoBanSao;
        private System.Windows.Forms.ComboBox cboDKSoBanSao;
        private System.Windows.Forms.ComboBox cboSachHiem;
        private System.Windows.Forms.ComboBox cboChoGVCB;
        private System.Windows.Forms.ComboBox cboLoaiTL;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtTenTL;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btnXemHet3;
        private System.Windows.Forms.Button btnXemTheoTheLoai;
        private System.Windows.Forms.ComboBox cboLoaiLinhVuc;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnXemHet2;
    }
}