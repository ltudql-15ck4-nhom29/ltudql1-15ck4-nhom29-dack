﻿namespace GUI
{
    partial class frmTiepNhanTL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnNhap = new System.Windows.Forms.Button();
            this.nudSoLuong = new System.Windows.Forms.NumericUpDown();
            this.cboLaSachHiem = new System.Windows.Forms.ComboBox();
            this.cboDanhChoGVCB = new System.Windows.Forms.ComboBox();
            this.cboLoaiTL = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTenTL = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnQuayVe = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cboTenTLCoSan = new System.Windows.Forms.ComboBox();
            this.cboMaTLCoSan = new System.Windows.Forms.ComboBox();
            this.btnNhapCoSan = new System.Windows.Forms.Button();
            this.nudSoLuongCoSan = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSoLuong)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSoLuongCoSan)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnNhap);
            this.groupBox1.Controls.Add(this.nudSoLuong);
            this.groupBox1.Controls.Add(this.cboLaSachHiem);
            this.groupBox1.Controls.Add(this.cboDanhChoGVCB);
            this.groupBox1.Controls.Add(this.cboLoaiTL);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtTenTL);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(464, 220);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tài liệu mới";
            // 
            // btnNhap
            // 
            this.btnNhap.Location = new System.Drawing.Point(313, 172);
            this.btnNhap.Name = "btnNhap";
            this.btnNhap.Size = new System.Drawing.Size(130, 36);
            this.btnNhap.TabIndex = 1;
            this.btnNhap.Text = "Nhập";
            this.btnNhap.UseVisualStyleBackColor = true;
            this.btnNhap.Click += new System.EventHandler(this.btnNhap_Click);
            // 
            // nudSoLuong
            // 
            this.nudSoLuong.Location = new System.Drawing.Point(178, 134);
            this.nudSoLuong.Name = "nudSoLuong";
            this.nudSoLuong.Size = new System.Drawing.Size(265, 20);
            this.nudSoLuong.TabIndex = 15;
            // 
            // cboLaSachHiem
            // 
            this.cboLaSachHiem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboLaSachHiem.FormattingEnabled = true;
            this.cboLaSachHiem.Items.AddRange(new object[] {
            "không",
            "có"});
            this.cboLaSachHiem.Location = new System.Drawing.Point(178, 107);
            this.cboLaSachHiem.Name = "cboLaSachHiem";
            this.cboLaSachHiem.Size = new System.Drawing.Size(265, 21);
            this.cboLaSachHiem.TabIndex = 14;
            // 
            // cboDanhChoGVCB
            // 
            this.cboDanhChoGVCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDanhChoGVCB.FormattingEnabled = true;
            this.cboDanhChoGVCB.Items.AddRange(new object[] {
            "không",
            "có"});
            this.cboDanhChoGVCB.Location = new System.Drawing.Point(178, 75);
            this.cboDanhChoGVCB.Name = "cboDanhChoGVCB";
            this.cboDanhChoGVCB.Size = new System.Drawing.Size(265, 21);
            this.cboDanhChoGVCB.TabIndex = 13;
            // 
            // cboLoaiTL
            // 
            this.cboLoaiTL.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboLoaiTL.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboLoaiTL.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboLoaiTL.FormattingEnabled = true;
            this.cboLoaiTL.Location = new System.Drawing.Point(178, 45);
            this.cboLoaiTL.Name = "cboLoaiTL";
            this.cboLoaiTL.Size = new System.Drawing.Size(265, 21);
            this.cboLoaiTL.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(44, 136);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Số lượng:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(44, 110);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Là sách hiếm:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(44, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 26);
            this.label3.TabIndex = 6;
            this.label3.Text = "Chỉ dành cho giảng\r\nviên/cán bộ trường:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(44, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Loại tài liệu:";
            // 
            // txtTenTL
            // 
            this.txtTenTL.Location = new System.Drawing.Point(178, 19);
            this.txtTenTL.Name = "txtTenTL";
            this.txtTenTL.Size = new System.Drawing.Size(265, 20);
            this.txtTenTL.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(44, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tên tài liệu:";
            // 
            // btnQuayVe
            // 
            this.btnQuayVe.Location = new System.Drawing.Point(12, 411);
            this.btnQuayVe.Name = "btnQuayVe";
            this.btnQuayVe.Size = new System.Drawing.Size(130, 36);
            this.btnQuayVe.TabIndex = 2;
            this.btnQuayVe.Text = "Quay về";
            this.btnQuayVe.UseVisualStyleBackColor = true;
            this.btnQuayVe.Click += new System.EventHandler(this.btnQuayVe_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cboTenTLCoSan);
            this.groupBox2.Controls.Add(this.cboMaTLCoSan);
            this.groupBox2.Controls.Add(this.btnNhapCoSan);
            this.groupBox2.Controls.Add(this.nudSoLuongCoSan);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Location = new System.Drawing.Point(12, 238);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(464, 167);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Tài liệu có sẵn";
            // 
            // cboTenTLCoSan
            // 
            this.cboTenTLCoSan.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboTenTLCoSan.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboTenTLCoSan.FormattingEnabled = true;
            this.cboTenTLCoSan.Location = new System.Drawing.Point(178, 55);
            this.cboTenTLCoSan.Name = "cboTenTLCoSan";
            this.cboTenTLCoSan.Size = new System.Drawing.Size(265, 21);
            this.cboTenTLCoSan.TabIndex = 21;
            this.cboTenTLCoSan.SelectedIndexChanged += new System.EventHandler(this.cboTenTLCoSan_SelectedIndexChanged);
            // 
            // cboMaTLCoSan
            // 
            this.cboMaTLCoSan.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboMaTLCoSan.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboMaTLCoSan.FormattingEnabled = true;
            this.cboMaTLCoSan.Location = new System.Drawing.Point(178, 29);
            this.cboMaTLCoSan.Name = "cboMaTLCoSan";
            this.cboMaTLCoSan.Size = new System.Drawing.Size(265, 21);
            this.cboMaTLCoSan.TabIndex = 20;
            this.cboMaTLCoSan.SelectedIndexChanged += new System.EventHandler(this.cboMaTLCoSan_SelectedIndexChanged);
            // 
            // btnNhapCoSan
            // 
            this.btnNhapCoSan.Location = new System.Drawing.Point(313, 119);
            this.btnNhapCoSan.Name = "btnNhapCoSan";
            this.btnNhapCoSan.Size = new System.Drawing.Size(130, 36);
            this.btnNhapCoSan.TabIndex = 17;
            this.btnNhapCoSan.Text = "Nhập";
            this.btnNhapCoSan.UseVisualStyleBackColor = true;
            this.btnNhapCoSan.Click += new System.EventHandler(this.btnNhapCoSan_Click);
            // 
            // nudSoLuongCoSan
            // 
            this.nudSoLuongCoSan.Location = new System.Drawing.Point(178, 81);
            this.nudSoLuongCoSan.Name = "nudSoLuongCoSan";
            this.nudSoLuongCoSan.Size = new System.Drawing.Size(265, 20);
            this.nudSoLuongCoSan.TabIndex = 19;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(44, 83);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "Số lượng:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(44, 58);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Tên tài liệu:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(44, 32);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Mã tài liệu:";
            // 
            // frmTiepNhanTL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(499, 456);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnQuayVe);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmTiepNhanTL";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tiếp nhận tài liệu mới";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSoLuong)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSoLuongCoSan)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown nudSoLuong;
        private System.Windows.Forms.ComboBox cboLaSachHiem;
        private System.Windows.Forms.ComboBox cboDanhChoGVCB;
        private System.Windows.Forms.ComboBox cboLoaiTL;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTenTL;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnNhap;
        private System.Windows.Forms.Button btnQuayVe;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cboTenTLCoSan;
        private System.Windows.Forms.ComboBox cboMaTLCoSan;
        private System.Windows.Forms.Button btnNhapCoSan;
        private System.Windows.Forms.NumericUpDown nudSoLuongCoSan;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
    }
}