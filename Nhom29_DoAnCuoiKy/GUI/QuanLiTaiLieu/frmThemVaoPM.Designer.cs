﻿namespace GUI
{
    partial class frmThemVaoPM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cboPhieuMuon = new System.Windows.Forms.ComboBox();
            this.lblMaBanSao = new System.Windows.Forms.Label();
            this.lblTenTL = new System.Windows.Forms.Label();
            this.lblHanChotTra = new System.Windows.Forms.Label();
            this.btnThem = new System.Windows.Forms.Button();
            this.btnQuayVe = new System.Windows.Forms.Button();
            this.lblTenDG = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblLoaiDocGia = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnChiTietLoaiDG = new System.Windows.Forms.Button();
            this.lblDanhChoGVCB = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(38, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Mã phiếu mượn:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(38, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Mã bản sao:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(38, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Tên tài liệu:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(38, 194);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Hạn chót trả:";
            // 
            // cboPhieuMuon
            // 
            this.cboPhieuMuon.FormattingEnabled = true;
            this.cboPhieuMuon.Location = new System.Drawing.Point(179, 30);
            this.cboPhieuMuon.Name = "cboPhieuMuon";
            this.cboPhieuMuon.Size = new System.Drawing.Size(175, 21);
            this.cboPhieuMuon.TabIndex = 10;
            this.cboPhieuMuon.SelectedIndexChanged += new System.EventHandler(this.cboPhieuMuon_SelectedIndexChanged);
            // 
            // lblMaBanSao
            // 
            this.lblMaBanSao.AutoSize = true;
            this.lblMaBanSao.Location = new System.Drawing.Point(176, 113);
            this.lblMaBanSao.Name = "lblMaBanSao";
            this.lblMaBanSao.Size = new System.Drawing.Size(59, 13);
            this.lblMaBanSao.TabIndex = 12;
            this.lblMaBanSao.Text = "maBanSao";
            // 
            // lblTenTL
            // 
            this.lblTenTL.AutoSize = true;
            this.lblTenTL.Location = new System.Drawing.Point(176, 66);
            this.lblTenTL.Name = "lblTenTL";
            this.lblTenTL.Size = new System.Drawing.Size(57, 13);
            this.lblTenTL.TabIndex = 13;
            this.lblTenTL.Text = "tenTaiLieu";
            // 
            // lblHanChotTra
            // 
            this.lblHanChotTra.AutoSize = true;
            this.lblHanChotTra.Location = new System.Drawing.Point(176, 194);
            this.lblHanChotTra.Name = "lblHanChotTra";
            this.lblHanChotTra.Size = new System.Drawing.Size(63, 13);
            this.lblHanChotTra.TabIndex = 14;
            this.lblHanChotTra.Text = "hanChotTra";
            // 
            // btnThem
            // 
            this.btnThem.Location = new System.Drawing.Point(226, 227);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(172, 41);
            this.btnThem.TabIndex = 16;
            this.btnThem.Text = "Thêm vào phiếu mượn";
            this.btnThem.UseVisualStyleBackColor = true;
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // btnQuayVe
            // 
            this.btnQuayVe.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnQuayVe.Location = new System.Drawing.Point(41, 227);
            this.btnQuayVe.Name = "btnQuayVe";
            this.btnQuayVe.Size = new System.Drawing.Size(172, 41);
            this.btnQuayVe.TabIndex = 17;
            this.btnQuayVe.Text = "Quay về";
            this.btnQuayVe.UseVisualStyleBackColor = true;
            // 
            // lblTenDG
            // 
            this.lblTenDG.AutoSize = true;
            this.lblTenDG.Location = new System.Drawing.Point(176, 139);
            this.lblTenDG.Name = "lblTenDG";
            this.lblTenDG.Size = new System.Drawing.Size(58, 13);
            this.lblTenDG.TabIndex = 19;
            this.lblTenDG.Text = "tenDocGia";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(38, 139);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Tên độc giả:";
            // 
            // lblLoaiDocGia
            // 
            this.lblLoaiDocGia.AutoSize = true;
            this.lblLoaiDocGia.Location = new System.Drawing.Point(176, 166);
            this.lblLoaiDocGia.Name = "lblLoaiDocGia";
            this.lblLoaiDocGia.Size = new System.Drawing.Size(59, 13);
            this.lblLoaiDocGia.TabIndex = 21;
            this.lblLoaiDocGia.Text = "loaiDocGia";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(38, 166);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Loại độc giả:";
            // 
            // btnChiTietLoaiDG
            // 
            this.btnChiTietLoaiDG.Location = new System.Drawing.Point(292, 161);
            this.btnChiTietLoaiDG.Name = "btnChiTietLoaiDG";
            this.btnChiTietLoaiDG.Size = new System.Drawing.Size(62, 23);
            this.btnChiTietLoaiDG.TabIndex = 22;
            this.btnChiTietLoaiDG.Text = "Chi tiết";
            this.btnChiTietLoaiDG.UseVisualStyleBackColor = true;
            this.btnChiTietLoaiDG.Click += new System.EventHandler(this.btnChiTietLoaiDG_Click);
            // 
            // lblDanhChoGVCB
            // 
            this.lblDanhChoGVCB.AutoSize = true;
            this.lblDanhChoGVCB.Location = new System.Drawing.Point(176, 90);
            this.lblDanhChoGVCB.Name = "lblDanhChoGVCB";
            this.lblDanhChoGVCB.Size = new System.Drawing.Size(79, 13);
            this.lblDanhChoGVCB.TabIndex = 24;
            this.lblDanhChoGVCB.Text = "danhChoGVCB";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(38, 90);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 13);
            this.label8.TabIndex = 23;
            this.label8.Text = "Dành cho GV/CB:";
            // 
            // frmThemVaoPM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(438, 295);
            this.Controls.Add(this.lblDanhChoGVCB);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btnChiTietLoaiDG);
            this.Controls.Add(this.lblLoaiDocGia);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lblTenDG);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnQuayVe);
            this.Controls.Add(this.btnThem);
            this.Controls.Add(this.lblHanChotTra);
            this.Controls.Add(this.lblTenTL);
            this.Controls.Add(this.lblMaBanSao);
            this.Controls.Add(this.cboPhieuMuon);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmThemVaoPM";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thêm vào phiếu mượn";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cboPhieuMuon;
        private System.Windows.Forms.Label lblMaBanSao;
        private System.Windows.Forms.Label lblTenTL;
        private System.Windows.Forms.Label lblHanChotTra;
        private System.Windows.Forms.Button btnThem;
        private System.Windows.Forms.Button btnQuayVe;
        private System.Windows.Forms.Label lblTenDG;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblLoaiDocGia;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnChiTietLoaiDG;
        private System.Windows.Forms.Label lblDanhChoGVCB;
        private System.Windows.Forms.Label label8;
    }
}