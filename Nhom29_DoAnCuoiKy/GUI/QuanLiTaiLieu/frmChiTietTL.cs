﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BLL;

namespace GUI
{
    public partial class frmChiTietTL : Form
    {
        TaiLieuDTO taiLieu = null;

        List<LoaiTaiLieuDTO> lstLoaiTaiLieu = null;
        List<DocGiaDTO> lstDocGia = null;

        public frmChiTietTL(TaiLieuDTO tl)
        {
            InitializeComponent();
            taiLieu = tl;

            napDSDocGia();
            napDSLoaiTaiLieu();

            hienThiCboLoaiTaiLieu();
            hienThiThongTinTaiLieu();
            hienThiDSBanSao();

            khoaChinhSua();
        }

        void khoaChinhSua()
        {
            txtTenTL.ReadOnly = true;
            cboLoaiTL.Enabled = false;
            cboChoGVCB.Enabled = false;
            cboLaSachHiem.Enabled = false;
        }

        void moChinhSua()
        {
            txtTenTL.ReadOnly = false;
            cboLoaiTL.Enabled = true;
            cboChoGVCB.Enabled = true;
            cboLaSachHiem.Enabled = true;
        }

        void napDSDocGia()
        {
            DocGiaBLL bll = new DocGiaBLL();
            lstDocGia = bll.layDSDocGia();
        }

        void napDSLoaiTaiLieu()
        {
            LoaiTaiLieuBLL bll = new LoaiTaiLieuBLL();
            lstLoaiTaiLieu = bll.layDSLoaiTaiLieu();
        }

        void hienThiCboLoaiTaiLieu()
        {
            cboLoaiTL.Items.Clear();
            for(int i = 0; i < lstLoaiTaiLieu.Count; i++)
            {
                cboLoaiTL.Items.Add(lstLoaiTaiLieu[i]);
            }
        }

        void hienThiThongTinTaiLieu()
        {
            lblMaTL.Text = taiLieu.MaTaiLieu.ToString();
            txtTenTL.Text = taiLieu.TenTaiLieu;
            LoaiTaiLieuDTO ltl = LoaiTaiLieuDTO.layLoaiTaiLieuTheoTaiLieu(taiLieu, lstLoaiTaiLieu);
            cboLoaiTL.SelectedItem = ltl;
            cboChoGVCB.SelectedIndex = 0;
            if(taiLieu.ChoGVCB)
            {
                cboChoGVCB.SelectedIndex = 1;
            }
            cboLaSachHiem.SelectedIndex = 0;
            if(taiLieu.LaSachHiem)
            {
                cboLaSachHiem.SelectedIndex = 1;
            }
            lblSoBanSao.Text = taiLieu.LstBanSao.Count.ToString();
            lblSoTLDangMuon.Text = taiLieu.laySoBanSaoDangDuocMuon().ToString();
            lblSoTLCoTheMuon.Text = taiLieu.laySoBanSaoCoTheMuon().ToString();
        }

        void hienThiDSBanSao()
        {
            lvDanhSachBanSao.Items.Clear();
            List<BanSaoDTO> ds = taiLieu.LstBanSao;
            for(int i = 0; i < ds.Count; i++)
            {
                BanSaoDTO bs = ds[i];
                ListViewItem item = new ListViewItem(bs.MaBanSao.ToString());
                if(bs.MaDocGiaDangMuon > -1)
                {
                    DocGiaDTO dg = DocGiaDTO.layDocGiaTheoBanSao(bs, lstDocGia);
                    item.SubItems.Add(bs.MaDocGiaDangMuon.ToString());
                    item.SubItems.Add(dg.HoTen);
                }
                else
                {
                    item.SubItems.Add("");
                    item.SubItems.Add("");
                }
                lvDanhSachBanSao.Items.Add(item);
            }
        }

        TaiLieuDTO taoTaiLieu()
        {
            TaiLieuDTO tl = new TaiLieuDTO();
            tl.MaTaiLieu = taiLieu.MaTaiLieu;
            tl.TenTaiLieu = txtTenTL.Text;
            tl.LoaiTaiLieu = ((LoaiTaiLieuDTO)cboLoaiTL.SelectedItem).MaLoai;
            tl.ChoGVCB = Convert.ToBoolean(cboChoGVCB.SelectedIndex);
            tl.LaSachHiem = Convert.ToBoolean(cboLaSachHiem.SelectedIndex);
            tl.LstBanSao = taiLieu.LstBanSao;
            return tl;
        }

        private void btnChinhSua_Click(object sender, EventArgs e)
        {
            if (btnChinhSua.Text == "Chỉnh sửa")
            {
                btnChinhSua.Text = "Hủy";
                btnLuu.Enabled = true;
                moChinhSua();
            }
            else if (btnChinhSua.Text == "Hủy")
            {
                if (DialogResult.Yes == MessageBox.Show("Bạn có chắc muốn hủy thay đổi?", "Hủy", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                {
                    hienThiThongTinTaiLieu();
                    btnChinhSua.Text = "Chỉnh sửa";
                    btnLuu.Enabled = false;
                    khoaChinhSua();
                }
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (txtTenTL.Text == "")
            {
                MessageBox.Show("Tên tài liệu trống!", "Lưu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (cboLoaiTL.SelectedIndex == -1)
            {
                MessageBox.Show("Không có loại tài liệu nào được chọn!", "Lưu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (DialogResult.Yes == MessageBox.Show("Bạn có chắc muốn lưu thay đổi?", "Lưu", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                TaiLieuDTO tl = taoTaiLieu();
                TaiLieuBLL bll = new TaiLieuBLL();
                if (bll.capNhatTaiLieu(tl) == 1)
                {
                    MessageBox.Show("Cập nhật tài liệu thành công!", "Lưu", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    btnLuu.Enabled = false;
                    btnChinhSua.Text = "Chỉnh sửa";
                    khoaChinhSua();
                    taiLieu = tl;
                    hienThiThongTinTaiLieu();
                }
                else
                {
                    MessageBox.Show("Cập nhật tài liệu thất bại!", "Lưu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnQuayVe_Click(object sender, EventArgs e)
        {

        }
    }
}
