﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BLL;

namespace GUI
{
    public partial class frmThemVaoPM : Form
    {
        TaiLieuDTO taiLieu = null;

        List<PhieuMuonDTO> lstPhieuMuon = null;
        List<DocGiaDTO> lstDocGia = null;
        List<LoaiDocGiaDTO> lstLoaiDocGia = null;

        PhieuMuonDTO phieuMuon = null;
        DocGiaDTO docGia = null;
        LoaiDocGiaDTO loaiDocGia = null;

        BanSaoDTO banSao = null;

        public frmThemVaoPM(TaiLieuDTO tl)
        {
            InitializeComponent();
            taiLieu = tl;

            napDSDocGia();
            napDSLoaiDocGia();
            napDSPhieuMuon();

            hienThiCboPhieuMuon();
            hienThiThongTinTaiLieu();
            lblTenDG.Text = "";
            lblLoaiDocGia.Text = "";
            lblHanChotTra.Text = "";
        }

        void napDSPhieuMuon()
        {
            PhieuMuonBLL bll = new PhieuMuonBLL();
            lstPhieuMuon = bll.layDSPhieuMuonDayDu();
        }

        void napDSDocGia()
        {
            DocGiaBLL bll = new DocGiaBLL();
            lstDocGia = bll.layDSDocGia();
        }

        void napDSLoaiDocGia()
        {
            LoaiDocGiaBLL bll = new LoaiDocGiaBLL();
            lstLoaiDocGia = bll.layDSLoaiDocGia();
        }

        void hienThiCboPhieuMuon()
        {
            cboPhieuMuon.Items.Clear();
            cboPhieuMuon.DisplayMember = "MaPhieu";
            for(int i = 0; i < lstPhieuMuon.Count; i++)
            {
                cboPhieuMuon.Items.Add(lstPhieuMuon[i]);
            }
        }

        void hienThiThongTinTaiLieu()
        {
            lblTenTL.Text = taiLieu.TenTaiLieu;
            if(taiLieu.ChoGVCB)
            {
                lblDanhChoGVCB.Text = "Có";
            }
            else
            {
                lblDanhChoGVCB.Text = "Không";
            }
            for(int i = 0; i < taiLieu.LstBanSao.Count; i++)
            {
                if(taiLieu.LstBanSao[i].MaDocGiaDangMuon == -1)
                {
                    banSao = taiLieu.LstBanSao[i];
                    break;
                }
            }
            lblMaBanSao.Text = banSao.MaBanSao.ToString();
        }

        void hienThiThongTinTuPhieuMuon()
        {
            phieuMuon = (PhieuMuonDTO)cboPhieuMuon.SelectedItem;
            docGia = DocGiaDTO.layDocGiaTheoPhieuMuon(phieuMuon, lstDocGia);
            loaiDocGia = LoaiDocGiaDTO.layLoaiDocGiaTheoDocGia(docGia, lstLoaiDocGia);
            lblTenDG.Text = docGia.HoTen;
            lblLoaiDocGia.Text = loaiDocGia.TenLoai;
            lblHanChotTra.Text = phieuMuon.LstCTPM[0].HanChotTra.ToString("dd/MM/yyyy");
        }

        private void cboPhieuMuon_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cboPhieuMuon.SelectedIndex > -1)
            {
                hienThiThongTinTuPhieuMuon();
            }
        }

        private void btnChiTietLoaiDG_Click(object sender, EventArgs e)
        {
            if(loaiDocGia != null)
            {
                frmChiTietLoaiDG f = new frmChiTietLoaiDG(loaiDocGia);
                f.ShowDialog();
            }
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            if(cboPhieuMuon.SelectedIndex == -1)
            {
                MessageBox.Show("Không có phiếu mượn nào được chọn!", "Thêm vào phiếu mượn", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if(docGia.SoTaiLieuDangMuon >= loaiDocGia.SoTaiLieuMuonMax)
            {
                MessageBox.Show("Độc giả không thể mượn thêm tài liệu!", "Thêm vào phiếu mượn", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if(taiLieu.ChoGVCB && !loaiDocGia.LaGVCB)
            {
                MessageBox.Show("Tài liệu chỉ dành cho giảng viên/cán bộ!", "Thêm vào phiếu mượn", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if(DialogResult.Yes == MessageBox.Show("Bạn có chắc muốn thêm tài liệu này vào phiếu mượn?", "Thêm vào phiếu mượn", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                ChiTietPhieuMuonDTO ctpm = new ChiTietPhieuMuonDTO();
                ctpm.MaBanSao = banSao.MaBanSao;
                ctpm.MaPhieuMuon = phieuMuon.MaPhieu;
                ctpm.HanChotTra = phieuMuon.LstCTPM[0].HanChotTra;
                ctpm.SoThuTuCTPT = -1;
                ChiTietPhieuMuonBLL bll1 = new ChiTietPhieuMuonBLL();
                if(bll1.themChiTietPhieuMuon(ctpm) == 1)
                {
                    MessageBox.Show("Thêm vào phiếu mượn thành công!", "Thêm vào phiếu mượn", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    banSao.MaDocGiaDangMuon = docGia.MaDocGia;
                    BanSaoBLL bll2 = new BanSaoBLL();
                    bll2.capNhatBanSao(banSao);
                    docGia.SoTaiLieuDangMuon++;
                    DocGiaBLL bll3 = new DocGiaBLL();
                    bll3.capNhatSoTLDangMuon(docGia);
                    Close();
                }
                else
                {
                    MessageBox.Show("Thêm vào phiếu mượn thất bại!", "Thêm vào phiếu mượn", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
