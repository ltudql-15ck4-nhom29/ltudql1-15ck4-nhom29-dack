﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class NguoiDungDTO
    {
        int maNguoiDung;

        public int MaNguoiDung
        {
            get { return maNguoiDung; }
            set { maNguoiDung = value; }
        }

        string tenNguoiDung;

        public string TenNguoiDung
        {
            get { return tenNguoiDung; }
            set { tenNguoiDung = value; }
        }

        string matKhau;

        public string MatKhau
        {
            get { return matKhau; }
            set { matKhau = value; }
        }

        int capBac;

        public int CapBac
        {
            get { return capBac; }
            set { capBac = value; }
        }
    }
}
