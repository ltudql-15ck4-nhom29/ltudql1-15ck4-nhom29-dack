﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class PhieuTraDTO
    {
        int maPhieu;

        public int MaPhieu
        {
            get { return maPhieu; }
            set { maPhieu = value; }
        }

        DateTime ngayLapPhieu;

        public DateTime NgayLapPhieu
        {
            get { return ngayLapPhieu; }
            set { ngayLapPhieu = value; }
        }

        int maDocGia;

        public int MaDocGia
        {
            get { return maDocGia; }
            set { maDocGia = value; }
        }

        List<ChiTietPhieuTraDTO> lstCTPT;

        public List<ChiTietPhieuTraDTO> LstCTPT
        {
            get { return lstCTPT; }
            set { lstCTPT = value; }
        }

        List<int> lstMaPM;

        public List<int> LstMaPM
        {
            get { return lstMaPM; }
            set { lstMaPM = value; }
        }

        public static List<PhieuTraDTO> layDSPhieuTraDayDu(List<PhieuTraDTO> lst1, List<ChiTietPhieuTraDTO> lst2)
        {
            for (int i = 0; i < lst1.Count; i++)
            {
                lst1[i].lstCTPT = ChiTietPhieuTraDTO.layDSChiTietPhieuTraTheoPhieuTra(lst1[i], lst2);
            }
            return lst1;
        }

        public void tinhDSMaPhieuMuon()
        {
            List<int> ds = new List<int>();
            for(int i = 0; i < lstCTPT.Count; i++)
            {
                ChiTietPhieuTraDTO ctpt = lstCTPT[i];
                bool daCo = false;
                for(int j = 0; j < ds.Count; j++)
                {
                    if(ds[j] == ctpt.MaPhieuMuon)
                    {
                        daCo = true;
                        break;
                    }
                }
                if(!daCo)
                {
                    ds.Add(ctpt.MaPhieuMuon);
                }
            }
            lstMaPM = ds;
        }

        public string taoChuoiDSMaPhieuMuon()
        {
            string s = "";
            for (int i = 0; i < lstMaPM.Count; i++)
            {
                if (i > 0)
                {
                    s += ", ";
                }
                s += lstMaPM[i].ToString();
            }
            return s;
        }
    }
}
