﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DocGiaDTO
    {
        int maDocGia;

        public int MaDocGia
        {
            get { return maDocGia; }
            set { maDocGia = value; }
        }

        string hoTen;

        public string HoTen
        {
            get { return hoTen; }
            set { hoTen = value; }
        }

        string cmnd;

        public string CMND
        {
            get { return cmnd; }
            set { cmnd = value; }
        }

        DateTime ngaySinh;

        public DateTime NgaySinh
        {
            get { return ngaySinh; }
            set { ngaySinh = value; }
        }

        string soDienThoai;

        public string SoDienThoai
        {
            get { return soDienThoai; }
            set { soDienThoai = value; }
        }

        string email;

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        string diaChi;

        public string DiaChi
        {
            get { return diaChi; }
            set { diaChi = value; }
        }

        string mssv;

        public string MSSV
        {
            get { return mssv; }
            set { mssv = value; }
        }

        string mscb;

        public string MSCB
        {
            get { return mscb; }
            set { mscb = value; }
        }

        int loaiDocGia;

        public int LoaiDocGia
        {
            get { return loaiDocGia; }
            set { loaiDocGia = value; }
        }

        int soTaiLieuDangMuon;

        public int SoTaiLieuDangMuon
        {
            get { return soTaiLieuDangMuon; }
            set { soTaiLieuDangMuon = value; }
        }

        int maHinhThucPhat;

        public int MaHinhThucPhat
        {
            get { return maHinhThucPhat; }
            set { maHinhThucPhat = value; }
        }

        int soTaiLieuDangMuonQuaHan;

        public int SoTaiLieuDangMuonQuaHan
        {
            get { return soTaiLieuDangMuonQuaHan; }
            set { soTaiLieuDangMuonQuaHan = value; }
        }

        List<PhieuMuonDTO> lstPM;

        public List<PhieuMuonDTO> LstPM
        {
            get { return lstPM; }
            set { lstPM = value; }
        }

        public override string ToString()
        {
            return String.Format("{0} - {1}", maDocGia, hoTen);
        }

        public void tinhSoTLQuaHan()
        {
            int soLuongQuaHan = 0;
            for(int i = 0; i < lstPM.Count; i++)
            {
                List<ChiTietPhieuMuonDTO> lst = lstPM[i].LstCTPM;
                for(int j = 0; j < lst.Count; j++)
                {
                    if(lst[j].SoThuTuCTPT == -1 && lst[j].HanChotTra.Date < DateTime.Now.Date)
                    {
                        soLuongQuaHan++;
                    }
                    else if(lst[j].SoThuTuCTPT != -1 && lst[j].HanChotTra.Date < lst[j].NgayTra.Date)
                    {
                        soLuongQuaHan++;
                    }
                }
            }
            soTaiLieuDangMuonQuaHan = soLuongQuaHan;
        }

        public static DocGiaDTO layDocGiaTheoPhieuMuon(PhieuMuonDTO pm, List<DocGiaDTO> lst)
        {
            for (int i = 0; i < lst.Count; i++)
            {
                if(lst[i].maDocGia == pm.MaDocGia)
                {
                    return lst[i];
                }
            }
            return null;
        }

        public static DocGiaDTO layDocGiaTheoPhieuTra(PhieuTraDTO pm, List<DocGiaDTO> lst)
        {
            for (int i = 0; i < lst.Count; i++)
            {
                if (lst[i].maDocGia == pm.MaDocGia)
                {
                    return lst[i];
                }
            }
            return null;
        }

        public static DocGiaDTO layDocGiaTheoBanSao(BanSaoDTO bs, List<DocGiaDTO> lst)
        {
            for(int i = 0; i < lst.Count; i++)
            {
                if(lst[i].maDocGia == bs.MaDocGiaDangMuon)
                {
                    return lst[i];
                }
            }
            return null;
        }
    }
}
