﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class PhieuCanhCaoDTO
    {
        int maPhieu;

        public int MaPhieu
        {
            get { return maPhieu; }
            set { maPhieu = value; }
        }

        int maDocGia;

        public int MaDocGia
        {
            get { return maDocGia; }
            set { maDocGia = value; }
        }

        DateTime ngayLapPhieu;

        public DateTime NgayLapPhieu
        {
            get { return ngayLapPhieu; }
            set { ngayLapPhieu = value; }
        }

        int maHinhThucPhat;

        public int MaHinhThucPhat
        {
            get { return maHinhThucPhat; }
            set { maHinhThucPhat = value; }
        }
    }
}
