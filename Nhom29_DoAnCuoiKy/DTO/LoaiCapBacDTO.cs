﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class LoaiCapBacDTO
    {
        int maCapBac;
        
        public int MaCapBac
        {
            get { return maCapBac; }
            set { maCapBac = value; }
        }

        string tenCapBac;

        public string TenCapBac
        {
            get { return tenCapBac; }
            set { tenCapBac = value; }
        }

        //Hàm lấy một LoaiCapBac nếu cung cấp maCB
        //Các hàm tương tự như thế này nên đặt trong DTO
        public static LoaiCapBacDTO layLoaiCapBac(int maCB, List<LoaiCapBacDTO> lst)
        {
            for(int i = 0; i < lst.Count; i++)
            {
                if(lst[i].maCapBac == maCB)
                {
                    return lst[i];
                }
            }
            return null;
        }

        public override string ToString()
        {
            return String.Format("{0} - {1}", maCapBac, tenCapBac);
        }
    }
}
