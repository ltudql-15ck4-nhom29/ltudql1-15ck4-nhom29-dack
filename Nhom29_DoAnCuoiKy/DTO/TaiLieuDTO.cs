﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class TaiLieuDTO
    {
        int maTaiLieu;

        public int MaTaiLieu
        {
            get { return maTaiLieu; }
            set { maTaiLieu = value; }
        }

        string tenTaiLieu;

        public string TenTaiLieu
        {
            get { return tenTaiLieu; }
            set { tenTaiLieu = value; }
        }

        int loaiTaiLieu;

        public int LoaiTaiLieu
        {
            get { return loaiTaiLieu; }
            set { loaiTaiLieu = value; }
        }

        bool choGVCB;

        public bool ChoGVCB
        {
            get { return choGVCB; }
            set { choGVCB = value; }
        }

        bool laSachHiem;

        public bool LaSachHiem
        {
            get { return laSachHiem; }
            set { laSachHiem = value; }
        }

        List<BanSaoDTO> lstBanSao;

        public List<BanSaoDTO> LstBanSao
        {
            get { return lstBanSao; }
            set { lstBanSao = value; }
        }

        public int laySoBanSaoDangDuocMuon()
        {
            int count = 0;
            List<BanSaoDTO> ds = lstBanSao;
            for (int i = 0; i < ds.Count; i++)
            {
                if (ds[i].MaDocGiaDangMuon > -1)
                {
                    count++;
                }
            }
            return count;
        }

        public int laySoBanSaoCoTheMuon()
        {
            int count = 0;
            List<BanSaoDTO> ds = lstBanSao;
            for (int i = 0; i < ds.Count; i++)
            {
                if (ds[i].MaDocGiaDangMuon == -1)
                {
                    count++;
                }
            }
            return count;
        }

        //Dựa vào Mã tài liệu của Bản sao để lấy Tài liệu
        //tương ứng trong list Tài liệu
        public static TaiLieuDTO layTaiLieuTheoBanSao(BanSaoDTO bs, List<TaiLieuDTO> lst)
        {
            for (int i = 0; i < lst.Count; i++)
            {
                if(lst[i].maTaiLieu == bs.MaTaiLieu)
                {
                    return lst[i];
                }
            }
            return null;
        }

        public static TaiLieuDTO layTaiLieuTheoChiTietPhieuMuon(ChiTietPhieuMuonDTO ctpm, List<TaiLieuDTO> lst)
        {
            for (int i = 0; i < lst.Count; i++)
            {
                List<BanSaoDTO> lstBS = lst[i].lstBanSao;
                for(int j = 0; j < lstBS.Count; j++)
                {
                    BanSaoDTO bs = lstBS[j];
                    if(bs.MaBanSao == ctpm.MaBanSao)
                    {
                        return lst[i];
                    }
                }
            }
            return null;
        }

        public static TaiLieuDTO layTaiLieuTheoChiTietPhieuTra(ChiTietPhieuTraDTO ctpt, List<TaiLieuDTO> lst)
        {
            for (int i = 0; i < lst.Count; i++)
            {
                List<BanSaoDTO> lstBS = lst[i].lstBanSao;
                for (int j = 0; j < lstBS.Count; j++)
                {
                    BanSaoDTO bs = lstBS[j];
                    if (bs.MaBanSao == ctpt.MaBanSao)
                    {
                        return lst[i];
                    }
                }
            }
            return null;
        }

        public static List<TaiLieuDTO> layDSTaiLieuCoDSBanSao(List<TaiLieuDTO> lst1, List<BanSaoDTO> lst2)
        {
            for (int i = 0; i < lst1.Count; i++)
            {
                lst1[i].lstBanSao = BanSaoDTO.layDSBanSaoTheoTaiLieu(lst1[i], lst2);
            }
            return lst1;
        }

        public static List<TaiLieuDTO> layDSTaiLieuTheoDSBanSao(List<BanSaoDTO> lst1, List<TaiLieuDTO> lst2)
        {
            List<TaiLieuDTO> ds = new List<TaiLieuDTO>();
            for(int i = 0; i < lst1.Count; i++)
            {
                for(int j = 0; j < lst2.Count; j++)
                {
                    if(lst2[j].maTaiLieu == lst1[i].MaTaiLieu)
                    {
                        ds.Add(lst2[j]);
                    }
                }
            }
            return ds;
        }
    }
}
