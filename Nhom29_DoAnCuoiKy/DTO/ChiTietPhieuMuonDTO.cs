﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class ChiTietPhieuMuonDTO
    {
        int soThuTu;

        public int SoThuTu
        {
            get { return soThuTu; }
            set { soThuTu = value; }
        }

        int maPhieuMuon;

        public int MaPhieuMuon
        {
            get { return maPhieuMuon; }
            set { maPhieuMuon = value; }
        }

        int maBanSao;

        public int MaBanSao
        {
            get { return maBanSao; }
            set { maBanSao = value; }
        }

        DateTime hanChotTra;

        public DateTime HanChotTra
        {
            get { return hanChotTra; }
            set { hanChotTra = value; }
        }

        int soThuTuCTPT;

        public int SoThuTuCTPT
        {
            get { return soThuTuCTPT; }
            set { soThuTuCTPT = value; }
        }

        DateTime ngayTra;

        public DateTime NgayTra
        {
            get { return ngayTra; }
            set { ngayTra = value; }
        }

        public static List<ChiTietPhieuMuonDTO> layDSChiTietPhieuMuonTheoPhieuMuon(PhieuMuonDTO pm, List<ChiTietPhieuMuonDTO> lst)
        {
            List<ChiTietPhieuMuonDTO> ds = new List<ChiTietPhieuMuonDTO>();
            for(int i = 0; i < lst.Count; i++)
            {
                if(lst[i].maPhieuMuon == pm.MaPhieu)
                {
                    ds.Add(lst[i]);
                }
            }
            return ds;
        }
    }
}
