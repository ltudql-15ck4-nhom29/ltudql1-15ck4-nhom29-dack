﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class LoaiDocGiaDTO
    {
        int maLoai;

        public int MaLoai
        {
            get { return maLoai; }
            set { maLoai = value; }
        }

        string tenLoai;

        public string TenLoai
        {
            get { return tenLoai; }
            set { tenLoai = value; }
        }

        bool laGVCB;

        public bool LaGVCB
        {
            get { return laGVCB; }
            set { laGVCB = value; }
        }

        bool laSinhVien;

        public bool LaSinhVien
        {
            get { return laSinhVien; }
            set { laSinhVien = value; }
        }

        int soNgayMuonMax;

        public int SoNgayMuonMax
        {
            get { return soNgayMuonMax; }
            set { soNgayMuonMax = value; }
        }

        int soTaiLieuMuonMax;

        public int SoTaiLieuMuonMax
        {
            get { return soTaiLieuMuonMax; }
            set { soTaiLieuMuonMax = value; }
        }

        int phiThuongNien;

        public int PhiThuongNien
        {
            get { return phiThuongNien; }
            set { phiThuongNien = value; }
        }

        public static LoaiDocGiaDTO layLoaiDocGiaTheoDocGia(DocGiaDTO dg, List<LoaiDocGiaDTO> lst)
        {
            for (int i = 0; i < lst.Count; i++)
            {
                if(lst[i].maLoai == dg.LoaiDocGia)
                {
                    return lst[i];
                }
            }
            return null;
        }

        public override string ToString()
        {
            return String.Format("{0} - {1}", maLoai, tenLoai);
        }
    }
}
