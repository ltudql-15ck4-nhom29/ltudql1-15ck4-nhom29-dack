﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class HinhThucPhatDTO
    {
        int maHinhThucPhat;

        public int MaHinhThucPhat
        {
            get { return maHinhThucPhat; }
            set { maHinhThucPhat = value; }
        }

        string tenHinhThucPhat;

        public string TenHinhThucPhat
        {
            get { return tenHinhThucPhat; }
            set { tenHinhThucPhat = value; }
        }

        int soTienPhat;

        public int SoTienPhat
        {
            get { return soTienPhat; }
            set { soTienPhat = value; }
        }

        bool ngungCungCapDichVu;

        public bool NgungCungCapDichVu
        {
            get { return ngungCungCapDichVu; }
            set { ngungCungCapDichVu = value; }
        }

        public override string ToString()
        {
            return String.Format("{0} - {1}", maHinhThucPhat, tenHinhThucPhat);
        }

        public static HinhThucPhatDTO layHinhThucPhatTheoDocGia(DocGiaDTO dg, List<HinhThucPhatDTO> lst)
        {
            for(int i = 0; i < lst.Count; i++)
            {
                if(lst[i].maHinhThucPhat == dg.MaHinhThucPhat)
                {
                    return lst[i];
                }
            }
            return null;
        }
    }
}
