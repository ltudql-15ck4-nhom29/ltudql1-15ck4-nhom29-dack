﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class ChiTietPhieuTraDTO
    {
        int soThuTu;

        public int SoThuTu
        {
            get { return soThuTu; }
            set { soThuTu = value; }
        }

        int maPhieuTra;

        public int MaPhieuTra
        {
            get { return maPhieuTra; }
            set { maPhieuTra = value; }
        }

        int maBanSao;

        public int MaBanSao
        {
            get { return maBanSao; }
            set { maBanSao = value; }
        }

        int maPhieuMuon;

        public int MaPhieuMuon
        {
            get { return maPhieuMuon; }
            set { maPhieuMuon = value; }
        }

        DateTime ngayTra;

        public DateTime NgayTra
        {
            get { return ngayTra; }
            set { ngayTra = value; }
        }

        //Dựa vào Số thứ tự CTPT của Chi tiết phiếu mượn để
        //lấy Chi tiết phiếu trả tương ứng trong list Chi tiết phiếu trả
        public static ChiTietPhieuTraDTO layChiTietPhieuTraTheoChiTietPhieuMuon(ChiTietPhieuMuonDTO ctpm, List<ChiTietPhieuTraDTO> lst)
        {
            if (ctpm.SoThuTuCTPT > -1)
            {
                for (int i = 0; i < lst.Count; i++)
                {
                    if (lst[i].soThuTu == ctpm.SoThuTuCTPT)
                    {
                        return lst[i];
                    }
                }
            }
            return null;
        }

        public static List<ChiTietPhieuTraDTO> layDSChiTietPhieuTraTheoPhieuTra(PhieuTraDTO pm, List<ChiTietPhieuTraDTO> lst)
        {
            List<ChiTietPhieuTraDTO> ds = new List<ChiTietPhieuTraDTO>();
            for (int i = 0; i < lst.Count; i++)
            {
                if (lst[i].maPhieuTra == pm.MaPhieu)
                {
                    ds.Add(lst[i]);
                }
            }
            return ds;
        }
    }
}
