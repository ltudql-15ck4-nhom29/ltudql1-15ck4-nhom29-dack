﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class PhieuMuonDTO
    {
        int maPhieu;

        public int MaPhieu
        {
            get { return maPhieu; }
            set { maPhieu = value; }
        }

        DateTime ngayLapPhieu;

        public DateTime NgayLapPhieu
        {
            get { return ngayLapPhieu; }
            set { ngayLapPhieu = value; }
        }

        int maDocGia;

        public int MaDocGia
        {
            get { return maDocGia; }
            set { maDocGia = value; }
        }

        List<ChiTietPhieuMuonDTO> lstCTPM;

        public List<ChiTietPhieuMuonDTO> LstCTPM
        {
            get { return lstCTPM; }
            set { lstCTPM = value; }
        }

        int quaHan;

        public int QuaHan
        {
            get { return quaHan; }
            set { quaHan = value; }
        }

        int chuaTra;

        public int ChuaTra
        {
            get { return chuaTra; }
            set { chuaTra = value; }
        }

        public void tinhQuaHanChuaTraNgayTra(List<ChiTietPhieuTraDTO> lst)
        {
            quaHan = 0;
            chuaTra = 0;
            for(int i = 0; i < lstCTPM.Count; i++)
            {
                ChiTietPhieuMuonDTO ctpm = lstCTPM[i];
                if(ctpm.SoThuTuCTPT == -1)
                {
                    chuaTra++;
                    if(ctpm.HanChotTra.Date < DateTime.Now.Date)
                    {
                        quaHan++;
                    }
                    ctpm.NgayTra = DateTime.MinValue;
                }
                else
                {
                    ChiTietPhieuTraDTO ctpt = ChiTietPhieuTraDTO.layChiTietPhieuTraTheoChiTietPhieuMuon(ctpm, lst);
                    if(ctpt.NgayTra.Date > ctpm.HanChotTra.Date)
                    {
                        quaHan++;
                    }
                    ctpm.NgayTra = ctpt.NgayTra;
                }
            }
        }

        public static List<PhieuMuonDTO> layDSPhieuMuonDayDu(List<PhieuMuonDTO> lst1, List<ChiTietPhieuMuonDTO> lst2)
        {
            for(int i = 0; i < lst1.Count; i++)
            {
                lst1[i].lstCTPM = ChiTietPhieuMuonDTO.layDSChiTietPhieuMuonTheoPhieuMuon(lst1[i], lst2);
            }
            return lst1;
        }

        public static List<PhieuMuonDTO> layDSPhieuMuonTheoDocGia(DocGiaDTO dg, List<PhieuMuonDTO> lst)
        {
            List<PhieuMuonDTO> ds = new List<PhieuMuonDTO>();
            for (int i = 0; i < lst.Count; i++)
            {
                if (lst[i].MaDocGia == dg.MaDocGia)
                {
                    ds.Add(lst[i]);
                }
            }
            return ds;
        }
    }
}
