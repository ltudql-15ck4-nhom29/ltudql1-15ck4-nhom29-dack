﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class BanSaoDTO
    {
        int maBanSao;

        public int MaBanSao
        {
            get { return maBanSao; }
            set { maBanSao = value; }
        }

        int maTaiLieu;

        public int MaTaiLieu
        {
            get { return maTaiLieu; }
            set { maTaiLieu = value; }
        }

        int maDocGiaDangMuon;

        public int MaDocGiaDangMuon
        {
            get { return maDocGiaDangMuon; }
            set { maDocGiaDangMuon = value; }
        }

        //Dựa vào Mã bản sao của Chi tiết phiếu mượn để lấy Bản sao tương ứng
        //trong list Bản sao
        public static BanSaoDTO layBanSao(ChiTietPhieuMuonDTO ctpm, List<BanSaoDTO> lst)
        {
            for (int i = 0; i < lst.Count; i++)
            {
                if (lst[i].maBanSao == ctpm.MaBanSao)
                {
                    return lst[i];
                }
            }
            return null;
        }

        //Dựa vào Mã bản sao của Chi tiết phiếu trả để lấy Bản sao tương ứng
        //trong list Bản sao
        public static BanSaoDTO layBanSao(ChiTietPhieuTraDTO ctpt, List<BanSaoDTO> lst)
        {
            for (int i = 0; i < lst.Count; i++)
            {
                if (lst[i].maBanSao == ctpt.MaBanSao)
                {
                    return lst[i];
                }
            }
            return null;
        }
        
        public static List<BanSaoDTO> layDSBanSaoTheoTaiLieu(TaiLieuDTO tl, List<BanSaoDTO> lst)
        {
            List<BanSaoDTO> ds = new List<BanSaoDTO>();
            for(int i = 0; i < lst.Count; i++)
            {
                if(lst[i].maTaiLieu == tl.MaTaiLieu)
                {
                    ds.Add(lst[i]);
                }
            }
            return ds;
        }

        public static BanSaoDTO layBanSaoTheoChiTietPhieuMuon(ChiTietPhieuMuonDTO ctpm, List<BanSaoDTO> lst)
        {
            for(int i = 0; i < lst.Count; i++)
            {
                if(lst[i].maBanSao == ctpm.MaBanSao)
                {
                    return lst[i];
                }
            }
            return null;
        }

        public static List<BanSaoDTO> layDSBanSaoTheoDSPhieuMuonChuaTra(List<PhieuMuonDTO> lst1, List<BanSaoDTO> lst2)
        {
            List<BanSaoDTO> ds = new List<BanSaoDTO>();
            for(int i = 0; i < lst1.Count; i++)
            {
                List<ChiTietPhieuMuonDTO> lstCTPM = lst1[i].LstCTPM;
                for(int j = 0; j < lstCTPM.Count; j++)
                {
                    ChiTietPhieuMuonDTO ctpm = lstCTPM[j];
                    if(ctpm.SoThuTuCTPT == -1)
                    {
                        BanSaoDTO bs = layBanSaoTheoChiTietPhieuMuon(ctpm, lst2);
                        ds.Add(bs);
                    }
                }
            }
            return ds;
        }
    }
}
