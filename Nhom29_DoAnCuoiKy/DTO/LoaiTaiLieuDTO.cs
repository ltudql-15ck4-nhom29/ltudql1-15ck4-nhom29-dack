﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class LoaiTaiLieuDTO
    {
        int maLoai;

        public int MaLoai
        {
            get { return maLoai; }
            set { maLoai = value; }
        }

        string tenLoai;

        public string TenLoai
        {
            get { return tenLoai; }
            set { tenLoai = value; }
        }

        public override string ToString()
        {
            return String.Format("{0} - {1}", maLoai, tenLoai);
        }

        public static LoaiTaiLieuDTO layLoaiTaiLieuTheoTaiLieu(TaiLieuDTO tl, List<LoaiTaiLieuDTO> lst)
        {
            for(int i = 0; i < lst.Count; i++)
            {
                if(lst[i].maLoai == tl.LoaiTaiLieu)
                {
                    return lst[i];
                }
            }
            return null;
        }
    }
}
