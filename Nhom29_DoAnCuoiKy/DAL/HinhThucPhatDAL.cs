﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DAL
{
    public class HinhThucPhatDAL
    {
        public List<HinhThucPhatDTO> layDSHinhThucPhat()
        {
            DataProvider db = new DataProvider();
            string query = "SELECT * FROM HinhThucPhat;";
            List<List<object>> lst = db.ExecuteReader(query);
            List<HinhThucPhatDTO> ds = new List<HinhThucPhatDTO>();
            for (int i = 0; i < lst.Count; i++)
            {
                HinhThucPhatDTO lhtp = new HinhThucPhatDTO();
                lhtp.MaHinhThucPhat = (int)lst[i][0];
                lhtp.TenHinhThucPhat = (string)lst[i][1];
                lhtp.SoTienPhat = (int)lst[i][2];
                lhtp.NgungCungCapDichVu = (bool)lst[i][3];               
                ds.Add(lhtp);
            }
            return ds;
        }
    }
}
