﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DAL
{
    public class TaiLieuDAL
    {
        public List<TaiLieuDTO> layDSTaiLieu()
        {
            DataProvider db = new DataProvider();
            string query = "SELECT * FROM TaiLieu";
            List<List<object>> lst = db.ExecuteReader(query);
            List<TaiLieuDTO> ds = new List<TaiLieuDTO>();
            for (int i = 0; i < lst.Count; i++)
            {
                TaiLieuDTO tl = new TaiLieuDTO();
                tl.MaTaiLieu = (int)lst[i][0];
                tl.TenTaiLieu = (string)lst[i][1];
                tl.LoaiTaiLieu = (int)lst[i][2];
                tl.ChoGVCB = (bool)lst[i][3];
                tl.LaSachHiem = (bool)lst[i][4];
                ds.Add(tl);
            }
            return ds;
        }

        public int xoaTaiLieu(TaiLieuDTO tl)
        {
            DataProvider db = new DataProvider();
            string query = String.Format("DELETE FROM TaiLieu WHERE MaTL = {0};",
                tl.MaTaiLieu);
            return db.ExecuteNonQuery(query);
        }

        public int capNhatTaiLieu(TaiLieuDTO tl)
        {
            DataProvider db = new DataProvider();
            string query = String.Format("UPDATE TaiLieu SET TenTL = N'{0}', LoaiTL = {1}, [ChiDanhChoGiangVien/CanBo] = '{2}', LaSachHiem = '{3}' WHERE MaTL = {4}",
                tl.TenTaiLieu, tl.LoaiTaiLieu, tl.ChoGVCB, tl.LaSachHiem, tl.MaTaiLieu);
            return db.ExecuteNonQuery(query);
        }

        public int themTaiLieu(TaiLieuDTO tl)
        {
            DataProvider db = new DataProvider();
            string query = String.Format("INSERT INTO TaiLieu(TenTL, LoaiTL, [ChiDanhChoGiangVien/CanBo], LaSachHiem) VALUES(N'{0}', {1}, '{2}', '{3}'); SELECT SCOPE_IDENTITY();",
                tl.TenTaiLieu, tl.LoaiTaiLieu, tl.ChoGVCB, tl.LaSachHiem);
            object obj = db.ExecuteScalar(query);
            int id = Convert.ToInt32((decimal)obj);
            return id;
        }

        public bool kiemTraTenTaiLieu(TaiLieuDTO tl)
        {
            DataProvider db = new DataProvider();
            string query = String.Format("SELECT * FROM TaiLieu WHERE TenTL = N'{0}';", tl.TenTaiLieu);
            List<List<object>> lst = db.ExecuteReader(query);
            if (lst.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
