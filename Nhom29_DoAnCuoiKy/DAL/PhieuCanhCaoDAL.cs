﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DAL
{
    public class PhieuCanhCaoDAL
    {
        public List<PhieuCanhCaoDTO> layDSPhieuCanhCao()
        {
            DataProvider db = new DataProvider();
            string query = "SELECT * FROM PhieuCanhCao;";
            List<List<object>> lst = db.ExecuteReader(query);
            List<PhieuCanhCaoDTO> ds = new List<PhieuCanhCaoDTO>();
            for (int i = 0; i < lst.Count; i++)
            {
                PhieuCanhCaoDTO lpcc = new PhieuCanhCaoDTO();
                lpcc.MaPhieu = (int)lst[i][0];
                lpcc.MaDocGia = (int)lst[i][1];
                lpcc.NgayLapPhieu = (DateTime)lst[i][2];
                lpcc.MaHinhThucPhat = (int)lst[i][3];
                ds.Add(lpcc);
            }
            return ds;
        }

        public int themPhieuCanhCao(PhieuCanhCaoDTO pcc)
        {
            DataProvider db = new DataProvider();
            string query = String.Format("INSERT INTO PhieuCanhCao(MaDG, NgayLapPhieu, MaHinhThucPhat) VALUES({0}, '{1}', {2});",
                pcc.MaDocGia, pcc.NgayLapPhieu, pcc.MaHinhThucPhat);
            return db.ExecuteNonQuery(query);
        }

        public int xoaPhieuCanhCaoNeuMaHinhThucPhatLonHon(PhieuCanhCaoDTO pcc)
        {
            DataProvider db = new DataProvider();
            string query = String.Format("DELETE FROM PhieuCanhCao WHERE MaDG = {0} AND MaHinhThucPhat > {1};",
                pcc.MaDocGia, pcc.MaHinhThucPhat);
            return db.ExecuteNonQuery(query);
        }

        public int themPhieuCanhCaoNeuMaHinhThucPhatNhoHon(PhieuCanhCaoDTO pcc)
        {
            if(pcc.MaHinhThucPhat > 0)
            {
                DataProvider db = new DataProvider();
                string query = String.Format("SELECT MAX(MaHinhThucPhat) AS MaHinhThucPhatHienTai FROM PhieuCanhCao WHERE MaDG = {0};", pcc.MaDocGia);
                object obj = db.ExecuteScalar(query);
                int maHinhThucPhatHienTai = 0;
                if (obj != null)
                {
                    maHinhThucPhatHienTai = Convert.ToInt32((decimal)obj);
                }
                for(; maHinhThucPhatHienTai < pcc.MaHinhThucPhat; maHinhThucPhatHienTai++)
                {
                    query = String.Format("INSERT INTO PhieuCanhCao(MaDG, NgayLapPhieu, MaHinhThucPhat) VALUES({0}, '{1}', {2});",
                        pcc.MaDocGia, pcc.NgayLapPhieu, maHinhThucPhatHienTai + 1);
                    db.ExecuteNonQuery(query);
                }
                return pcc.MaHinhThucPhat;
            }
            return -1;
        }
    }
}
