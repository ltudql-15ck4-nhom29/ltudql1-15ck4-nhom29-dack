﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DAL
{
    public class PhieuTraDAL
    {
        public List<PhieuTraDTO> layDSPhieuTra()
        {
            DataProvider db = new DataProvider();
            string query = "SELECT * FROM PhieuTra;";
            List<List<object>> lst = db.ExecuteReader(query);
            List<PhieuTraDTO> ds = new List<PhieuTraDTO>();
            for (int i = 0; i < lst.Count; i++)
            {
                PhieuTraDTO lpt = new PhieuTraDTO();
                lpt.MaPhieu = (int)lst[i][0];
                lpt.NgayLapPhieu = (DateTime)lst[i][1];
                lpt.MaDocGia = (int)lst[i][2];
                ds.Add(lpt);
            }
            return ds;
        }

        public int xoaPhieuTra(PhieuTraDTO pt)
        {
            DataProvider db = new DataProvider();
            string query = String.Format("DELETE FROM PhieuTra WHERE MaPT = {0};",
                pt.MaPhieu);
            return db.ExecuteNonQuery(query);
        }

        public int capNhatPhieuTra(PhieuTraDTO pt)
        {
            DataProvider db = new DataProvider();
            string query = String.Format("UPDATE PhieuTra SET NgayLapPhieu = '{0}', MaDG = {1} WHERE MaPT = {2};",
                pt.NgayLapPhieu, pt.MaDocGia, pt.MaPhieu);
            return db.ExecuteNonQuery(query);
        }

        public int themPhieuTra(PhieuTraDTO pt)
        {
            DataProvider db = new DataProvider();
            string query = String.Format("INSERT INTO PhieuTra(NgayLapPhieu, MaDG) VALUES('{0}', {1}); SELECT SCOPE_IDENTITY();",
                pt.NgayLapPhieu, pt.MaDocGia);
            object obj = db.ExecuteScalar(query);
            int id = Convert.ToInt32((decimal)obj);
            return id;
        }

        public int xoaPhieuTraTrong()
        {
            DataProvider db = new DataProvider();
            string query = "DELETE FROM PhieuTra WHERE (SELECT COUNT(*) FROM ChiTietPhieuTra WHERE MaPT = PhieuTra.MaPT) = 0;";
            return db.ExecuteNonQuery(query);
        }
    }
}
