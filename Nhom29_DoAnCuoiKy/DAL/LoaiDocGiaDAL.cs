﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DAL
{
    public class LoaiDocGiaDAL
    {
        public List<LoaiDocGiaDTO> layDSLoaiDocGia()
        {
            DataProvider db = new DataProvider();
            string query = "SELECT * FROM LoaiDocGia";
            List<List<object>> lst = db.ExecuteReader(query);
            List<LoaiDocGiaDTO> ds = new List<LoaiDocGiaDTO>();
            for (int i = 0; i < lst.Count; i++)
            {
                LoaiDocGiaDTO ldg = new LoaiDocGiaDTO();
                ldg.MaLoai = (int)lst[i][0];
                ldg.TenLoai = (string)lst[i][1];
                ldg.LaGVCB = (bool)lst[i][2];
                ldg.LaSinhVien = (bool)lst[i][3];
                ldg.SoNgayMuonMax = (int)lst[i][4];
                ldg.SoTaiLieuMuonMax = (int)lst[i][5];
                ldg.PhiThuongNien = (int)lst[i][6];
                ds.Add(ldg);
            }
            return ds;
        }
    }
}
