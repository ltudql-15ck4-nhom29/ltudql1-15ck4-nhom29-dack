﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DAL
{
    public class NguoiDungDAL
    {
        public NguoiDungDTO layNguoiDung(string tenDangNhap, string matKhau)
        {
            DataProvider db = new DataProvider();
            string query = String.Format("SELECT * FROM NguoiDung WHERE TenND=N'{0}' AND MatKhau='{1}';", tenDangNhap, HashAlgorithm.layMaHoaMD5(matKhau));
            //Dùng List<List<object>> để nhận mảng dữ liệu từ ExecuteReader()
            List<List<object>> lst = db.ExecuteReader(query);
            if (lst.Count > 0)
            {
                //Sau đó ép kiểu dữ liệu:
                //ép về (int) nếu trong db là int
                //ép về (string) nếu trong db là nvarchar
                //ép về (DateTime) nếu trong db là date
                //ép về (bool) nếu trong db là bit
                NguoiDungDTO nd = new NguoiDungDTO();
                nd.MaNguoiDung = (int)lst[0][0];
                nd.TenNguoiDung = (string)lst[0][1];
                nd.MatKhau = (string)lst[0][2];
                nd.CapBac = (int)lst[0][3];
                return nd;
            }
            else
            {
                return null;
            }
        }

        //Hàm để kiểm tra tên người dùng có tồn tại hay chưa
        public NguoiDungDTO layNguoiDung(string tenNguoiDung)
        {
            DataProvider db = new DataProvider();
            string query = String.Format("SELECT * FROM NguoiDung WHERE TenND=N'{0}';"
                , tenNguoiDung);
            List<List<object>> lst = db.ExecuteReader(query);
            if (lst.Count > 0)
            {
                NguoiDungDTO nd = new NguoiDungDTO();
                nd.MaNguoiDung = (int)lst[0][0];
                nd.TenNguoiDung = (string)lst[0][1];
                nd.MatKhau = (string)lst[0][2];
                nd.CapBac = (int)lst[0][3];
                return nd;
            }
            else
            {
                return null;
            }
        }

        public List<NguoiDungDTO> layDSNguoiDung()
        {
            DataProvider db = new DataProvider();
            string query = "SELECT * FROM NguoiDung";
            List<List<object>> lst = db.ExecuteReader(query);
            List<NguoiDungDTO> ds = new List<NguoiDungDTO>();
            for(int i = 0; i < lst.Count; i++)
            {
                NguoiDungDTO nd = new NguoiDungDTO();
                nd.MaNguoiDung = (int)lst[i][0];
                nd.TenNguoiDung = (string)lst[i][1];
                nd.MatKhau = (string)lst[i][2];
                nd.CapBac = (int)lst[i][3];
                ds.Add(nd);
            }
            return ds;
        }

        //Nếu là hàm INSERT, UPDATE, DELETE thì dùng ExecuteNonQuery() để trả về số dòng
        //bị ảnh hưởng để có thể kiểm tra ở các form giao diện

        //Khi INSERT hoặc UPDATE, nếu cột trong db có kiểu dữ liệu là nvarchar
        //thì phải thêm N vào trước giá trị nhập vào (xem kỹ bên dưới)
        public int themNguoiDung(NguoiDungDTO nd)
        {
            DataProvider db = new DataProvider();
            string query = String.Format("INSERT INTO NguoiDung(TenND, MatKhau, CapBac) VALUES(N'{0}',N'{1}',{2});",
                nd.TenNguoiDung, HashAlgorithm.layMaHoaMD5(nd.MatKhau), nd.CapBac);
            return db.ExecuteNonQuery(query);
        }

        public int capNhatNguoiDung(NguoiDungDTO nd)
        {
            DataProvider db = new DataProvider();
            string query;
            //Nếu mật khẩu trống thì không cập nhật mật khẩu
            if(nd.MatKhau == "")
            {
                query = String.Format("UPDATE NguoiDung SET TenND=N'{0}', CapBac={1} WHERE MaND={2};",
                    nd.TenNguoiDung, nd.CapBac, nd.MaNguoiDung);
            }
            else
            {
                query = String.Format("UPDATE NguoiDung SET TenND=N'{0}', MatKhau='{1}', CapBac={2} WHERE MaND={3};",
                    nd.TenNguoiDung, HashAlgorithm.layMaHoaMD5(nd.MatKhau), nd.CapBac, nd.MaNguoiDung);
            }
            return db.ExecuteNonQuery(query);
        }

        public int xoaNguoiDung(NguoiDungDTO nd)
        {
            DataProvider db = new DataProvider();
            string query = String.Format("DELETE FROM NguoiDung WHERE MaND={0};",
                nd.MaNguoiDung);
            return db.ExecuteNonQuery(query);
        }
    }
}
