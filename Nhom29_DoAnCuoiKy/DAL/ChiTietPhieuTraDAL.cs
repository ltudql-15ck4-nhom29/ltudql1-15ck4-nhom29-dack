﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DAL
{
    public class ChiTietPhieuTraDAL
    {
        public List<ChiTietPhieuTraDTO> layDSChiTietPhieuTra()
        {
            DataProvider db = new DataProvider();
            string query = "SELECT * FROM ChiTietPhieuTra;";
            List<List<object>> lst = db.ExecuteReader(query);
            List<ChiTietPhieuTraDTO> ds = new List<ChiTietPhieuTraDTO>();
            for (int i = 0; i < lst.Count; i++)
            {
                ChiTietPhieuTraDTO ctpt = new ChiTietPhieuTraDTO();
                ctpt.SoThuTu = (int)lst[i][0];
                ctpt.MaPhieuTra = (int)lst[i][1];
                ctpt.MaBanSao = (int)lst[i][2];
                ctpt.MaPhieuMuon = (int)lst[i][3];
                ctpt.NgayTra = (DateTime)lst[i][4];
                ds.Add(ctpt);
            }
            return ds;
        }

        public int themChiTietPhieuTra(ChiTietPhieuTraDTO ctpt)
        {
            DataProvider db = new DataProvider();
            string query = String.Format("INSERT INTO ChiTietPhieuTra(MaPT, MaBS, MaPM, NgayTra) VALUES({0}, {1}, {2}, '{3}'); SELECT SCOPE_IDENTITY();",
                ctpt.MaPhieuTra, ctpt.MaBanSao, ctpt.MaPhieuMuon, ctpt.NgayTra);
            object obj = db.ExecuteScalar(query);
            int id = Convert.ToInt32((decimal)obj);
            return id;
        }

        public int xoaChiTietPhieuTra(ChiTietPhieuTraDTO ctpt)
        {
            DataProvider db = new DataProvider();
            string query = String.Format("DELETE FROM ChiTietPhieuTra WHERE STT = {0};",
                ctpt.SoThuTu);
            return db.ExecuteNonQuery(query);
        }

        public int capNhatChiTietPhieuTraTheoPhieuTra(ChiTietPhieuTraDTO ctpt)
        {
            DataProvider db = new DataProvider();
            string query = String.Format("UPDATE ChiTietPhieuTra SET NgayTra = '{0}' WHERE STT = {1};",
                ctpt.NgayTra, ctpt.SoThuTu);
            return db.ExecuteNonQuery(query);
        }

        public int xoaChiTietPhieuTraTheoMaBanSao(ChiTietPhieuTraDTO ctpt)
        {
            DataProvider db = new DataProvider();
            string query = String.Format("DELETE FROM ChiTietPhieuTra WHERE MaBS = {0};",
                ctpt.MaBanSao);
            return db.ExecuteNonQuery(query);
        }

        public int xoaChiTietPhieuTraTheoPhieuMuon(PhieuMuonDTO pm)
        {
            DataProvider db = new DataProvider();
            string query = String.Format("DELETE FROM ChiTietPhieuTra WHERE MaPM = {0};",
                pm.MaPhieu);
            return db.ExecuteNonQuery(query);
        }
    }
}
