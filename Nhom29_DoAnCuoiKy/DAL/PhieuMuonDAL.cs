﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DAL
{
    public class PhieuMuonDAL
    {
        //Lấy danh sách Phiếu mượn và nạp list Chi tiết phiếu mượn tương ứng
        //với từng Phiếu mượn (xem thêm PhieuMuonDTO)
        public List<PhieuMuonDTO> layDSPhieuMuon()
        {
            DataProvider db = new DataProvider();
            string query = "SELECT * FROM PhieuMuon;";
            List<List<object>> lst = db.ExecuteReader(query);
            List<PhieuMuonDTO> ds = new List<PhieuMuonDTO>();
            for (int i = 0; i < lst.Count; i++)
            {
                PhieuMuonDTO pm = new PhieuMuonDTO();
                pm.MaPhieu = (int)lst[i][0];
                pm.NgayLapPhieu = (DateTime)lst[i][1];
                pm.MaDocGia = (int)lst[i][2];
                ds.Add(pm);
            }
            return ds;
        }

        public int themPhieuMuon(PhieuMuonDTO pm)
        {
            DataProvider db = new DataProvider();
            string query = String.Format("INSERT INTO PhieuMuon(NgayLapPhieu, MaDG) VALUES('{0}', {1}); SELECT SCOPE_IDENTITY();",
                pm.NgayLapPhieu, pm.MaDocGia);
            object obj = db.ExecuteScalar(query);
            int id = Convert.ToInt32((decimal)obj);
            return id;
        }

        public int xoaPhieuMuon(PhieuMuonDTO pm)
        {
            DataProvider db = new DataProvider();
            string query = String.Format("DELETE FROM PhieuMuon WHERE MaPM = {0};",
                pm.MaPhieu);
            return db.ExecuteNonQuery(query);
        }

        public int capNhatPhieuMuonDayDu(PhieuMuonDTO pm)
        {
            DataProvider db = new DataProvider();
            string query = String.Format("UPDATE PhieuMuon SET NgayLapPhieu = '{0}', MaDG = {1} WHERE MaPM = {2};",
                pm.NgayLapPhieu, pm.MaDocGia, pm.MaPhieu);
            db.ExecuteNonQuery(query);
            query = String.Format("UPDATE ChiTietPhieuMuon SET HanChotTra = (SELECT CONVERT(datetime, '{0}') + (SELECT (SELECT SoNgayMuonToiDa FROM LoaiDocGia WHERE MaLoaiDG = DocGia.LoaiDG) AS SoNgayMuonToiDa FROM DocGia WHERE MaDG = {1})) WHERE MaPM = {2};",
                pm.NgayLapPhieu, pm.MaDocGia, pm.MaPhieu);
            db.ExecuteNonQuery(query);
            List<ChiTietPhieuMuonDTO> lstCTPM = pm.LstCTPM;
            for (int i = 0; i < lstCTPM.Count; i++)
            {
                ChiTietPhieuMuonDTO ctpm = lstCTPM[i];
                if(ctpm.SoThuTuCTPT == -1)
                {
                    BanSaoDTO bs = new BanSaoDTO();
                    bs.MaBanSao = ctpm.MaBanSao;
                    BanSaoDAL dal = new BanSaoDAL();
                    dal.capNhatBanSao(bs, null);
                }
            }
            return 1;
        }

        public int xoaPhieuMuonTrong()
        {
            DataProvider db = new DataProvider();
            string query = "DELETE FROM PhieuMuon WHERE (SELECT COUNT(*) FROM ChiTietPhieuMuon WHERE MaPM = PhieuMuon.MaPM) = 0;";
            return db.ExecuteNonQuery(query);
        }
    }
}
