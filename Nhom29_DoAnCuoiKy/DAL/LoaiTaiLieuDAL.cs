﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DAL
{
    public class LoaiTaiLieuDAL
    {
        public List<LoaiTaiLieuDTO> layDSLoaiTaiLieu()
        {
            DataProvider db = new DataProvider();
            string query = "SELECT * FROM LoaiTaiLieu;";
            List<List<object>> lst = db.ExecuteReader(query);
            List<LoaiTaiLieuDTO> ds = new List<LoaiTaiLieuDTO>();
            for (int i = 0; i < lst.Count; i++)
            {
                LoaiTaiLieuDTO ltl = new LoaiTaiLieuDTO();
                ltl.MaLoai = (int)lst[i][0];
                ltl.TenLoai = (string)lst[i][1];
                ds.Add(ltl);
            }
            return ds;
        }
    }
}
