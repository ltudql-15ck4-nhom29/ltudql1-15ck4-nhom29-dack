﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;

namespace DAL
{
    //Thử nghiệm Pull
    class DataProvider
    {
        SqlConnection connection = null;
        SqlCommand command = new SqlCommand();

        public DataProvider()
        {
            Initialize();
        }

        void Initialize()
        {
            //SqlConnectionStringBuilder cnStringBuilder = new SqlConnectionStringBuilder();
            //cnStringBuilder["Data Source"] = @".\SQLEXPRESS";
            //cnStringBuilder["Integrated Security"] = true;
            //cnStringBuilder["Initial Catalog"] = "QLTV";
            //connection = new SqlConnection(cnStringBuilder.ConnectionString);

            string connectionString = ConfigurationManager.ConnectionStrings["GUI.Properties.Settings.QLTVConnectionString"].ConnectionString;
            connection = new SqlConnection(connectionString);

            command.Connection = connection;
        }

        public int ExecuteNonQuery(string query)
        {
            connection.Open();
            command.CommandText = query;
            int affected = command.ExecuteNonQuery();
            connection.Close();
            return affected;
        }

        public object ExecuteScalar(string query)
        {
            connection.Open();
            command.CommandText = query;
            object obj = command.ExecuteScalar();
            if(DBNull.Value.Equals(obj))
            {
                obj = null;
            }
            connection.Close();
            return obj;
        }

        public List<List<object>> ExecuteReader(string query)
        {
            connection.Open();
            List<List<object>> lst = new List<List<object>>();
            command.CommandText = query;
            SqlDataReader rd = command.ExecuteReader();
            int fieldCount = rd.FieldCount;
            while (rd.Read())
            {
                List<object> row = new List<object>();
                for (int i = 0; i < fieldCount; i++)
                {
                    row.Add(rd.GetValue(i));
                }
                lst.Add(row);
            }
            rd.Close();
            connection.Close();
            return lst;
        }
    }
}
