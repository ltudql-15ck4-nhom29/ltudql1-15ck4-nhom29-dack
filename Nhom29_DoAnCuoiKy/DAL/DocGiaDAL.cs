﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DAL
{
    public class DocGiaDAL
    {
        public List<DocGiaDTO> layDSDocGia()
        {
            DataProvider db = new DataProvider();
            string query = "SELECT * FROM DocGia";
            List<List<object>> lst = db.ExecuteReader(query);
            List<DocGiaDTO> ds = new List<DocGiaDTO>();
            for (int i = 0; i < lst.Count; i++) 
            {
                DocGiaDTO dg = new DocGiaDTO();
                dg.MaDocGia = (int)lst[i][0];
                dg.HoTen = (string)lst[i][1];
                dg.CMND = (string)lst[i][2];
                dg.NgaySinh = (DateTime)lst[i][3];
                dg.SoDienThoai = (string)lst[i][4];
                dg.Email = (string)lst[i][5];
                dg.DiaChi = (string)lst[i][6];
                if(DBNull.Value.Equals(lst[i][7]))
                {
                    dg.MSSV = null;
                }
                else
                {
                    dg.MSSV = (string)lst[i][7];
                }
                if(DBNull.Value.Equals(lst[i][8]))
                {
                    dg.MSCB = null;
                }
                else
                {
                    dg.MSCB = (string)lst[i][8];
                }
                dg.LoaiDocGia = (int)lst[i][9];
                dg.SoTaiLieuDangMuon = (int)lst[i][10];
                dg.MaHinhThucPhat = (int)lst[i][11];
                ds.Add(dg);
            }
            return ds;
        }

        public int themDocGia(DocGiaDTO dg)
        {
            DataProvider db = new DataProvider();
            string query = null;
            if (dg.MSCB != null)
            {
                query = String.Format("INSERT INTO DocGia(HoTen, CMND, NgaySinh, SDT, Email, DiaChi, LoaiDG, SoTaiLieuDangMuon, MaHinhThucPhat, MSCB) VALUES(N'{0}',N'{1}','{2}',N'{3}',N'{4}',N'{5}', {6}, 0, 0, N'{7}');",
                    dg.HoTen, dg.CMND, dg.NgaySinh, dg.SoDienThoai, dg.Email, dg.DiaChi, dg.LoaiDocGia, dg.MSCB);
            }
            else if(dg.MSSV != null)
            {
                query = String.Format("INSERT INTO DocGia(HoTen, CMND, NgaySinh, SDT, Email, DiaChi, LoaiDG, SoTaiLieuDangMuon, MaHinhThucPhat, MSSV) VALUES(N'{0}',N'{1}','{2}',N'{3}',N'{4}',N'{5}', {6}, 0, 0, N'{7}');",
                    dg.HoTen, dg.CMND, dg.NgaySinh, dg.SoDienThoai, dg.Email, dg.DiaChi, dg.LoaiDocGia, dg.MSSV);
            }
            else
            {
                query = String.Format("INSERT INTO DocGia(HoTen, CMND, NgaySinh, SDT, Email, DiaChi, LoaiDG, SoTaiLieuDangMuon, MaHinhThucPhat) VALUES(N'{0}',N'{1}','{2}',N'{3}',N'{4}',N'{5}', {6}, 0, 0);",
                    dg.HoTen, dg.CMND, dg.NgaySinh, dg.SoDienThoai, dg.Email, dg.DiaChi, dg.LoaiDocGia);
            }
            return db.ExecuteNonQuery(query);
        }

        public int capNhatSoTLDangMuon(DocGiaDTO dg)
        {
            DataProvider db = new DataProvider();
            string query = String.Format("UPDATE DocGia SET SoTaiLieuDangMuon = {0} WHERE MaDG = {1};",
                dg.SoTaiLieuDangMuon, dg.MaDocGia);
            return db.ExecuteNonQuery(query);
        }

        public int xoaDocGia(DocGiaDTO dg)
        {
            DataProvider db = new DataProvider();
            string query = String.Format("DELETE FROM DocGia WHERE MaDG = {0};",
                dg.MaDocGia);
            return db.ExecuteNonQuery(query);
        }

        public int capNhatDocGia(DocGiaDTO dg)
        {
            DataProvider db = new DataProvider();
            string query = null;
            if (dg.MSCB != null)
            {
                query = String.Format("UPDATE DocGia SET HoTen = N'{0}', CMND = N'{1}', NgaySinh = '{2}', SDT = N'{3}', Email = N'{4}', DiaChi = N'{5}', LoaiDG = {6}, MaHinhThucPhat = {7}, MSCB = N'{8}', MSSV = NULL WHERE MaDG = {9};",
                    dg.HoTen, dg.CMND, dg.NgaySinh, dg.SoDienThoai, dg.Email, dg.DiaChi, dg.LoaiDocGia, dg.MaHinhThucPhat, dg.MSCB, dg.MaDocGia);
            }
            else if (dg.MSSV != null)
            {
                query = String.Format("UPDATE DocGia SET HoTen = N'{0}', CMND = N'{1}', NgaySinh = '{2}', SDT = N'{3}', Email = N'{4}', DiaChi = N'{5}', LoaiDG = {6}, MaHinhThucPhat = {7}, MSCB = NULL, MSSV = N'{8}' WHERE MaDG = {9};",
                    dg.HoTen, dg.CMND, dg.NgaySinh, dg.SoDienThoai, dg.Email, dg.DiaChi, dg.LoaiDocGia, dg.MaHinhThucPhat, dg.MSSV, dg.MaDocGia);
            }
            else
            {
                query = String.Format("UPDATE DocGia SET HoTen = N'{0}', CMND = N'{1}', NgaySinh = '{2}', SDT = N'{3}', Email = N'{4}', DiaChi = N'{5}', LoaiDG = {6}, MaHinhThucPhat = {7}, MSCB = NULL, MSSV = NULL WHERE MaDG = {8};",
                    dg.HoTen, dg.CMND, dg.NgaySinh, dg.SoDienThoai, dg.Email, dg.DiaChi, dg.LoaiDocGia, dg.MaHinhThucPhat, dg.MaDocGia);
            }
            return db.ExecuteNonQuery(query);
        }

        public int capNhatHinhThucPhat(DocGiaDTO dg)
        {
            DataProvider db = new DataProvider();
            string query = String.Format("UPDATE DocGia SET MaHinhThucPhat = {0} WHERE MaDG = {1};",
                dg.MaHinhThucPhat, dg.MaDocGia);
            return db.ExecuteNonQuery(query);
        }

        public int capNhatSoTLDangMuonTheoCSDL(DocGiaDTO dg)
        {
            DataProvider db = new DataProvider();
            string query = String.Format("UPDATE DocGia SET SoTaiLieuDangMuon = (SELECT COUNT(*) FROM BanSao WHERE MaDocGiaDangMuon = {0}) WHERE MaDG = {1};",
                dg.MaDocGia, dg.MaDocGia);
            return db.ExecuteNonQuery(query);
        }
    }
}
