﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DAL
{
    public class BanSaoDAL
    {
        public List<BanSaoDTO> layDSBanSao()
        {
            DataProvider db = new DataProvider();
            string query = "SELECT * FROM BanSao";
            List<List<object>> lst = db.ExecuteReader(query);
            List<BanSaoDTO> ds = new List<BanSaoDTO>();
            for (int i = 0; i < lst.Count; i++)
            {
                BanSaoDTO bs = new BanSaoDTO();
                bs.MaBanSao = (int)lst[i][0];
                bs.MaTaiLieu = (int)lst[i][1];
                if(!DBNull.Value.Equals(lst[i][2]))
                {
                    bs.MaDocGiaDangMuon = (int)lst[i][2];
                }
                else
                {
                    bs.MaDocGiaDangMuon = -1;
                }
               
                ds.Add(bs);
            }
            return ds;
        }

        public int capNhatBanSao(BanSaoDTO bs)
        {
            DataProvider db = new DataProvider();
            string query = String.Format("UPDATE BanSao SET MaDocGiaDangMuon = {0} WHERE MaBS = {1};",
                    bs.MaDocGiaDangMuon, bs.MaBanSao);
            return db.ExecuteNonQuery(query);
        }

        public int capNhatBanSao(BanSaoDTO bs, DocGiaDTO dg)
        {
            DataProvider db = new DataProvider();
            string query = null;
            if(dg != null)
            {
                query = String.Format("UPDATE BanSao SET MaDocGiaDangMuon = {0} WHERE MaBS = {1};",
                    dg.MaDocGia, bs.MaBanSao);
            }
            else
            {
                query = String.Format("UPDATE BanSao SET MaDocGiaDangMuon = NULL WHERE MaBS = {0};",
                    bs.MaBanSao);
            }
            return db.ExecuteNonQuery(query);
        }

        public int themBanSao(BanSaoDTO bs)
        {
            DataProvider db = new DataProvider();
            string query = String.Format("INSERT INTO BanSao(MaTL) VALUES({0});",
                bs.MaTaiLieu);
            return db.ExecuteNonQuery(query);
        }
        public int xoaBanSao(BanSaoDTO bs)
        {
            DataProvider db = new DataProvider();
            string query = String.Format("DELETE FROM BanSao WHERE MaBS = {0}",
                bs.MaBanSao);
            return db.ExecuteNonQuery(query);
        }
    }
}
