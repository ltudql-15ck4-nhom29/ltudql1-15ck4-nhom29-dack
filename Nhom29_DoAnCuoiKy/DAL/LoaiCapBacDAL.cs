﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DAL
{
    public class LoaiCapBacDAL
    {
        public List<LoaiCapBacDTO> layDSLoaiCapBac()
        {
            DataProvider db = new DataProvider();
            string query = "SELECT * FROM LoaiCapBac;";
            List<List<object>> lst = db.ExecuteReader(query);
            List<LoaiCapBacDTO> ds = new List<LoaiCapBacDTO>();
            for(int i = 0; i < lst.Count; i++)
            {
                LoaiCapBacDTO lcb = new LoaiCapBacDTO();
                lcb.MaCapBac = (int)lst[i][0];
                lcb.TenCapBac = (string)lst[i][1];
                ds.Add(lcb);
            }
            return ds;
        }
    }
}
