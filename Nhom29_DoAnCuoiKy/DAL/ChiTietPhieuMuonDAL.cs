﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DAL
{
    public class ChiTietPhieuMuonDAL
    {
        public List<ChiTietPhieuMuonDTO> layDSChiTietPhieuMuon()
        {
            DataProvider db = new DataProvider();
            string query = "SELECT * FROM ChiTietPhieuMuon;";
            List<List<object>> lst = db.ExecuteReader(query);
            List<ChiTietPhieuMuonDTO> ds = new List<ChiTietPhieuMuonDTO>();
            for (int i = 0; i < lst.Count; i++)
            {
                ChiTietPhieuMuonDTO ctpm = new ChiTietPhieuMuonDTO();
                ctpm.SoThuTu = (int)lst[i][0];
                ctpm.MaPhieuMuon = (int)lst[i][1];
                ctpm.MaBanSao = (int)lst[i][2];
                ctpm.HanChotTra = (DateTime)lst[i][3];
                if (DBNull.Value.Equals(lst[i][4]))
                {
                    ctpm.SoThuTuCTPT = -1;
                }
                else
                {
                    ctpm.SoThuTuCTPT = (int)lst[i][4];
                }
                ds.Add(ctpm);
            }
            return ds;
        }

        public int themChiTietPhieuMuon(ChiTietPhieuMuonDTO ctpm)
        {
            DataProvider db = new DataProvider();
            string query = String.Format("INSERT INTO ChiTietPhieuMuon(MaPM, MaBS, HanChotTra) VALUES({0}, {1}, '{2}');",
                ctpm.MaPhieuMuon, ctpm.MaBanSao, ctpm.HanChotTra);
            return db.ExecuteNonQuery(query);
        }

        public int capNhatChiTietPhieuMuon(ChiTietPhieuMuonDTO ctpm)
        {
            DataProvider db = new DataProvider();
            string query = String.Format("UPDATE ChiTietPhieuMuon SET STT_ChiTietPhieuTra = {0} WHERE MaBS = {1} AND STT_ChiTietPhieuTra IS NULL;",
                ctpm.SoThuTuCTPT, ctpm.MaBanSao);
            return db.ExecuteNonQuery(query);
        }

        public int xoaChiTietPhieuMuonTheoSTT_CTPT(ChiTietPhieuMuonDTO ctpm)
        {
            DataProvider db = new DataProvider();
            string query = String.Format("DELETE FROM ChiTietPhieuMuon WHERE STT_ChiTietPhieuTra = {0};",
                ctpm.SoThuTuCTPT);
            return db.ExecuteNonQuery(query);
        }

        public int xoaChiTietPhieuMuon(ChiTietPhieuMuonDTO ctpm)
        {
            DataProvider db = new DataProvider();
            string query = String.Format("DELETE FROM ChiTietPhieuMuon WHERE STT = {0};",
                ctpm.SoThuTu);
            return db.ExecuteNonQuery(query);
        }
        
        public int xoaChiTietPhieuMuonTheoMaBanSao(ChiTietPhieuMuonDTO ctpm)
        {
            DataProvider db = new DataProvider();
            string query = String.Format("DELETE FROM ChiTietPhieuMuon WHERE MaBS = {0};",
                ctpm.MaBanSao);
            return db.ExecuteNonQuery(query);
        }
    }
}
