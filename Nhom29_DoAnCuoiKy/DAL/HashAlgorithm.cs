﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace DAL
{
    public class HashAlgorithm
    {
        static string salt = "2MJfLeT1NrW83SKQ";
        public static string layMaHoaMD5(string s)
        {
            MD5 thuatToan = MD5.Create();
            byte[] data = thuatToan.ComputeHash(Encoding.UTF8.GetBytes(s + salt));

            string computed = "";
            for (int i = 0; i < data.Length; i++)
            {
                computed += data[i].ToString("x2");
            }

            return computed;
        }

        public static bool kiemTraMD5(string s, string hash)
        {
            string computed = layMaHoaMD5(s);
            return (computed == hash);
        }
    }
}
